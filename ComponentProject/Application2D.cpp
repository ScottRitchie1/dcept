#include "Application2D.h"
#include "Font.h"

#include "SplashState.h"
#include "GameState.h"
#include "PauseState.h"
#include "TestState.h"
#include "GameOverState.h"
#include "GameBeginState.h"
#include "GameStateManager.h"
#include "MenuNewGameState.h"
#include "MenuTitleState.h"
#include "MenuHowToPlayState.h"

Application2D::Application2D() {
	m_cameraX = 0;
	m_cameraY = 0;
}

Application2D::~Application2D() {

}

bool Application2D::startup() {
	
	m_2dRenderer = new aie::Renderer2D();
	m_debugFont = new aie::Font("./font/consolas_italic.ttf", 12);
	LoadDefaultFont("./font/dpcomic.ttf");


	m_gameStateManager = new GameStateManager();
	m_gameStateManager->SetState("SplashState", new SplashState(this));
	m_gameStateManager->SetState("GameState", new GameState(this));
	m_gameStateManager->SetState("PauseState", new PauseState(this));
	m_gameStateManager->SetState("GameOverState", new GameOverState(this));
	m_gameStateManager->SetState("GameBeginState", new GameBeginState(this));
	m_gameStateManager->SetState("MenuNewGameState", new MenuNewGameState(this));
	m_gameStateManager->SetState("MenuTitleState", new MenuTitleState(this));
	m_gameStateManager->SetState("MenuHowToPlayState", new MenuHowToPlayState(this));


	m_gameStateManager->PushState("MenuTitleState");

	return true;
}

void Application2D::shutdown() {
	delete m_gameStateManager;
	delete m_font16;
	delete m_font24;
	delete m_font36;
	delete m_font48;
	delete m_font72;

	delete m_debugFont;
	delete m_2dRenderer;
}

void Application2D::update(float deltaTime) {

	// input example
	m_input = aie::Input::getInstance();
	m_gameStateManager->UpdateGameStates(deltaTime);
}

void Application2D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// set the camera position before we begin rendering
	m_2dRenderer->setCameraPos(m_cameraX, m_cameraY);

	// begin drawing sprites
	m_2dRenderer->begin();

	m_gameStateManager->DrawGameStates();

	// done drawing sprites
	m_2dRenderer->end();
}

void Application2D::LoadDefaultFont(const char * fontFile)
{
	m_font16 = new aie::Font(fontFile, 16);
	m_font24 = new aie::Font(fontFile, 24);
	m_font36 = new aie::Font(fontFile, 36);
	m_font48 = new aie::Font(fontFile, 48);
	m_font72 = new aie::Font(fontFile, 72);
}
