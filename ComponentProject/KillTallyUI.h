#pragma once
#include "IComponent.h"


namespace aie
{
	class Texture;
};

class CharacterManager;
class AnimatedSprite;


class KillTallyUI : public IComponent {
public:
	KillTallyUI();
	virtual ~KillTallyUI();

	//virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	virtual void SetCharacterManager(CharacterManager* manager);
	virtual void UpdateGameTimeCounter(float time);
	virtual void SetSkullTexture(const char* textureFileName, unsigned int numFrames, float fps = 1.0f);
	virtual void SetTallyCounterTexture(const char* textureFileName);
	virtual void SetClockTexture(const char* textureFileName);



private:
	unsigned int m_currentTally;


	float m_tallySpacing;
	Vector2 m_tallyStartPositionFromTopLeft;
	Vector2 m_tallySize;
	Vector2 m_tallyFrameSize;
	aie::Texture* m_tallyCounterTexture;


	CharacterManager* m_charManager;
	
	Vector2 m_skullStartPositionFromTopLeft;
	AnimatedSprite* m_skullAnimation;

	Vector2 m_ClockStartPositionFromTopLeft;
	AnimatedSprite* m_ClockAnimation;
	float m_time;
};