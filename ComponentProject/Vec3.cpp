#include "Vec3.h"

Vector3 Vector3::Right(1,0,0);
Vector3 Vector3::Up(0, 1, 0);
Vector3 Vector3::Forward(0, 0, 1);

Vector3::Vector3() : x(0), y(0), z(0)  {


}
Vector3::Vector3(float x, float y, float z = 0) : x(x), y(y), z(z) {

}
Vector3::~Vector3() {

}

float Vector3::Length() const{
	return sqrt(x*x + y*y + z*z);
}



Vector3 Vector3::normalised() {
	Vector3 copy(*this);
	copy.Normalise();
	return copy;
}


Vector3& Vector3::Normalise() {
	float l = this->Length();
	if (l > 0) {
		x /= l;
		y /= l;
		z /= l;
	}
	return *this;
}

Vector3	Vector3::GetNormalised() {

	return this->normalised();
}

float Vector3::Dot(const Vector3& rhs) const {
	return x * rhs.x + y * rhs.y + z * rhs.z;
}

Vector3 Vector3::Cross(Vector3& rhs) const {
	return Vector3((y*rhs.z) - (z*rhs.y), (z*rhs.x) - (x*rhs.z), (x*rhs.y) - (y*rhs.x));
}

Vector3 Vector3::operator * (const float &f) const {
	return Vector3(x * f, y * f, z * f);
}

Vector3 Vector3::operator+(const Vector3 &rhs)  const {
	return Vector3(x + rhs.x, y + rhs.y, z + rhs.z);
}

Vector3& Vector3::operator+=(const Vector3 &rhs) {
	this->x += rhs.x;
	this->y += rhs.y;
	this->z += rhs.z;
	return *this;
}

Vector3 Vector3::operator-(const Vector3 &rhs)  const {
	return Vector3(x - rhs.x, y - rhs.y, z - rhs.z);
}

Vector3& Vector3::operator-=(const Vector3 &rhs) {
	this->x -= rhs.x;
	this->y -= rhs.y;
	this->z -= rhs.z;

	return *this;
}

bool Vector3::operator == (const Vector3 &rhs)  const {
	return (x == rhs.x && y == rhs.y && z == rhs.z);
}

bool Vector3::operator != (const Vector3 &rhs)  const {
	return (x != rhs.x || y != rhs.y || z != rhs.z);
}

Vector3 operator + (const float &lhs, const Vector3 &rhs) {
	return Vector3(rhs.x + lhs, rhs.y + lhs, rhs.z + lhs);
}

Vector3 operator - (const float &lhs, const Vector3 &rhs) {
	return Vector3(rhs.x - lhs, rhs.y - lhs, rhs.z - lhs);
}

Vector3 operator * (const float &lhs, const Vector3 &rhs) {
	return Vector3(rhs.x * lhs, rhs.y * lhs, rhs.z * lhs);
}

float Vector3::operator [] (const unsigned int index) const {


	if (index == 0) {
		return x;
	}
	else if (index == 1) {
		return y;
	}

	return z;
}

Vector3 Vector3::operator - () const {
	return Vector3(-x, -y, -z);
}