#include "KillTallyUI.h"
#include "CharacterManager.h"

#include "Application2D.h"
#include <Texture.h>
#include "Sprite.h"
#include "Hunter_AI.h"


KillTallyUI::KillTallyUI() {
	m_charManager = nullptr;
	m_tallyCounterTexture = nullptr;
	m_skullAnimation = nullptr;
	m_ClockAnimation = nullptr;
	m_currentTally = 0;
	m_tallyStartPositionFromTopLeft = Vector2(100, 40);
	m_skullStartPositionFromTopLeft = Vector2(10, 10);
	m_ClockStartPositionFromTopLeft = Vector2(20, 120);
	m_tallySpacing = -10;
	m_tallySize = Vector2(64,64);
	m_tallyFrameSize = Vector2();
}
KillTallyUI::~KillTallyUI() {
	delete m_tallyCounterTexture;
	delete m_skullAnimation;
}

void KillTallyUI::Draw(aie::Renderer2D* m_2dRenderer) {
	
	

	if (m_charManager != nullptr) {

		if (GetApp()->debugMode) {//DEBUG STUFF
			char count[32];
			char activeCount[32];
			char aliveCount[32];
			char time[32];
			

			sprintf_s(count, 32, "Civilians: %i", m_charManager->GetCharList()->size()-1);
			sprintf_s(activeCount, 32, "Suspects: %i", m_charManager->GetActiveCharacterCount());
			sprintf_s(aliveCount, 32, "Dead / Assassin Score: %i", m_charManager->GetDeadCivilianCount());
			sprintf_s(time, "Play Time: %0.1f", m_time);
			

			GetApp()->GetRender2D()->setRenderColour(0.f, 0.f, 1.f, 1.0f);
			GetApp()->GetRender2D()->drawText(GetApp()->DebugFont(), activeCount , 0, 720 - 64);
			GetApp()->GetRender2D()->drawText(GetApp()->DebugFont(), strcat(count, " + 1 Assassin"), 0, 720 - 96);
			GetApp()->GetRender2D()->drawText(GetApp()->DebugFont(), aliveCount, 0, 720 - 128);
			m_2dRenderer->drawText(GetApp()->DebugFont(), time, 0, 720 - 192);


			auto hunter = m_charManager->GetHunter_AI();
			if (hunter != nullptr) {
				GetApp()->GetRender2D()->setRenderColour(0.f, 0.f, 1.f, 1.0f);

				char threshhold[32];
				sprintf(threshhold, "Required Proability: %0.2f", hunter->GetCurrentRequiredProbabilityThreshhold());
				GetApp()->GetRender2D()->drawText(GetApp()->DebugFont(), threshhold, 0, 720 - 160);
			}

		}
		else 
		{
			unsigned int newTally = m_charManager->GetDeadCivilianCount();
			unsigned int windowHeight = GetApp()->getWindowHeight();


			if (m_skullAnimation != nullptr) {
				if (newTally > m_currentTally) {
					m_skullAnimation->JumpToFirstFrame();
				}
				else if (newTally < m_currentTally) {
					m_skullAnimation->JumpToLastFrame();
				}
			}
			m_currentTally = newTally;

			unsigned int overFlowTally = m_currentTally % 5;
			unsigned int fullTallys = (m_currentTally - overFlowTally) / 5;

			float currentX = m_tallyStartPositionFromTopLeft.x;
			for (size_t i = 0; i < fullTallys; i++)
			{
				m_2dRenderer->setUVRect(//this gets the potition of the current frame on the image
					(m_tallyFrameSize.x * 4) / m_tallyCounterTexture->getWidth(),
					0,
					m_tallyFrameSize.x / m_tallyCounterTexture->getWidth(),
					m_tallyFrameSize.y / m_tallyCounterTexture->getHeight()
				);
				m_2dRenderer->drawSprite(m_tallyCounterTexture, currentX, windowHeight - m_tallyStartPositionFromTopLeft.y, m_tallySize.x, m_tallySize.y, 0, 0, 0.0f, 1.f);
				currentX += m_tallySize.x + m_tallySpacing;
			}

			if (overFlowTally > 0) {
				m_2dRenderer->setUVRect(//this gets the potition of the current frame on the image
					(m_tallyFrameSize.x * (overFlowTally - 1)) / m_tallyCounterTexture->getWidth(),
					0,
					m_tallyFrameSize.x / m_tallyCounterTexture->getWidth(),
					m_tallyFrameSize.y / m_tallyCounterTexture->getHeight()
				);
				m_2dRenderer->drawSprite(m_tallyCounterTexture, currentX, windowHeight - m_tallyStartPositionFromTopLeft.y, m_tallySize.x, m_tallySize.y, 0, 0, 0.0f, 1.f);
			}
			m_2dRenderer->setUVRect(0, 0, 1, 1);

			if (m_skullAnimation != nullptr) {
				m_skullAnimation->gameObject->transform.position = Vector2(m_skullStartPositionFromTopLeft.x, windowHeight - m_skullStartPositionFromTopLeft.y);
				m_skullAnimation->Draw(m_2dRenderer);
			}

			if (m_ClockAnimation != nullptr) {
				m_ClockAnimation->gameObject->transform.position = Vector2(m_ClockStartPositionFromTopLeft.x, windowHeight - m_ClockStartPositionFromTopLeft.y);
				m_ClockAnimation->Draw(m_2dRenderer);

				char time[32];
				sprintf_s(time,"%0.1f", m_time);
				m_2dRenderer->setRenderColour(0.6f, 0.6f, 0.6f, 0.5f);
				m_2dRenderer->drawText(GetApp()->DefaultFont24(), time, m_ClockStartPositionFromTopLeft.x + 32, windowHeight - m_ClockStartPositionFromTopLeft.y - 32);
				m_2dRenderer->setRenderColour(1.f, 1.f, 1.f, 1.f);

			}
		}
	}
}

void KillTallyUI::SetCharacterManager(CharacterManager* manager) {
	m_charManager = manager;
}
void KillTallyUI::UpdateGameTimeCounter(float time)
{
	m_time = time;
}
void KillTallyUI::SetSkullTexture(const char* textureFileName, unsigned int numFrames, float fps) {
	m_skullAnimation = new AnimatedSprite(textureFileName, numFrames, fps, false, Vector2(128, 128), Vector2(0, 1));
	m_skullAnimation->gameObject = gameObject;
	//m_skullAnimation->SetDepth(10);
}
void KillTallyUI::SetTallyCounterTexture(const char* textureFileName) {
	m_tallyCounterTexture = new aie::Texture(textureFileName);
	m_tallyFrameSize = Vector2(m_tallyCounterTexture->getWidth()/5, m_tallyCounterTexture->getHeight());
}

void KillTallyUI::SetClockTexture(const char* textureFileName) {
	m_ClockAnimation = new AnimatedSprite(textureFileName, 12, 3, true, Vector2(32, 32), Vector2(0, 1));
	m_ClockAnimation->gameObject = gameObject;
}
