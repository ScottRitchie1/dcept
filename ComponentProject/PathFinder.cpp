#include "PathFinder.h"
#include "DefaultController.h"
#include <Input.h>
#include "Application2D.h"

PathFinder::PathFinder() : AI_Behaviour(){
	m_graph = nullptr;
	m_pathFound = false;
	targetNode = nullptr;
	speed = 1;
}
PathFinder::PathFinder(Graph2D *graph) : AI_Behaviour() {
	m_graph = graph;
	m_pathFound = false;
	speed = 1;
}
PathFinder::~PathFinder() {
	for (auto iter = m_Open.begin(); iter != m_Open.end(); iter++)
	{
		delete (*iter);
	}

	for (auto iter = m_Closed.begin(); iter != m_Closed.end(); iter++)
	{
		delete (*iter);
	}
}

void PathFinder::Initialize() {
	if(gameObject->GetComponent<DefaultController>() != nullptr)
		speed = gameObject->GetComponent<DefaultController>()->moveSpeed;
}


void PathFinder::Update(float deltaTime) {
	aie::Input* input = GetApp()->GetInput();
	if (input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_MIDDLE) && m_graph != nullptr) {
		targetNode = nullptr;
		m_path.ClearPath();
		currentPathIndex = 0;


		Graph2D::Node* _startNode = nullptr;
		m_graph->GetClosestNode(gameObject->transform.position, _startNode);

		Graph2D::Node* _endNode = nullptr;
		m_graph->GetClosestNode(Vector2(input->getMouseX(), input->getMouseY()), _endNode);

		if (_endNode != nullptr && _startNode != nullptr) {
			targetNode = _endNode;
			FindPath(_startNode, [this](Graph2D::Node *n) {
				return n == targetNode;
			});

			while (m_pathFound == false) {
				UpdateSearch();
			}
		}
	}

	if (m_path.IsEmpty() == false) {
		auto & _path = m_path.GetPath();

		Vector2 point = _path[currentPathIndex];
		if ((point - gameObject->transform.position).Length() < 10) {
			currentPathIndex++;
			currentPathIndex = currentPathIndex % _path.size();
			point = _path[currentPathIndex];
		}

		Vector2 dirPoint = (point - gameObject->transform.position).normalised();
		gameObject->ApplyForce(dirPoint * speed);
	}
}
void PathFinder::Draw(aie::Renderer2D* m_2dRenderer) {
	auto & _path = m_path.GetPath();

	if (m_path.IsEmpty() == false) {
		Vector2 lastPos;
		for (auto iter = _path.begin(); iter != _path.end(); iter++)
		{
			Vector2 point = (*iter);
			//m_2dRenderer->setRenderColour(0xDCDCDC4D);
			//m_2dRenderer->drawCircle(point.x, point.y, 10.f);
			if (iter != _path.begin()) {
				m_2dRenderer->setRenderColour(0xFF00004D);
				m_2dRenderer->drawLine(lastPos.x, lastPos.y, point.x, point.y, 2.0f);
			}
			lastPos = (*iter);
		}
		m_2dRenderer->setRenderColour(0xFFFFFFFFFF);

	}
}

Graph2D* PathFinder::GetGraph() {
	return m_graph;
}
void PathFinder::SetGraph(Graph2D* graph) {
	m_graph = graph;
}

void PathFinder::FindPath(Graph2D::Node *startNode, std::function<bool(Graph2D::Node *)> goalNodeFunc) {
	PathFindNode *node = new PathFindNode();
	node->node = startNode;
	node->gScore = 0;
	node->parent = nullptr;

	m_Open.push_back(node);

	m_goalNodeFunc = goalNodeFunc;

	m_pathFound = false;
}

bool PathFinder::PathFound() {
	return m_pathFound;
}

void PathFinder::UpdateSearch() {
	if (m_Open.empty() == true) {
		m_pathFound = true;
	}

	if (m_pathFound == false) {
		PathFindNode *node = m_Open.back();
		m_Open.pop_back();
		m_Closed.push_back(node);

		if (m_goalNodeFunc(node->node)) {
			CalculatePath(node);
			m_pathFound = true;
			return;
		}

		auto edges = node->node->connections;
		for (int i = 0; i < edges.size(); i++)
		{
			Graph2D::Edge& edge = edges[i];
			Graph2D::Node* child = edge.to;

			float cost = edge.data;
			float gScore = node->gScore + cost;

			PathFindNode* nodeInList = GetNodeInList(m_Open, child);

			if (nodeInList == nullptr) {
				nodeInList = GetNodeInList(m_Closed, child);
			}
			if (nodeInList == nullptr) {
				PathFindNode* newNode = new PathFindNode();
				newNode->node = child;
				newNode->gScore = gScore;
				newNode->parent = node;

				m_Open.push_back(newNode);
			}
			else {
				if (nodeInList->gScore > gScore) {
					nodeInList->parent = node;
					nodeInList->gScore = gScore;
				}
			}
		}

		m_Open.sort([this](PathFindNode *a, PathFindNode* b) {
			return a->gScore > b->gScore;
		});
	}
}

Path& PathFinder::GetPath() {
	return m_path;
}

PathFinder::PathFindNode* PathFinder::GetNodeInList(std::list<PathFindNode*> &list, Graph2D::Node* node) {
	for (auto iter = list.begin(); iter != list.end(); iter++)
	{
		if ((*iter)->node == node) {
			return (*iter);
		}
	}

	return nullptr;
}


void PathFinder::CalculatePath(PathFindNode* goal) {
	m_path.ClearPath();

	PathFindNode* current = goal;
	while (current != nullptr)
	{
		m_path.PushPathSegment(current->node->data);
		current = current->parent;
	}

	std::reverse(m_path.GetPath().begin(), m_path.GetPath().end());


	m_Open.clear();
	m_Closed.clear();
}