#include "GameOverState.h"
#include "GameState.h"
#include "Application2D.h"
#include "GameStateManager.h"
#include "Font.h"
#include "CharacterManager.h"
#include <string>
#include "Character.h"
#include "Lerp.h"


GameOverState::GameOverState(Application2D *pApp) : IState(pApp) {
	gameState = nullptr;
	menuSpacing = 50.f;
	menuIndex = 0;
	IsMouseSelecting = false;
	Vector2 lastMousePos;
	menuItemFont = nullptr;

	elapsedTime = 0.f;

	backgroundAnimationFinished = false;

	backgroundLerpStart = 0;
	backgroundCurrent = 0.6f;
	backgroundLerpSpeed = 0.2f;
	backgroundTargetY = 0.8f;

	GameOverAnimationFinished = false;
	GameOverLerpStart = 0;
	GameOverCurrent = 0.5f;
	GameOverLerpSpeed = 75;
	GameOverTargetY = 0;

	bodyTextAnimationFinished = false;
	bodyTextFont = nullptr;
	bodyTextLerpStart = 1.;
	bodyTextCurrent = 0.f;
	bodyTextLerpSpeed = .3f;
	bodyTextMax = 0.8f;

	MenuItemAnimationFinished = false;
	MenuItemLerpStart = 0.5f;
	MenuItemCurrentY = 0.5f;
	MenuItemLerpSpeed = 150.f;
	MenuItemTargetY = 0.8f;

	skipAlphaActive = false;
	skipAlphaMin = 0.1f;
	skipAlphaMax = 0.9f;
	skipAlphaCurrent = 0.5f;
	skipAlphaChange = 1.f;
}
GameOverState::~GameOverState() {
}

void GameOverState::OnLoad() {
	gameState = dynamic_cast<GameState*>(GetApp()->GetGameStateManager()->GetState("GameState"));
	bodyTextFont = GetApp()->DefaultFont24();
	menuItemFont = GetApp()->DefaultFont36();

	elapsedTime = 0.f;
	backgroundCurrent = 0.6f;
	bodyTextCurrent = 0.f;
	MenuItemCurrentY = 0.f - menuSpacing;
	GameOverCurrent = GetApp()->getWindowHeight() + 50;

	skipAlphaCurrent = 0.5f;
	skipAlphaChange = 1.f;


	backgroundAnimationFinished = false;
	bodyTextAnimationFinished = false;
	GameOverAnimationFinished = false;
	MenuItemAnimationFinished = false;

	if (gameState != nullptr) {
		Character* hunter = (Character*)gameState->characterManager->GetComponent<CharacterManager>()->GetHunter();

		Character* assassin = gameState->characterManager->GetComponent<CharacterManager>()->GetAssassin()->character;
		if (assassin->gameObject->transform.position.y > hunter->gameObject->transform.position.y) {
			assassin->SetOverrideDepth(-0.5f);
			hunter->SetOverrideDepth(-1);
		}
		else {
			assassin->SetOverrideDepth(-1);
			hunter->SetOverrideDepth(-0.5f);
		}
		
	}
}
void GameOverState::Update(float deltaTime) {
	 
	aie::Input* input = GetApp()->GetInput();
	elapsedTime += deltaTime;

	if (input->wasKeyPressed(aie::INPUT_KEY_SPACE)) {
		backgroundAnimationFinished = true;
		bodyTextAnimationFinished = true;
		GameOverAnimationFinished = true;
		MenuItemAnimationFinished = true;
	}

	if (!backgroundAnimationFinished || !bodyTextAnimationFinished || !GameOverAnimationFinished || !MenuItemAnimationFinished ) {
		if (skipAlphaCurrent > skipAlphaMax || skipAlphaCurrent < skipAlphaMin) {
			skipAlphaChange = -skipAlphaChange;
		}
		skipAlphaCurrent += deltaTime * skipAlphaChange;
		skipAlphaActive = true;
	}
	else {
		skipAlphaActive = false;
	}

	if (elapsedTime > backgroundLerpStart || backgroundAnimationFinished) {
		if (backgroundCurrent < backgroundTargetY && backgroundAnimationFinished == false) {
			backgroundCurrent += deltaTime * backgroundLerpSpeed;
		}
		else {
			backgroundCurrent = backgroundTargetY;
			backgroundAnimationFinished = true;
		}
	}

	if (elapsedTime > bodyTextLerpStart || bodyTextAnimationFinished) {
		if (bodyTextCurrent < bodyTextMax && bodyTextAnimationFinished == false) {
			bodyTextCurrent += deltaTime * bodyTextLerpSpeed;
		}
		else {
			bodyTextCurrent = bodyTextMax;
			bodyTextAnimationFinished = true;
		}
	}

	if (elapsedTime > GameOverLerpStart || GameOverAnimationFinished) {
		if (GameOverCurrent > GameOverTargetY && GameOverAnimationFinished == false) {
			GameOverCurrent -= deltaTime * GameOverLerpSpeed;
		}
		else {
			GameOverCurrent = GameOverTargetY;
			GameOverAnimationFinished = true;
		}
	}

	if (elapsedTime > MenuItemLerpStart || MenuItemAnimationFinished) {
		if (MenuItemCurrentY < MenuItemTargetY && MenuItemAnimationFinished == false) {
			MenuItemCurrentY += deltaTime * MenuItemLerpSpeed;
		}
		else {
			MenuItemCurrentY = MenuItemTargetY;
			MenuItemAnimationFinished = true;
		}
	}
	

	if (input->wasKeyPressed(aie::INPUT_KEY_DOWN)) {
		menuIndex++;
		menuIndex = menuIndex % MENU_ITEMS_COUNT;
	}
	else if (input->wasKeyPressed(aie::INPUT_KEY_UP)) {
		menuIndex--;
		if (menuIndex < 0) {
			menuIndex = MENU_ITEMS_COUNT - 1;
		}
	}

	Vector2 mouseXY = Vector2(input->getMouseX(), input->getMouseY());
	if (mouseXY.DistanceFrom(lastMousePos) > 5) {
		for (int i = 0; i < MENU_ITEMS_COUNT; i++)
		{
			float menuItemBottom = MenuItemCurrentY - (menuSpacing * i) - ((MenuItemTargetY - MenuItemCurrentY) * i);
			float menuItemTop = menuItemBottom + menuItemFont->getStringHeight(menuItems[i]);

			float menuItemLeft = (GetApp()->getWindowWidth() / 2) - (menuItemFont->getStringWidth(menuItems[i]) / 2);
			float menuItemRight = menuItemLeft + (menuItemFont->getStringWidth(menuItems[i]));

			if (mouseXY.y < menuItemTop && mouseXY.y > menuItemBottom
				&& mouseXY.x > menuItemLeft && mouseXY.x < menuItemRight) {
				menuIndex = i;
				IsMouseSelecting = true;
				break;
			}
			else {
				IsMouseSelecting = false;
			}
		}
		lastMousePos = mouseXY;
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_ENTER) || (IsMouseSelecting && input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_LEFT))) {
		switch (menuIndex)
		{
		case 0: {
			if (gameState != nullptr) {
				gameState->RestartGame();
			}
			GetApp()->GetGameStateManager()->Remove("GameOverState");
			return; break; }
		case 1:
		{
			GetApp()->GetGameStateManager()->Remove("GameOverState");
			GetApp()->GetGameStateManager()->Remove("GameState");
			GetApp()->GetGameStateManager()->PushState("MenuNewGameState");
			return; break; }
		default: {
			GetApp()->quit();
			GetApp()->GetGameStateManager()->Remove("GameOverState");
			return; break; }
		}

	}
}
void GameOverState::Draw() {
	auto renderer = GetApp()->GetRender2D();

	float startPosY = (GetApp()->getWindowHeight() / 2) + 150;

	
	renderer->setRenderColour(0, 0, 0, backgroundCurrent);
	renderer->drawBox((GetApp()->getWindowWidth() / 2), (GetApp()->getWindowHeight() / 2), GetApp()->getWindowWidth(), GetApp()->getWindowHeight(), 0, 0);
	renderer->setRenderColour(1.f, 1.f, 1.f, 1.f);
	GameOverTargetY = startPosY;
	renderer->drawText(GetApp()->DefaultFont48(), "GAME OVER", (GetApp()->getWindowWidth() / 2) - (GetApp()->DefaultFont48()->getStringWidth("GAME OVER") / 2), GameOverCurrent);
	if (skipAlphaActive) {
		renderer->setRenderColour(1, 1, 1, skipAlphaCurrent);
		renderer->drawText(GetApp()->DefaultFont24(), "Press Space to Skip", (GetApp()->getWindowWidth()) - (GetApp()->DefaultFont24()->getStringWidth("Press Space to Skip")) - 50, 50);
	}

	std::string str;
	char buffer[32];
	if (gameState != nullptr) {
		renderer->setRenderColour(0.5f, 0.5f, 0.5f, bodyTextCurrent);


		startPosY -= 50;
		auto charManager = gameState->characterManager->GetComponent<CharacterManager>();
		if (charManager != nullptr) {
			str = "The Hunter discovered the assassin after ";
			sprintf_s(buffer, 32, "%0.2f", gameState->playTime);
			str += std::string(buffer);
			str += " seconds,";
			renderer->drawText(bodyTextFont, str.c_str(), (GetApp()->getWindowWidth() / 2) - (bodyTextFont->getStringWidth(str.c_str()) / 2), startPosY);
			startPosY -= bodyTextFont->getStringHeight(str.c_str()) + 10;
			
			if (charManager->GetDeadCivilianCount() > 0) {

				str = "but not before ";
				str += std::to_string(charManager->GetDeadCivilianCount());
				str += " of the assassins ";
				str += std::to_string(charManager->GetCharacterCount());
				str += " targets where killed.";
				renderer->drawText(bodyTextFont, str.c_str(), (GetApp()->getWindowWidth() / 2) - (bodyTextFont->getStringWidth(str.c_str()) / 2), startPosY);
				startPosY -= bodyTextFont->getStringHeight(str.c_str()) + 10;


				if (charManager->GetAssassin()->character->isTagged == true) {
					str = "The hinter did identify ";
					str += std::to_string(charManager->GetTaggedCivilianCount() - 1);
					str += " civilians,";
					renderer->drawText(bodyTextFont, str.c_str(), (GetApp()->getWindowWidth() / 2) - (bodyTextFont->getStringWidth(str.c_str()) / 2), startPosY);
					startPosY -= bodyTextFont->getStringHeight(str.c_str()) + 10;
					str = " but misidentified the assassin as a civilian aswell.";
					renderer->drawText(bodyTextFont, str.c_str(), (GetApp()->getWindowWidth() / 2) - (bodyTextFont->getStringWidth(str.c_str()) / 2), startPosY);

				}
				else {
					str = "The Hunter did successfully identifie ";
					str += std::to_string(charManager->GetTaggedCivilianCount());
					str += " civilians.";
					renderer->drawText(bodyTextFont, str.c_str(), (GetApp()->getWindowWidth() / 2) - (bodyTextFont->getStringWidth(str.c_str()) / 2), startPosY);
				}
			}
			else {
				str = " before they could take out any targets.";
				renderer->drawText(bodyTextFont, str.c_str(), (GetApp()->getWindowWidth() / 2) - (bodyTextFont->getStringWidth(str.c_str()) / 2), startPosY);

			}
			
		}
	}

	MenuItemTargetY = startPosY - 100;
	float temp = MenuItemCurrentY;
	for (int i = 0; i < MENU_ITEMS_COUNT; i++)
	{
		if (menuIndex == i) {
			renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
		}
		else {
			renderer->setRenderColour(0.3f, 0.3f, 0.3f, 0.6f);
		}
		renderer->drawText(menuItemFont, menuItems[i], (GetApp()->getWindowWidth() / 2) - (menuItemFont->getStringWidth(menuItems[i]) / 2), temp - ((MenuItemTargetY - MenuItemCurrentY) * i));
		temp -= menuSpacing;
	}
}