#pragma once
#include "Character.h"
#include "Graph2D.h"
#include "Path.h"
#include "AStarPathFinder.h"

class Civilian : public Character {
public:
	Civilian();
	Civilian(const char* textureFileName, float speed = 50.0f, float idleChance = 0.5f, Vector2 size = { 64, 64 }, Vector2 origin = { 0.5f, 0.0f });
	virtual ~Civilian();

	virtual void Reset();
	virtual void SetGraph(Graph2D* graph);
	virtual void ClearPath();

	virtual void Update(float deltaTime);

	virtual void SetCharacterDeath(bool _isDead);
private:
	Path m_path;
	Graph2D * m_graph;
	float m_idleChance;
	float m_idleTimeMin;
	float m_idleTimeMax;
	float m_curTime;
};
