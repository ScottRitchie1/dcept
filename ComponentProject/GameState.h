#pragma once
#include "IState.h"
#include "GameObject.h"

namespace aie
{
	class Texture;
}

class GameState : public IState
{
public:

	GameState(Application2D *pApp);
	virtual ~GameState();

	void OnLoad();
	virtual void Update(float deltaTime);
	virtual void Draw();


	void RestartGame();
	GameObject * point;
	GameObject * graphObject;
	GameObject * characterManager;
	GameObject * killTally;

	aie::Texture* background;

	float playTime;

protected:
	
private:

};

