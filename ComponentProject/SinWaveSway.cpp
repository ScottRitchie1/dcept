#include "SInWaveSway.h"

SinWaveSway::SinWaveSway() : IComponent() {
	m_totalTime = 0.0f;
}
SinWaveSway::~SinWaveSway() {

}

void SinWaveSway::Update(float deltaTime) {
	m_totalTime += deltaTime;
	
	float value = sinf(m_totalTime) * 5;
	gameObject->transform.position += (gameObject->transform.right * value);
}
void SinWaveSway::Draw(aie::Renderer2D* m_2dRenderer) {

}

void SinWaveSway::OnEnabled() {

}
void SinWaveSway::OnDisabled() {

}