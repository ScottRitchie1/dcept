#include "Rect.h"

Rect::Rect() {
	m_start = Vector2();
	m_size = Vector2(1,1);
}
Rect::Rect(Vector2 start, Vector2 size) {
	m_start = start;
	m_size = size;
}
Rect::~Rect() {

}

bool Rect::IsPointInRect(Vector2 point) {
	return (((point.y > m_start.y) && (point.y < m_start.y + m_size.y)) && ((point.x > m_start.x) && (point.x < m_start.x + m_size.x)));
}
float Rect::Top() {
	return m_start.y + m_size.y;
}
float Rect::Bottom() {
	return m_start.y;
}
float Rect::Right() {
	return m_start.x + m_size.x;
}
float Rect::Left() {
	return m_start.x;
}
Vector2 Rect::Center() {
	return Vector2(m_start.x + (m_size.x*0.5f), m_start.y + (m_size.y*0.5f));
}

Vector2 Rect::StarPos() {
	return m_start;
}
Vector2 Rect::Size() {
	return m_size;
}