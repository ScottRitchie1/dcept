#include "Matrix3.h"
#include <cmath>
#include <assert.h>

Matrix3::Matrix3()
{
	m0 = 1;	m1 = 0; m2 = 0;
	m3 = 0; m4 = 1; m5 = 0;
	m6 = 0; m7 = 0; m8 = 1;
}

Matrix3::~Matrix3()
{

}

Matrix3 Matrix3::MakeIdentity() {
	return Matrix3();
}
Matrix3 Matrix3::MakeTranslation(float x, float y) {
	return Matrix3(1, 0, 0,
		0, 1, 0,
		x, y, 1);
}
Matrix3 Matrix3::MakeScale(float sx, float sy) {
	return Matrix3(sx, 0, 0,
		0, sy, 0,
		0, 0, 1);
}
Matrix3 Matrix3::MakeRotationZ(float radians) {
	float c = cosf(radians);
	float s = sinf(radians);

	return Matrix3(c, s, 0,
		-s, c, 0,
		0, 0, 1);
}

float Matrix3::GetRotation() {
	return std::atan2f(right.y, right.x);
}

void Matrix3::SetRotation(float radians){
	Vector2 curScale = GetScale();

	float c = cosf(radians);
	float s = sinf(radians);

	m3 = c * curScale.x;
	m4 = s * curScale.y;
	m0 = -s * curScale.x;
	m1 = c * curScale.y;
}

Vector2 Matrix3::GetScale() {
	return (Vector2(right.Length(), up.Length()));
}

Matrix3::Matrix3(float M0, float M1, float M2, float M3, float M4, float M5, float M6, float M7, float M8) {
	m0 = M0; m1 = M1; m2 = M2;
	m3 = M3; m4 = M4; m5 = M5;
	m6 = M6; m7 = M7; m8 = M8;
}

Matrix3 Matrix3::operator * (const Matrix3& rhs)const {

	return Matrix3(
		(m0 * rhs.m0) + (m3 * rhs.m1) + (m6 * rhs.m2),
		(m1 * rhs.m0) + (m4 * rhs.m1) + (m7 * rhs.m2),
		(m2 * rhs.m0) + (m5 * rhs.m1) + (m8 * rhs.m2),
		(m0 * rhs.m3) + (m3 * rhs.m4) + (m6 * rhs.m5),
		(m1 * rhs.m3) + (m4 * rhs.m4) + (m7 * rhs.m5),
		(m2 * rhs.m3) + (m5 * rhs.m4) + (m8 * rhs.m5),
		(m0 * rhs.m6) + (m3 * rhs.m7) + (m6 * rhs.m8),
		(m1 * rhs.m6) + (m4 * rhs.m7) + (m7 * rhs.m8),
		(m2 * rhs.m6) + (m5 * rhs.m7) + (m8 * rhs.m8)
	);
}

Vector3 Matrix3::operator * (const Vector3& rhs)const {
	return Vector3(
		(m0 * rhs.x) + (m1 * rhs.y) + (m2 * rhs.z ),
		(m3 * rhs.x) + (m4 * rhs.y) + (m5 * rhs.z),
		(m6 * rhs.x) + (m7 * rhs.y) + (m8 * rhs.z)
	);
}
Matrix3& Matrix3::operator *=(const Matrix3& rhs) {
	*this = *this * rhs;
	return *this;
}

bool Matrix3::operator == (const Matrix3& rhs) const{
	return (xAxis == rhs.xAxis && yAxis == rhs.yAxis && zAxis == rhs.zAxis);
}
bool Matrix3::operator != (const Matrix3& rhs) const {
	return (xAxis != rhs.xAxis || yAxis != rhs.yAxis || zAxis != rhs.zAxis);
}

float& Matrix3::operator [] (const unsigned int index) {

	assert(index <= 9 && "Index out of range");
	return (&m0)[index];
}