#include "Hunter_AI.h"
#include "CharacterManager.h"
#include "Application2D.h"
#include <algorithm>
#include "AStarPathFinder.h"
#include "Assassin.h"

Hunter_AI::Hunter_AI() { Initialize(); }
Hunter_AI::Hunter_AI(const char* textureFileName, float speed, Vector2 size, Vector2 origin) : Hunter(textureFileName, speed, size, origin) { Initialize(); }
Hunter_AI::~Hunter_AI() {
	m_tagQueue.clear();
}

void Hunter_AI::Initialize() {
	AI_Settings.killProbabilityThreshhold = 3;
	AI_Settings.forgetTagTime = 3;
	AI_Settings.checkInaciveTime = 5.f;
	AI_Settings.inaciveTimeBeforeProabilityScalingThreshhold = 7.5f;
	AI_Settings.inaciveProbabilityScaleAmount = 0.25f;
	AI_Settings.actionCoolDown = 0.5f;
	AI_Settings.recheckPopulatedTime = 0.5f;
	AI_Settings.recheckPopulatedLenghtThreshhold = 200;


	m_curForgetTagTime = 0;
	m_curCheckPopTime = 0;
	m_curTagTime = 0;
	m_curActionCooldown = 0;
	m_curRequiredProbabilityThreshhold = 0;
	
	m_tracking = nullptr;
	targetToKill = nullptr;
	targetToTag = nullptr;
	targetToDiscover = nullptr;
}



void Hunter_AI::Update(float deltaTime) {
	if (m_tracking == nullptr && m_characterManager != nullptr) {
		m_tracking = m_characterManager->GetCharList();
	}

	if (targetToKill != nullptr) {
		if (targetToKill->character == nullptr || targetToKill->character->gameObject == nullptr) {
			targetToKill = nullptr;
		}
	}
	if (targetToTag != nullptr) {
		if (targetToTag->character == nullptr || targetToTag->character->gameObject == nullptr) {
			targetToTag = nullptr;
		}
	}


	targetToKill = FindKillTarget();
	//TODO ======== MAKE sure the determine probability is not just based off difference betweent he top 2 but difference between the whole active group's average.
	targetToTag = FindAndUpdateTagQueue();
	targetToDiscover = FindTargetToDiscover(AI_Settings.inaciveProbabilityScaleAmount * deltaTime);
	m_curTagTime -= deltaTime;
	m_curCheckPopTime -= deltaTime;
	m_curActionCooldown -= deltaTime;
	if (m_characterManager->GetActiveCharacterCount() > 0) {
		m_curRequiredProbabilityThreshhold = AI_Settings.killProbabilityThreshhold * (m_characterManager->GetActiveCharacterCount() - 1) / 100;
	}
	else {
		m_curRequiredProbabilityThreshhold = 0;
	}

	

	if (targetToKill != nullptr) {
		AStarPathFinder::CalculatePath(gameObject->transform.position, targetToKill->character->gameObject->transform.position, &m_path, m_graph, true, false);
		OnAICheckAttack();
	}
	else {
		if(targetToTag != nullptr){
			AStarPathFinder::CalculatePath(gameObject->transform.position, targetToTag->character->gameObject->transform.position, &m_path, m_graph, true, false);
			OnAICheckTag();
		}
		else if (targetToDiscover != nullptr) {
			AStarPathFinder::CalculatePath(gameObject->transform.position, targetToDiscover->character->gameObject->transform.position, &m_path, m_graph, true, false);
		}
		else{

			Vector2 newTargetpopulated = FindPopulated();
			if ((newTargetpopulated - m_targetForPopulated).Length() > AI_Settings.recheckPopulatedLenghtThreshhold) {
				if (m_curCheckPopTime <= 0) {
					m_targetForPopulated = newTargetpopulated;
					m_curCheckPopTime = AI_Settings.recheckPopulatedTime;
				}
			}
			else {
				m_targetForPopulated = newTargetpopulated;

			}

			AStarPathFinder::CalculatePath(gameObject->transform.position, m_targetForPopulated, &m_path, m_graph, true, false);
		}
	}

	
	if (m_path.IsEmpty() == false) {
		Vector2 dir = AStarPathFinder::GetCurrentPathPointDirection(&m_path, gameObject->transform.position);
		gameObject->ApplyForce(dir * speed);
	}

	CheckDiscovery();
	UpdateState();
}

void Hunter_AI::Draw(aie::Renderer2D* m_2dRenderer) {
	Hunter::Draw(m_2dRenderer);
	

	if (GetApp()->debugMode) {

		float radius = 50;
		float depth = 10;
		if (m_light != nullptr) {
			radius = (m_light->range / 2) * 1.1f;
			depth = m_light->depth - 1;

			m_2dRenderer->setRenderColour(1.0f, 1.0f, 1.0f, 0.2f);
			m_2dRenderer->drawCircle(m_light->GetWorldCenter().x, m_light->GetWorldCenter().y, radius, depth);
		}
		m_2dRenderer->setRenderColour(0.0f, 0.0f, 1.0f, 0.5f);
		m_2dRenderer->drawCircle(gameObject->WorldTransform().position.x, gameObject->WorldTransform().position.y, attackRange, depth);
		//=====RANGES

		if (targetToKill != nullptr) {//=====KILL
			m_2dRenderer->setRenderColour(1.0f, 0.0f, 0.0f, 0.5f);
			Vector2 pos = targetToKill->character->gameObject->transform.position;
			Vector2 origin = targetToKill->character->GetOrigin();
			Vector2 size = Vector2(targetToKill->character->GetSize().x * targetToKill->character->gameObject->WorldTransform().GetScale().x, targetToKill->character->GetSize().y * targetToKill->character->gameObject->WorldTransform().GetScale().y);
			m_2dRenderer->drawBox(pos.x + (size.x / 2) - (size.x*origin.x), pos.y + (size.y / 2) - (size.y*origin.y), size.x, size.y, 0, depth);
		}
		else //=====POPULATED
		{
			m_2dRenderer->setRenderColour(1.f, 0.5f, 0, 0.2f);
			m_2dRenderer->drawCircle(m_targetForPopulated.x, m_targetForPopulated.y, radius, depth);

			if (targetToTag != nullptr) {//=====TAG TARGET
				m_2dRenderer->setRenderColour(0.0f, 1.0f, 0.0f, 0.5f);
				Vector2 pos = targetToTag->character->gameObject->WorldTransform().position;
				Vector2 origin = targetToTag->character->GetOrigin();
				Vector2 size = Vector2(targetToTag->character->GetSize().x * targetToTag->character->gameObject->transform.GetScale().x, targetToTag->character->GetSize().y * targetToTag->character->gameObject->transform.GetScale().y);
				m_2dRenderer->drawBox(pos.x + (size.x / 2) - (size.x*origin.x), pos.y + (size.y / 2) - (size.y*origin.y), size.x, size.y, 0, depth);
			}
		}

		m_2dRenderer->setRenderColour(0.0f, 1.0f, 0.0f, 1.f);

		//=====TAGS
		for (size_t i = 0; i < m_tagQueue.size(); i++)
		{
 			if ((*m_tagQueue[i]) != nullptr) {
				m_2dRenderer->drawCircle((*m_tagQueue[i])->character->gameObject->transform.position.x, (*m_tagQueue[i])->character->gameObject->transform.position.y, 10, depth);
			}
		}

		//=====PROBABILITIES
		if (m_tracking != nullptr) {
			for (size_t i = 0; i < m_tracking->size(); i++)
			{
				m_2dRenderer->setRenderColour(0, 0, 0, 255);

				if ((*m_tracking)[i]->probability > 0) {
					char buffer[64];
					sprintf(buffer, "%0.2f", (*m_tracking)[i]->probability);
					m_2dRenderer->drawText(GetApp()->DebugFont(), buffer, (*m_tracking)[i]->character->gameObject->transform.position.x, (*m_tracking)[i]->character->gameObject->transform.position.y, depth);
				}
				//else {
				//	m_2dRenderer->drawText(GetApp()->DebugFont(), "-", (*m_tracking)[i]->character->gameObject->transform.position.x, (*m_tracking)[i]->character->gameObject->transform.position.y, depth);
				//
				//}

				//=======INACTIVE
				m_2dRenderer->setRenderColour(0.2f, 0.2f, 0.2f, 255);

				if ((*m_tracking)[i]->inactiveTime > 0) {
					if ((*m_tracking)[i]->inactiveTime > AI_Settings.inaciveTimeBeforeProabilityScalingThreshhold) {
						m_2dRenderer->setRenderColour(1.f, 0.2f, 0.2f, 255);
					}
					else if ((*m_tracking)[i]->inactiveTime > AI_Settings.checkInaciveTime) {
						m_2dRenderer->setRenderColour(0.5f, 0.2f, 0.2f, 255);
					}

					char buffer[64];
					sprintf(buffer, "%0.2f", (*m_tracking)[i]->inactiveTime);
					m_2dRenderer->drawText(GetApp()->DebugFont(), buffer, (*m_tracking)[i]->character->gameObject->transform.position.x, (*m_tracking)[i]->character->gameObject->transform.position.y + 50, depth);

					if ((*m_tracking)[i]->character->isDead) {
						m_2dRenderer->setRenderColour(0.f, 0.f, 0.f, 0.05f);
						float range = (*m_tracking)[i]->inactiveTime * (*m_tracking)[i]->character->speed;
						m_2dRenderer->drawCircle((*m_tracking)[i]->character->gameObject->transform.position.x, (*m_tracking)[i]->character->gameObject->transform.position.y, range, depth);
					}
				}
			}
		}
		m_2dRenderer->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
	}
}



Vector2 Hunter_AI::FindPopulated() {
	CharacterTracking* mostPopulated = nullptr;
	Vector2 averagePosition;

	if (m_tracking != nullptr) {
		for (size_t i = 0; i < m_tracking->size(); i++)
		{
			float radius = 50;
			if (m_light != nullptr) {
				radius = m_light->range * 1.1f;
			}

			int inRangeCount = 0;
			Vector2 tempAveragePosition;

			for (size_t j = 0; j < m_tracking->size(); j++)
			{
				if ((*m_tracking)[j]->probability > 0 && (*m_tracking)[i]->character->isTagged == false) {
					if ((*m_tracking)[i]->character->gameObject->transform.position.DistanceFrom((*m_tracking)[j]->character->gameObject->transform.position) < radius) {
						inRangeCount++;
						tempAveragePosition += (*m_tracking)[j]->character->gameObject->transform.position;
					}
				}
			}

			(*m_tracking)[i]->numNeighbours = inRangeCount;
			if (mostPopulated == nullptr || (*m_tracking)[i]->numNeighbours > mostPopulated->numNeighbours) {
				mostPopulated = (*m_tracking)[i];
				averagePosition = tempAveragePosition * (1.f / (float)inRangeCount);
			}
		}
	}
	return averagePosition;
}

CharacterTracking* Hunter_AI::FindKillTarget() {

	if (m_tracking != nullptr && m_tracking->size() > 1) {

		CharacterTracking* secondHighestProbability = nullptr;
		CharacterTracking* highestProbability = nullptr;

		for (size_t i = 0; i < m_tracking->size(); i++) {
			if (secondHighestProbability == nullptr || (*m_tracking)[i]->probability > secondHighestProbability->probability) {
				if (highestProbability == nullptr || (*m_tracking)[i]->probability > highestProbability->probability) {
					if (highestProbability != nullptr) {
						secondHighestProbability = highestProbability;
					}
					highestProbability = (*m_tracking)[i];

				}
				else {
					secondHighestProbability = (*m_tracking)[i];

				}
			}


		}
		float probabilityDifference = highestProbability->probability;

		if (highestProbability != nullptr) {

			if (secondHighestProbability != nullptr) {
				probabilityDifference = highestProbability->probability - secondHighestProbability->probability;
			}

			if (m_characterManager != nullptr) {

				if (probabilityDifference >= m_curRequiredProbabilityThreshhold) {
					if (targetToKill == nullptr) {
						return highestProbability;
					}
					else {
						if (highestProbability->probability > targetToKill->probability) {
							return highestProbability;
						}
						else if (highestProbability->probability == targetToKill->probability &&
							highestProbability->character->gameObject->WorldTransform().position.DistanceFrom(gameObject->WorldTransform().position)
							< targetToKill->character->gameObject->WorldTransform().position.DistanceFrom(gameObject->WorldTransform().position)) {
							return highestProbability;
						}
						else {
							return targetToKill;
						}
					}
				}
			}
		}

	}
	return nullptr;
}


bool Hunter_AI::AddToTagQueue(CharacterTracking* &toTag) {
	if (toTag->character->isTagged == false && toTag->probability > 0) {
		if (std::find(m_tagQueue.begin(), m_tagQueue.end(), &toTag) == m_tagQueue.end()) {
			m_tagQueue.push_back(&toTag);
			return true;
		}
	}
	return false;
}


CharacterTracking* Hunter_AI::GetClosestToTagTarget() {
	CharacterTracking* charToReturn = nullptr;

	for (size_t i = 0; i < m_tagQueue.size(); i++)
	{
		if (m_tagQueue[i] != nullptr) {
			if (charToReturn == nullptr || (*m_tagQueue[i])->character->gameObject->transform.position.DistanceFrom(gameObject->transform.position) < charToReturn->character->gameObject->transform.position.DistanceFrom(gameObject->transform.position)) {
				charToReturn = (*m_tagQueue[i]);
			}
		}
	}

	return charToReturn;
}

CharacterTracking* Hunter_AI::FindAndUpdateTagQueue() {
	
	for (size_t i = 0; i < m_tagQueue.size(); i++)
	{
		if ((*m_tagQueue[i]) == nullptr) {
			auto iterBecasueICantUseAPointerAparently = std::find(m_tagQueue.begin(), m_tagQueue.end(), m_tagQueue[i]);
			m_tagQueue.erase(iterBecasueICantUseAPointerAparently);

		}
	}

	if (m_tagQueue.size() > 0) 
	{
		std::sort(m_tagQueue.begin(), m_tagQueue.end(), [this](CharacterTracking **a, CharacterTracking** b) {
			return ((*a)->character->gameObject->transform.position.DistanceFrom
			(gameObject->transform.position) > 
				(*b)->character->gameObject->transform.position.DistanceFrom
				(gameObject->transform.position));
		});//sort by furthest away

		if (m_curTagTime <= 0 && m_tagQueue.size() > 1) {
			m_curTagTime = AI_Settings.forgetTagTime / m_tagQueue.size();
			m_tagQueue.front() = std::move(m_tagQueue.back());
			m_tagQueue.pop_back();
		}
		
		return (*m_tagQueue.back());
	}
	else {
		return nullptr;
	}
}

CharacterTracking* Hunter_AI::FindTargetToDiscover(float scaleAmount) {

	CharacterTracking* highestInactiveTime = nullptr;

	if (m_tracking != nullptr) {

		for (size_t i = 0; i < m_tracking->size(); i++)
		{
			if ((*m_tracking)[i]->character->isDead == false || (*m_tracking)[i]->isDiscovered == false) {
				if ((*m_tracking)[i]->inactiveTime > AI_Settings.checkInaciveTime) {
					if ((*m_tracking)[i]->character->isTagged == false &&m_characterManager != nullptr && (*m_tracking)[i]->inactiveTime > AI_Settings.inaciveTimeBeforeProabilityScalingThreshhold) {
						m_characterManager->DistributeProbability((*m_tracking)[i], scaleAmount);
					}
					
					if (highestInactiveTime == nullptr || (*m_tracking)[i]->inactiveTime > highestInactiveTime->inactiveTime) {
						highestInactiveTime = (*m_tracking)[i];
					}
				}
			}
		}
	}
	return highestInactiveTime;
}


void Hunter_AI::TagAllInLight() {
	if (m_light != nullptr && m_tracking != nullptr) {
		for (size_t i = 0; i < m_tracking->size(); i++)
		{
			if ((*m_tracking)[i]->character->gameObject->WorldTransform().position.DistanceFrom(m_light->GetWorldCenter()) < m_light->range / 2 && isTagged == false) {
				AddToTagQueue((*m_tracking)[i]);
			}
		}
	}
}

void Hunter_AI::OnAICheckAttack() {
	if (m_curActionCooldown <= 0) {
		if (targetToKill != nullptr) {
			if ((targetToKill->character->gameObject->transform.position - gameObject->transform.position).Length() < attackRange) {
				if (m_characterManager != nullptr) {
					m_characterManager->DistributeProbability(targetToKill);
				}
				targetToKill->character->SetCharacterDeath(true);
				targetToKill->isDiscovered = true;
				
				m_curActionCooldown = AI_Settings.actionCoolDown;

				auto targetInTagQueue = std::find(m_tagQueue.begin(), m_tagQueue.end(), &targetToKill);
				if (targetInTagQueue != m_tagQueue.end()) {
					m_tagQueue.erase(targetInTagQueue);
				}
			}
		}
	}
}

void Hunter_AI::OnAICheckTag() {
	if (m_curActionCooldown <= 0) {
		if (targetToTag != nullptr) {
			if ((targetToTag->character->gameObject->transform.position - gameObject->transform.position).Length() < tagRange) {
				if (m_characterManager != nullptr) {
					m_characterManager->DistributeProbability(targetToTag);
				}
				targetToTag->character->isTagged = true;
				m_tagQueue.pop_back();
				targetToTag = nullptr;
				m_curActionCooldown = AI_Settings.actionCoolDown;
			}
		}
	}
}

void Hunter_AI::CheckDiscovery()
{
	if (m_tracking != nullptr && m_light != nullptr) {
		for (size_t i = 0; i < m_tracking->size(); i++)
		{
			if ((*m_tracking)[i]->character->isDead && (*m_tracking)[i]->isDiscovered == false && (*m_tracking)[i]->character->GetCurrentAnimation() != nullptr) {
				Vector2 point = (*m_tracking)[i]->character->GetCurrentAnimation()->GetCenterPosition();
				if (point.DistanceFrom(m_light->GetWorldCenter()) < m_light->range/2) {
					OnDiscoverDead((*m_tracking)[i]);
				}
			}
		}
	}
}

void Hunter_AI::OnDiscoverDead(CharacterTracking* deadCharacter) {
	if (deadCharacter != nullptr) {
		if (deadCharacter->isDiscovered == false && deadCharacter->character->isDead == true) {

			deadCharacter->isDiscovered = true;
			deadCharacter->character->SetCharacterDeath(true);

			if (m_characterManager != nullptr && m_characterManager->GetAssassin() != nullptr) {
				auto assassinAttackRange = (dynamic_cast<Assassin*>(m_characterManager->GetAssassin()->character)->attackRange);
				float range = (deadCharacter->inactiveTime * deadCharacter->character->speed) + (assassinAttackRange * 2);

				std::vector<CharacterTracking*>* withinRange = m_characterManager->GetCharatersWithinRange(deadCharacter->character->gameObject->transform.position, range);
				std::vector<CharacterTracking*>* OutsideRange = m_characterManager->GetCharatersOutsideRange(deadCharacter->character->gameObject->transform.position, range);


				if (withinRange->size() > 0 && (targetToKill == nullptr || targetToKill != deadCharacter)) {
					m_characterManager->DistributeProbability(deadCharacter, 0, withinRange);
					if (withinRange->size() == 1) {
						float prob = m_characterManager->GetHighestProbability()->probability + (m_curRequiredProbabilityThreshhold*1.1f) - (*withinRange)[0]->probability;
						m_characterManager->DistributeProbability((*withinRange)[0], prob);
					}

				}else {
					m_characterManager->DistributeProbability(deadCharacter);
				}

				for (size_t i = 0; i < OutsideRange->size(); i++)
				{
					AddToTagQueue((*OutsideRange)[i]);
				}
			}
			deadCharacter->inactiveTime = 0;
			auto targetInTagQueue = std::find(m_tagQueue.begin(), m_tagQueue.end(), &deadCharacter);
			if (targetInTagQueue != m_tagQueue.end()) {
				m_tagQueue.erase(targetInTagQueue);
			}
		}
	}
}

void Hunter_AI::OnNewDeath(CharacterTracking* deadCharacter) {
	if (m_characterManager != nullptr && m_light != nullptr && deadCharacter != nullptr) {
		std::vector<CharacterTracking*>* inRangeOfLight = m_characterManager->GetCharatersWithinRange(m_light->GetWorldCenter(), m_light->range / 2);

		float range = ((dynamic_cast<Assassin*>(m_characterManager->GetAssassin()->character)->attackRange) * 2);
		std::vector<CharacterTracking*>* inRangeOfDeath = m_characterManager->GetCharatersWithinRange(deadCharacter->character->gameObject->transform.position, range);

		for (size_t i = 0; i < inRangeOfLight->size(); i++)
		{
			bool contained = false;
			for (size_t j = 0; j < inRangeOfDeath->size(); j++)
			{
				if ((*inRangeOfLight)[i] == (*inRangeOfDeath)[j]) {
					contained = true;
				}
			}

			if (contained == false) {
				AddToTagQueue((*inRangeOfLight)[i]);
			}
		}

		inRangeOfDeath->clear();
		inRangeOfLight->clear();
	}
}


void Hunter_AI::ClearHunterTargets()
{
	Hunter::ClearHunterTargets();
	m_curForgetTagTime = 0;
	m_curCheckPopTime = 0;
	m_curTagTime = 0;
	m_curActionCooldown = 0;
	m_curRequiredProbabilityThreshhold = 0;
	m_tracking = nullptr;
	m_tagQueue.clear();
}

float Hunter_AI::GetCurrentRequiredProbabilityThreshhold() {
	return m_curRequiredProbabilityThreshhold;
}

