#pragma once
#include "IComponent.h"
#include "Renderer2D.h"
#include "Texture.h"
#include <string>
#include "Vec2.h"

class Sprite : public IComponent {
public:
	Sprite();
	Sprite(const char* textureFileName, Vector2 size = Vector2(32.0f,32.0f), Vector2 origin = Vector2(0.5f, 0.5f) );
	virtual ~Sprite();

	static Vector2 GetClosestPointOnSprite(Sprite* sprite, Vector2 to);

	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	void SetTexture(const char* textureFileName);

	void SetDepth(float depth = 0.0f);

	void SetTextureSize(Vector2 size);
	Vector2 GetTextureSize();

	void SetTextureOrigin(Vector2 origin);
	Vector2 GetTextureOrigin();

	Vector2 GetCenterPosition();

protected:
	aie::Texture*		m_texture;

	Vector2 m_size;
	Vector2 m_Origin;
	float m_depth;
private:
};




class AnimatedSprite : public Sprite
{
public:
	float fps;
	bool loop;

	AnimatedSprite();
	AnimatedSprite(const char* textureFileName, unsigned int numFrames, float fps = 1.0f, bool loop = true, Vector2 size = Vector2(32.0f, 32.0f), Vector2 origin = Vector2(0.5f, 0.5f));
	virtual ~AnimatedSprite();

	void BuildSpriteAnimation(const char* textureFileName, unsigned int numFrames, float fps = 1.0f, bool loop = true, Vector2 size = Vector2(32.0f, 32.0f), Vector2 origin = Vector2(0.5f, 0.5f));

	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	bool isValid();
	void JumpToFirstFrame();
	void JumpToLastFrame();

protected:
	Vector2 m_frameSize;

	int m_numFrames;

	unsigned int m_currentFrame;

	

	float m_currentFrameTime;

	

private:

};

AnimatedSprite* CreateSpriteAnimationPointer(const char* textureFileName, unsigned int numFrames, float fps = 1.0f, bool loop = true, Vector2 size = Vector2(32.0f, 32.0f), Vector2 origin = Vector2(0.5f, 0.5f));
