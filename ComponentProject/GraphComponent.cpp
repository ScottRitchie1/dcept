#include "GraphComponent.h"
#include <iostream>
#include <algorithm>
#include "Application2D.h"
#include <Input.h>

GraphComponent::GraphComponent() {
	SetGraph(new Graph2D());
}
GraphComponent::~GraphComponent() {
	delete m_graph;
}

void GraphComponent::Update(float deltaTime) {
	aie::Input* input = gameObject->GetApp()->GetInput();
	if (editable && GetApp()->debugMode) {
		if (input->wasKeyPressed(aie::INPUT_KEY_KP_ADD)) {
			auto node = m_graph->AddNode(Vector2(input->getMouseX(), input->getMouseY()));
			std::vector<Graph2D::Node*> surroundingNodes;
			m_graph->GetNearbyNodes(Vector2(input->getMouseX(), input->getMouseY()), 75, surroundingNodes);

			for (auto iter = surroundingNodes.begin(); iter != surroundingNodes.end(); iter++)
			{
				if ((*iter) == node) {
					continue;
				}
				float dist = (node->data - (*iter)->data).Length();
				m_graph->AddEdge(node, (*iter), dist, true);
			}
		}else if (input->wasKeyPressed(aie::INPUT_KEY_KP_SUBTRACT)) {
			Graph2D::Node* node = nullptr;
			m_graph->GetClosestNode(Vector2(input->getMouseX(), input->getMouseY()), node);

			if (node != nullptr) {
				m_graph->RemoveNode(node);
			}
		}
	}
}
void GraphComponent::Draw(aie::Renderer2D* m_2dRenderer) {
	if (GetApp()->debugMode) {
		auto &nodes = m_graph->GetNodes();

		for (auto iter = nodes.begin(); iter != nodes.end(); iter++)
		{
			//m_2dRenderer->setRenderColour(0x32CD324D);
			Graph2D::Node* node = (*iter);
			//m_2dRenderer->drawCircle(node->data.x, node->data.y, 8, 9);

			auto &edges = node->connections;
			
			for (auto edgeIter = edges.begin(); edgeIter != edges.end(); edgeIter++)
			{
				float gScore = ((*edgeIter).data * (*edgeIter).dataMultiplier) / 255;
				if (gScore > 1) {
					gScore = 1.0f;
				}
				m_2dRenderer->setRenderColour(gScore, 1.0f, gScore, 1.f);
				Graph2D::Node* to = (*edgeIter).to;
				m_2dRenderer->drawLine(node->data.x, node->data.y, to->data.x, to->data.y, 2.f, 9-gScore);
			}
		}
		m_2dRenderer->setRenderColour(0xFFFFFFFF);

		if (editable) {
			aie::Input* input = gameObject->GetApp()->GetInput();
			std::vector<Graph2D::Node*> surroundingNodes;
			m_graph->GetNearbyNodes(Vector2(input->getMouseX(), input->getMouseY()), 75, surroundingNodes);
			m_2dRenderer->setRenderColour(0x000000FF);
			for (auto iter = surroundingNodes.begin(); iter != surroundingNodes.end(); iter++)
			{
				m_2dRenderer->drawLine(input->getMouseX(), input->getMouseY(), (*iter)->data.x, (*iter)->data.y, 1.0f);
			}
		}
	}
}

Graph2D* GraphComponent::GetGraph() {
	return m_graph;
}
void GraphComponent::SetGraph(Graph2D* graph) {
	m_graph = graph;
}

bool GraphComponent::ClearNodeDataMultipliers()
{
	if (m_graph != nullptr) {
		auto &nodes = m_graph->GetNodes();

		for (auto iter = nodes.begin(); iter != nodes.end(); iter++)
		{
			auto &edges = (*iter)->connections;

			for (auto edgeIter = edges.begin(); edgeIter != edges.end(); edgeIter++)
			{
				(*edgeIter).dataMultiplier = 1;
			}
		}
	}
	return false;
}
