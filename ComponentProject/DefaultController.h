#pragma once
#include "IComponent.h"

class DefaultController : public IComponent {
public:
	DefaultController(int up = 0, int down = 0, int right = 0, int left = 0, int tRight = 0, int tLeft = 0);
	virtual ~DefaultController();

	virtual void Update(float deltaTime);

	void SetKeys(int up = 0, int down = 0, int right = 0, int left = 0, int tRight = 0, int tLeft = 0);

	bool IsUsingInputs();

	float moveSpeed;
	float rotSpeed;

	int upKey;
	int downKey;
	int rightKey;
	int leftKey;

	int turnRight;
	int turnLeft;

protected:
private:
	bool m_usingInputs;
};
