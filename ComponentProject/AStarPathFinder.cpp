#include "AStarPathFinder.h"

void AStarPathFinder::DrawPath(Path* path, aie::Renderer2D* m_2dRenderer) {
	auto & _path = path->GetPath();

	if (path != nullptr && path->IsEmpty() == false) {
		Vector2 lastPos;
		for (auto iter = _path.begin(); iter != _path.end(); iter++)
		{
			Vector2 point = (*iter);
			if (iter != _path.begin()) {
				m_2dRenderer->setRenderColour(0xFF00004D);
				m_2dRenderer->drawLine(lastPos.x, lastPos.y, point.x, point.y, 5.0f);
			}
			lastPos = (*iter);
		}
		m_2dRenderer->setRenderColour(0xFFFFFFFFFF);

	}
}
bool AStarPathFinder::CalculatePath(Vector2 startPoint, Vector2 endPoint, Path* path, Graph2D* graph, bool includeEndAsNodes ,bool useDataMultipliers) {
	if (path != nullptr && graph != nullptr) {
		std::list<PathFindNode*> m_Open;
		std::list<PathFindNode*> m_Closed;

		path->ClearPath();


		Graph2D::Node* startNode = nullptr;
		graph->GetClosestNode(startPoint, startNode);

		Graph2D::Node* endNode = nullptr;
		graph->GetClosestNode(endPoint, endNode);

		if (endNode != nullptr && startNode != nullptr && startNode != endNode) {
			PathFindNode *node = new PathFindNode();
			node->node = startNode;
			node->gScore = 0;
			node->parent = nullptr;
			m_Open.push_back(node);
			path->m_succsessful = false;

			while (path->wasPathSuccsessful() == false) {
				if (m_Open.empty() == true) {
					path->m_succsessful = true;
				}

				if (path->wasPathSuccsessful() == false) {
					PathFindNode *node = m_Open.back();
					m_Open.pop_back();
					m_Closed.push_back(node);

					if (node->node == endNode) {
						path->ClearPath();
						

						if (includeEndAsNodes){
							path->PushPathSegment(endPoint);
							node = node->parent;
						}
						while (node != nullptr)
						{
							path->PushPathSegment(node->node->data);
							node = node->parent;
						}

						if (path->GetPath()[0].Dot(path->GetPath()[1]) > 0) {
							path->GetPath().pop_back();//we dont want to move backwards to get to the first point, make sure yo only move forwards
						}

						std::reverse(path->GetPath().begin(), path->GetPath().end());


						m_Open.clear();
						m_Closed.clear();

						path->m_succsessful = true;
						return path->m_succsessful;
					}

					auto edges = node->node->connections;
					for (int i = 0; i < edges.size(); i++)
					{
						Graph2D::Edge& edge = edges[i];
						Graph2D::Node* child = edge.to;

						float cost;
						if (useDataMultipliers) {
							cost = edge.data * edge.dataMultiplier;
						}
						else {
							cost = edge.data;
						}
						float gScore = node->gScore + cost;

						PathFindNode* nodeInList = GetNodeInList(m_Open, child);

						if (nodeInList == nullptr) {
							nodeInList = GetNodeInList(m_Closed, child);
						}
						if (nodeInList == nullptr) {
							PathFindNode* newNode = new PathFindNode();
							newNode->node = child;
							newNode->gScore = gScore;
							newNode->parent = node;

							m_Open.push_back(newNode);
						}
						else {
							if (nodeInList->gScore > gScore) {
								nodeInList->parent = node;
								nodeInList->gScore = gScore;
							}
						}
					}

					m_Open.sort([](PathFindNode *a, PathFindNode* b) {
						return a->gScore > b->gScore;
					});
				}
			}
		}
		else {
			return false;
		}
	}
	return false;
}

AStarPathFinder::PathFindNode* AStarPathFinder::GetNodeInList(std::list<PathFindNode*> &list, Graph2D::Node* node) {
	for (auto iter = list.begin(); iter != list.end(); iter++)
	{
		if ((*iter)->node == node) {
			return (*iter);
		}
	}

	return nullptr;
}

Vector2 AStarPathFinder::GetCurrentPathPointDirection(Path* path, Vector2 curPos) {
	Vector2 currentPoint = GetCurrentPathPoint(path);
	if ((currentPoint - curPos).Length() <= path->arrivalRange) {
		path->curIndex++;
		currentPoint = GetCurrentPathPoint(path);
	}

	return (currentPoint - curPos).normalised();
}
Vector2 AStarPathFinder::GetCurrentPathPoint(Path* path) {
	if (path != nullptr && path->IsEmpty() == false) {
		auto & _path = path->GetPath();

		if (path->IsPathComplete() == false) {
			Vector2 point = _path[path->curIndex];
			return point;
		}
		else {
			path->ClearPath();
		}
	}
	return Vector2();
}

void AStarPathFinder::CheckPathAndRemoveBadNodes(Path* path, Vector2 curPos) {//THIS DOESNT WORK. NEED TO REFACTOR SO POP BACK REMOVES FRONT NODE NOT END NODES
	if (path != nullptr && path->GetPath().size() > path->curIndex+1) {
		
		Vector2 toFirstNode = path->GetPath()[path->curIndex];
		Vector2 toSecondNode = path->GetPath()[path->curIndex+1];

		if ((toFirstNode - curPos).Dot((toSecondNode - curPos)) < 0) {
			path->PopPathSegment();
		}
	}
}
