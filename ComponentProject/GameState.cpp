#include "GameState.h"
#include "Application2D.h"
#include "GameStateManager.h"
#include "DebugRender.h"
#include "DefaultController.h"
#include "WrapObject.h"
#include "AI_BehaviourController.h"
#include "AI_Seek.h"
#include "AI_FollowPath.h"
#include "PathSelector.h"
#include "GraphComponent.h"
#include "PathFinder.h"
#include "Sprite.h"
#include "HunterLight.h"
#include "Character.h"
#include "CharacterManager.h";
#include "Hunter_AI.h"
#include "KillTallyUI.h"


GameState::GameState(Application2D *pApp) : IState(pApp) {
	characterManager = new GameObject(this);
	killTally = new GameObject(this);
	background = new aie::Texture("./textures/Background.png");


	//GRAPH;
	graphObject = new GameObject(this);
	Graph2D* _graph = graphObject->AddComponent<GraphComponent>()->GetGraph();
	//_graph->GenerateGraphGrid(14, 25);
	_graph->GenerateGraphFromTexture("./textures/BackgroundNodeMap.png", 720, 25, 50, 0, 0);

	//CHARACTERMANAGER
	auto charMgr = characterManager->AddComponent<CharacterManager>();
	charMgr->SetGraph(_graph);
	charMgr->AddAssassin();
	//charMgr->CreateNewHunter(false);
	//charMgr->SetCivilianCount(20);

	//UI
	auto tallyUI = killTally->AddComponent<KillTallyUI>();
	tallyUI->SetCharacterManager(charMgr);
	tallyUI->SetTallyCounterTexture("./textures/UI/Game/Tally.png");
	tallyUI->SetSkullTexture("./textures/UI/Game/Skull.png", 10, 10);
	tallyUI->SetClockTexture("./textures/UI/Game/Clock.png");






}

GameState::~GameState() {
	delete graphObject;
	delete characterManager;
	delete background;
	delete killTally;
}

void GameState::OnLoad()
{
	auto charMgr = characterManager->GetComponent<CharacterManager>();
	if (charMgr->GetCivilianCount() != GetApp()->gameSettings.CivilianCount) {
		charMgr->SetCivilianCount(GetApp()->gameSettings.CivilianCount);
	}

	if (GetApp()->gameSettings.hunterIsAI != charMgr->IsHunterAI() || charMgr->GetHunter() == nullptr) {
		charMgr->CreateNewHunter(GetApp()->gameSettings.hunterIsAI);
	}
	RestartGame();
}

void GameState::Update(float deltaTime) {
	
	auto assassin = characterManager->GetComponent<CharacterManager>()->GetAssassin();

	


	if (GetApp()->GetGameStateManager()->IsStateRunning("PauseState") || GetApp()->GetGameStateManager()->IsStateRunning("GameOverState") || GetApp()->GetGameStateManager()->IsStateRunning("GameBeginState")) {
		killTally->GetComponent<KillTallyUI>()->SetEnabled(false);
		GetApp()->debugMode = false;
		characterManager->GetComponent<CharacterManager>()->GetHunter()->gameObject->GetComponent<HunterLight>()->SetEnabled(false);
		return;
	}
	else {
		killTally->GetComponent<KillTallyUI>()->SetEnabled(true);
		characterManager->GetComponent<CharacterManager>()->GetHunter()->gameObject->GetComponent<HunterLight>()->SetEnabled(true);
	}

	if (assassin->character->isDead == true) {
		GetApp()->GetGameStateManager()->PushState("GameOverState");
		return;
	}

	if (GetApp()->GetInput()->wasKeyPressed(aie::EInputCodes::INPUT_KEY_ESCAPE)) {
		GetApp()->GetGameStateManager()->PushState("PauseState");
		return;
	}

	playTime += deltaTime;
	killTally->GetComponent<KillTallyUI>()->UpdateGameTimeCounter(playTime);


	if (GetApp()->GetInput()->wasKeyPressed(aie::INPUT_KEY_TAB)) {
		GetApp()->debugMode = !GetApp()->debugMode;
	}

	if (GetApp()->debugMode) {

		if (GetApp()->GetInput()->wasKeyPressed(aie::EInputCodes::INPUT_KEY_1)) {
			characterManager->GetComponent<CharacterManager>()->IncreaseCivilianCount(5);
		}
		else if (GetApp()->GetInput()->wasKeyPressed(aie::EInputCodes::INPUT_KEY_2)) {
			characterManager->GetComponent<CharacterManager>()->IncreaseCivilianCount(-5);
		}

		if (GetApp()->GetInput()->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_LEFT)) {
			characterManager->GetComponent<CharacterManager>()->DistributeProbability(characterManager->GetComponent<CharacterManager>()->FindClosestCivilian(Vector2(GetApp()->GetInput()->getMouseX(), GetApp()->GetInput()->getMouseY())), 0.5f);
		}
		if (GetApp()->GetInput()->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_RIGHT)) {
			auto civ = characterManager->GetComponent<CharacterManager>()->FindClosestCivilian(Vector2(GetApp()->GetInput()->getMouseX(), GetApp()->GetInput()->getMouseY()));
			if (civ != nullptr) {
				civ->character->SetCharacterDeath(true);
				auto hunter = characterManager->GetComponent<CharacterManager>()->GetHunter_AI();
				if (hunter != nullptr) {

					hunter->OnNewDeath(civ);
				}
			}
		}
	}

	graphObject->Update(deltaTime);
	characterManager->Update(deltaTime);
}

void GameState::Draw() {
	//DRAW BACKGROUND
	if (background != nullptr) {
		GetApp()->GetRender2D()->setRenderColour(1, 1, 1, 1);

		GetApp()->GetRender2D()->drawSprite(background, 1280 / 2, 720 / 2, 1280, 720, 0, 10);
	}
	else
	{
		GetApp()->GetRender2D()->setRenderColour(0.5f, 0.5f, 0.5f, 1.0f);
		GetApp()->GetRender2D()->drawBox(GetApp()->getWindowWidth() / 2, GetApp()->getWindowHeight() / 2, GetApp()->getWindowWidth(), GetApp()->getWindowHeight(), 0, 10);
	}
	
	GetApp()->GetRender2D()->setRenderColour(1.f, 1.f, 1.f, 1.0f);
	char fps[32];
	sprintf_s(fps, 32, "FPS: %i", GetApp()->getFPS());
	GetApp()->GetRender2D()->drawText(GetApp()->DebugFont(), fps, 0, 720 - 32);



	graphObject->Draw(GetApp()->GetRender2D());
	characterManager->Draw(GetApp()->GetRender2D());
	killTally->Draw(GetApp()->GetRender2D());
}

void GameState::RestartGame() {
	playTime = 0.f;

	GetApp()->GetGameStateManager()->PushState("GameBeginState");
	
	auto graph = graphObject->GetComponent<GraphComponent>();
	auto charMgr = characterManager->GetComponent<CharacterManager>();

	if (graph != nullptr) {
		graph->ClearNodeDataMultipliers();
	}

	if (charMgr != nullptr) {
		charMgr->ResetCivilians();
		charMgr->ResetAssassin();
		charMgr->ResetHunter();
	}
}