#include "Vec2.h"

Vector2::Vector2() {

	this->x = 0.0f;
	this->y = 0.0f;
}
Vector2::Vector2(float x, float y) {
	this->x = x;
	this->y = y;
}
Vector2::~Vector2() {

}

Vector2 Vector2::Up() {
	return Vector2(0, 1);
}
Vector2 Vector2::Right() {
	return Vector2(1, 0);
}
Vector2 Vector2::Zero() {
	return Vector2();
}

Vector2 Vector2::PerpLeft() const{
	return Vector2(-y, x);
}
Vector2 Vector2::PerpRight() const{
	return Vector2(y, -x);
}

float Vector2::Rotation() const {
	Vector2 _normalised(*this);
	_normalised.Normalise();
	return std::atan2f(_normalised.y, _normalised.x);
}

Vector2 Vector2::RotateAround(Vector2 origin, float raidans) const{
	float c = cos(raidans);
	float s = sin(raidans);
	Vector2 point(x, y);
	
	point.x = c * (x - origin.x) - s * (y - origin.y) + origin.x;
	point.y = s * (x - origin.x) + c * (y - origin.y) + origin.y;

	return Vector2(point.x, point.y);
}

Vector2 Vector2::Rotate(float radians) const {
	return Vector2(cos(radians), sin(radians));
}

float Vector2::AngleBetween(Vector2 &param) {
	Vector2 _normalised1 = param.normalised();
	Vector2 _normalised2 = this->normalised();
	return std::atan2f(_normalised1.y, _normalised1.x) - std::atan2f(_normalised2.y, _normalised2.x);
}

float Vector2::DistanceFrom(Vector2 pos) {
	return (*this - pos).Length();
}

float Vector2::Length() const{
	return sqrt(x*x + y * y);
}

Vector2 Vector2::normalised() {
	Vector2 copy(*this);
	copy.Normalise();
	return copy;
}

Vector2& Vector2::Normalise() {
	float l = this->Length();
	if (l > 0) {
		x /= l;
		y /= l;
	}
	return *this;
}

Vector2 Vector2::GetNormalised() {

	return this->normalised();
}

float Vector2::Dot(const Vector2& rhs) const {
	return x * rhs.x + y * rhs.y;
}


Vector2 Vector2::Vector2::operator * (const float &f) const{
	return Vector2(x * f, y * f);
}

Vector2 Vector2::operator+(const Vector2 &rhs) const {
	return Vector2(x + rhs.x, y + rhs.y);
}

Vector2& Vector2::operator+=(const Vector2 &rhs){
	this->x += rhs.x;
	this->y += rhs.y;

	return *this;
}

Vector2 Vector2::operator-(const Vector2 &rhs) const {
	return Vector2(x - rhs.x, y - rhs.y);
}

Vector2& Vector2::operator-=(const Vector2 &rhs) {
	this->x -= rhs.x;
	this->y -= rhs.y;

	return *this;
}

bool Vector2::operator == (const Vector2 &rhs) const {
	return (x == rhs.x && y == rhs.y);
}

bool Vector2::operator != (const Vector2 &rhs) const{
	return (x != rhs.x || y != rhs.y);
}

Vector2 operator + (const float &lhs, const Vector2 &rhs) {
	return Vector2(rhs.x + lhs, rhs.y + lhs);
}

Vector2 operator - (const float &lhs, const Vector2 &rhs) {
	return Vector2(rhs.x - lhs, rhs.y - lhs);
}

Vector2  Vector2::operator - () const {
	return Vector2(-x, -y);
}


Vector2 operator * (const float &lhs, const Vector2 &rhs){
	return Vector2(rhs.x * lhs, rhs.y * lhs);
}

float Vector2::operator [] (const unsigned int index) const{
	if (index == 0) {
		return x;
	}

	return y;
}