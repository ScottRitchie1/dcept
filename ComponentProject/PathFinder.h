#pragma once
#include "AI_BehaviourController.h"
#include "Graph2D.h"
#include "GraphComponent.h"
#include <functional>
#include <list>
#include "Path.h"

class PathFinder : public AI_Behaviour {
protected:

	struct PathFindNode
	{
		Graph2D::Node *node = nullptr;
		PathFindNode* parent = nullptr;
		float gScore;
	};

public:
	PathFinder();
	PathFinder(Graph2D *graph);
	~PathFinder();

	Graph2D* GetGraph();
	void SetGraph(Graph2D* graph);

	virtual void Initialize();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	void FindPath(Graph2D::Node *startNode, std::function<bool(Graph2D::Node *)> goalNodeFunc);

	bool PathFound();

	void UpdateSearch();

	Path& GetPath();

	float speed;

protected:
	Graph2D * m_graph;

	std::function<bool(Graph2D::Node *)> m_goalNodeFunc;

	bool m_pathFound;

	Path m_path;

	Graph2D::Node* targetNode;

	int currentPathIndex;

	std::list<PathFindNode*> m_Open;
	std::list<PathFindNode*> m_Closed;

private:

	PathFindNode* GetNodeInList(std::list<PathFindNode*> &list, Graph2D::Node* node);

	void CalculatePath(PathFindNode* goal);
};
