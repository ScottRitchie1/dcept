#pragma once
#include "GameObject.h"

class IComponent {
public:
	GameObject * gameObject;

	IComponent();
	virtual ~IComponent();

	void SetEnabled(bool enable);
	bool GetEnabled();

	virtual void OnEnabled();
	virtual void OnDisabled();

	virtual void Initialize();
	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	Application2D *GetApp();

protected:
private:
	bool m_enabled;
};