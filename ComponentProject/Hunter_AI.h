#pragma once
#include "Hunter.h"

struct CharacterTracking;


struct Hunter_AI_Settings
{
	//CORE
	float killProbabilityThreshhold;//the required probability to pursue taget
	float forgetTagTime;//time it takes to remove
	float actionCoolDown;// time before the hunter can perform another action - tag or attack
	float checkInaciveTime;// the time a character can be idles for before been suspected
	float inaciveTimeBeforeProabilityScalingThreshhold;// the time a character can be idles before they start to scale probability

	float inaciveProbabilityScaleAmount;// the amount of probability that is scaled per second after the inactive time has exceeded


	//OTHER
	float recheckPopulatedLenghtThreshhold;//distance from new to old position to determine if we should update our position or keep lerping towards our current
	float recheckPopulatedTime;// the time it takes to recheck for a populated area

};

class Hunter_AI : public Hunter {
public:
	Hunter_AI();
	Hunter_AI(const char* textureFileName, float speed = 50.0f, Vector2 size = { 64, 64 }, Vector2 origin = { 0.5f, 0.0f });
	virtual ~Hunter_AI();

	Hunter_AI_Settings AI_Settings;

	const float INITIAL_PROBABILITY = 1;

	CharacterTracking* targetToDiscover;

	virtual void ClearHunterTargets();

	float GetCurrentRequiredProbabilityThreshhold();
	void OnNewDeath(CharacterTracking* deadCharacter);


private:
	std::vector<CharacterTracking*>* m_tracking;
	std::vector<CharacterTracking**> m_tagQueue;
	float m_curForgetTagTime;
	float m_curCheckPopTime;
	float m_curTagTime;
	float m_curActionCooldown;
	float m_curRequiredProbabilityThreshhold;


	virtual void Initialize();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	Vector2 m_targetForPopulated;

	bool AddToTagQueue(CharacterTracking* &toTag);
	CharacterTracking* GetClosestToTagTarget();

	
	Vector2 FindPopulated();
	CharacterTracking* FindKillTarget();
	CharacterTracking* FindTargetToDiscover(float scaleAmount = 0.1f);
	CharacterTracking* FindAndUpdateTagQueue();
	
	void CheckDiscovery();
	void OnDiscoverDead(CharacterTracking* deadCharacter);

	void TagAllInLight();

	void OnAICheckAttack();
	void OnAICheckTag();
};
