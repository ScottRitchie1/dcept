#include "WrapObject.h"
#include "Application2D.h"

WrapObject::WrapObject() : IComponent(){

}

void WrapObject::Update(float deltaTime) {
	if (gameObject->transform.position.y > GetApp()->getWindowHeight()) {
		gameObject->transform.position.y = 0.0f;
	}
	else if(gameObject->transform.position.y < 0){
		gameObject->transform.position.y = (float)GetApp()->getWindowHeight();
	}

	if (gameObject->transform.position.x > GetApp()->getWindowWidth()) {
		gameObject->transform.position.x = 0.0f;
	}
	else if (gameObject->transform.position.x < 0) {
		gameObject->transform.position.x = (float)GetApp()->getWindowWidth();
	}
}