#pragma once
#include "IComponent.h"
#include "Civilian.h"
#include "Graph2D.h"


class CivilianManager : public IComponent {
public:
	CivilianManager();
	virtual ~CivilianManager();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);
	virtual Civilian* FindClosestCivilian(Vector2 Pos, bool includeDead);
	virtual void SetCivilianCount(unsigned int num);
	std::vector<Civilian*>* GetCivList();
	virtual void IncreaseCivilianCount(int num);
	virtual Civilian* CreateNewCiv();
	virtual void ResetCivilians();

	void SetGraph(Graph2D * graph);

private:
	Graph2D * m_graph;
	std::vector<Civilian*> m_civs;
};