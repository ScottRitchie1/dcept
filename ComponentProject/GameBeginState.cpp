#include "GameBeginState.h"
#include "GameState.h"
#include "Application2D.h"
#include "GameStateManager.h"
#include "Font.h"
#include "CharacterManager.h"
#include <string>
#include "Character.h"
#include "Assassin.h"
#include "Lerp.h"

GameBeginState::GameBeginState(Application2D *pApp) : IState(pApp) {
	m_gameState = nullptr;
	m_assassin = nullptr;
	readyStatusFont = nullptr;
	readyStatusFontSmall = nullptr;
	headingFont = nullptr;
	postReadyStartCountDown = 3.f;
}
GameBeginState::~GameBeginState() {
}

void GameBeginState::OnLoad() {
	m_gameState = dynamic_cast<GameState*>(GetApp()->GetGameStateManager()->GetState("GameState"));
	readyStatusFont = GetApp()->DefaultFont36();
	headingFont = GetApp()->DefaultFont48();
	readyStatusFontSmall = GetApp()->DefaultFont24();
	backgroundCurrent = 0.8f;
	currentVisabilityTimer = 0;
	isAssassinReady = false;
	isHunterReady = false;
	hunterIsAi = false;
	currentpostReadyStartCountDown = postReadyStartCountDown;
	if (m_gameState != nullptr) {
		m_assassin = dynamic_cast<Assassin*>(m_gameState->characterManager->GetComponent<CharacterManager>()->GetAssassin()->character);
		hunterIsAi = (m_gameState->characterManager->GetComponent<CharacterManager>()->GetHunter_AI() != nullptr);
		isHunterReady = hunterIsAi;
	}

	skipAlphaMin = 0.1f;
	skipAlphaMax = 0.9f;
	skipAlphaCurrent = 0.5f;
	skipAlphaChange = 1.f;
}
void GameBeginState::Update(float deltaTime) {
	aie::Input* input = GetApp()->GetInput();
	currentVisabilityTimer -= deltaTime;


	if (skipAlphaCurrent > skipAlphaMax || skipAlphaCurrent < skipAlphaMin) {
		skipAlphaChange = -skipAlphaChange;
		if (skipAlphaCurrent > skipAlphaMax) {
			skipAlphaCurrent = skipAlphaMax;
		}
		else {
			skipAlphaCurrent = skipAlphaMin;
		}
	}
	skipAlphaCurrent += deltaTime * skipAlphaChange;



	if (m_assassin != nullptr) {
		AssassinControls* controls = m_assassin->GetInputs();

		if (isAssassinReady == false && (input->isKeyDown(controls->upKey) || input->isKeyDown(controls->downKey) || input->isKeyDown(controls->leftKey) || input->isKeyDown(controls->rightKey))) {
			currentVisabilityTimer = 0.5f;
		}


		if (currentVisabilityTimer > 0) {
			m_assassin->SetOverrideDepth(-1);
		}
		else {
			m_assassin->SetOverrideDepth();
		}
	}



	if (isAssassinReady == false && input->wasKeyPressed(aie::INPUT_KEY_SPACE)) {
		isAssassinReady = true;
	}

	if (isHunterReady == false && (input->wasMouseButtonPressed(0) || input->wasMouseButtonPressed(1))) {
		isHunterReady = true;
	}

	if (isAssassinReady && isHunterReady) {
		currentpostReadyStartCountDown -= deltaTime;
		backgroundCurrent = Lerp<float>(backgroundCurrent, 0.6f, deltaTime / currentpostReadyStartCountDown);
	}

	if (currentpostReadyStartCountDown < 0) {
		if (m_assassin != nullptr) {
			m_assassin->SetOverrideDepth();
		}
		GetApp()->GetGameStateManager()->Remove("GameBeginState");
	}
}
void GameBeginState::Draw() {
	auto renderer = GetApp()->GetRender2D();
	renderer->setRenderColour(0, 0, 0, backgroundCurrent);
	renderer->drawBox((GetApp()->getWindowWidth() / 2), (GetApp()->getWindowHeight() / 2), GetApp()->getWindowWidth(), GetApp()->getWindowHeight(), 0, 0);
	renderer->setRenderColour(1.f, 1.f, 1.f, 1.f);

	float uiStartY = (GetApp()->getWindowHeight() / 2) + 150;

	std::string str = "STARTING";
	for (size_t i = 0; i < (int)(currentpostReadyStartCountDown *2); i++)
	{
		str += ".";
	}

	if (isAssassinReady && isHunterReady) {
		renderer->setRenderColour(1.f, 1.f, 1.f, skipAlphaCurrent);
		renderer->drawText(headingFont, str.c_str(), (GetApp()->getWindowWidth() / 2) - (headingFont->getStringWidth("STARTING") / 2), uiStartY);
	}
	else {
		renderer->drawText(headingFont, "WAITING", (GetApp()->getWindowWidth() / 2) - (headingFont->getStringWidth("WAITING") / 2), uiStartY);
	}
	if (isAssassinReady == false) {
		renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
		if (!hunterIsAi) {
			renderer->drawText(readyStatusFontSmall, "Oi, Hunter look away so the Assassin can get their bearings. (WASD)", (GetApp()->getWindowWidth() / 2) - (readyStatusFontSmall->getStringWidth("Oi, Hunter look away so the Assassin can get their bearings. (WASD)") / 2), uiStartY - 25);
		}
		else {
			renderer->drawText(readyStatusFontSmall, "Oi, Assassin get your bearings. (WASD)", (GetApp()->getWindowWidth() / 2) - (readyStatusFontSmall->getStringWidth("Oi, Assassin get your bearings. (WASD)") / 2), uiStartY - 25);
		}
	}
	uiStartY -= 100;
	

	if (hunterIsAi == false) {
		if (isHunterReady == false) {
			renderer->setRenderColour(0.5f, 0.5f, 0.5f, 0.8f);
			renderer->drawText(readyStatusFont, "Hunter, you ready?", (GetApp()->getWindowWidth() / 2) - (readyStatusFont->getStringWidth("Hunter, you ready?") / 2), uiStartY);
			renderer->setRenderColour(0.8f, 0.8f, 0.8f, skipAlphaCurrent);
			renderer->drawText(readyStatusFontSmall, "(Press RMD)", (GetApp()->getWindowWidth() / 2) - (readyStatusFontSmall->getStringWidth("(Press RMD)") / 2), uiStartY - 25);
		}
		else {
			renderer->setRenderColour(1.0f, 0.4f, 0.4f, 1.0f);
			renderer->drawText(readyStatusFont, "Hunter is ready!", (GetApp()->getWindowWidth() / 2) - (readyStatusFont->getStringWidth("Hunter is ready!") / 2), uiStartY);
		}
		uiStartY -= 75;
	}

	if (isAssassinReady == false) {
		renderer->setRenderColour(0.5f, 0.5f, 0.5f, 0.8f);
		renderer->drawText(readyStatusFont, "Assassin, you ready to start killin'?", (GetApp()->getWindowWidth() / 2) - (readyStatusFont->getStringWidth("Assassin, you ready to start killin'?") / 2), uiStartY);
		renderer->setRenderColour(0.8f, 0.8f, 0.8f, skipAlphaCurrent);
		renderer->drawText(readyStatusFontSmall, "(Press Space)", (GetApp()->getWindowWidth() / 2) - (readyStatusFontSmall->getStringWidth("(Press Space)") / 2), uiStartY - 25);

	}
	else {
		renderer->setRenderColour(0.4f, 1.0f, 0.4f, 1.0f);
		renderer->drawText(readyStatusFont, "Assassin is ready to kill!", (GetApp()->getWindowWidth() / 2) - (readyStatusFont->getStringWidth("Assassin is ready to kill!") / 2), uiStartY);
	}
}