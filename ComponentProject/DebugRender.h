#pragma once
#include "IComponent.h"

class DebugRenderer : public IComponent {

public:
	DebugRenderer();
	DebugRenderer(const unsigned int _color, float _width = 16.0f, float _height = 16.0f);
	virtual ~DebugRenderer();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	unsigned int color;
	float width;
	float height;

protected:

private:

};
