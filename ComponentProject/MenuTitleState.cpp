#include "MenuTitleState.h"
#include "GameState.h"
#include "Application2D.h"
#include "GameStateManager.h"
#include "Font.h"
#include "Rect.h"
#include <string>

MenuTitleState::MenuTitleState(Application2D *pApp) : IState(pApp) {
	input = nullptr;

	titleFont = nullptr;
	optionFont = nullptr;
	menuSpacing = 25;

}
MenuTitleState::~MenuTitleState() {

}

void MenuTitleState::OnLoad() {
	input = GetApp()->GetInput();
	titleFont = GetApp()->DefaultFont72();
	optionFont = GetApp()->DefaultFont36();

	menuIndex = 0;
}
void MenuTitleState::Update(float deltaTime) {
	titleUIRect = Rect(Vector2((GetApp()->getWindowWidth() / 2) - (titleFont->getStringWidth("DCEPT") / 2), GetApp()->getWindowHeight() * 0.7f), Vector2(titleFont->getStringWidth("DCEPT"), titleFont->getStringHeight("DCEPT")));
	for (size_t i = 0; i < MENU_ITEMS_COUNT; i++)
	{
		MenuRects[i] = Rect(Vector2(0, titleUIRect.Bottom() - 100 - ((optionFont->getStringHeight(menuItems[i]) + menuSpacing) * i)), Vector2(GetApp()->getWindowWidth(), optionFont->getStringHeight(menuItems[i])));
	}

	Vector2 mouseXY = Vector2(input->getMouseX(), input->getMouseY());
	if (mouseXY.DistanceFrom(lastMousePos) > 5) {
		for (int i = 0; i < MENU_ITEMS_COUNT; i++)
		{
			if (MenuRects[i].IsPointInRect(mouseXY)) {
				menuIndex = i;
				IsMouseSelecting = true;
				break;
			}
			else {
				IsMouseSelecting = false;
			}
		}
		lastMousePos = mouseXY;
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_DOWN)) {
		menuIndex++;
		menuIndex = menuIndex % MENU_ITEMS_COUNT;
	}
	else if (input->wasKeyPressed(aie::INPUT_KEY_UP)) {
		menuIndex--;
		if (menuIndex < 0) {
			menuIndex = MENU_ITEMS_COUNT - 1;
		}
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_ENTER) || (IsMouseSelecting && input->wasMouseButtonPressed(0))) {
		switch (menuIndex)
		{
			case 0:
			{
				GetApp()->GetGameStateManager()->Remove("MenuTitleState");
				GetApp()->GetGameStateManager()->PushState("MenuNewGameState");
				break;
			}
			case 1:
			{
				GetApp()->GetGameStateManager()->Remove("MenuTitleState");
				GetApp()->GetGameStateManager()->PushState("MenuHowToPlayState");
				break;
			}
			default: {
				GetApp()->quit();
				break;
			}	
		}
	}
}
void MenuTitleState::Draw() {
	auto renderer = GetApp()->GetRender2D();


	renderer->drawText(titleFont, "DCEPT", titleUIRect.StarPos().x, titleUIRect.StarPos().y, 0);

	for (size_t i = 0; i < MENU_ITEMS_COUNT; i++)
	{
		if (menuIndex == i) {
			renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
		}
		else {
			renderer->setRenderColour(0.6f, 0.6f, 0.6f, 1);
		}
		renderer->drawText(optionFont, menuItems[i], (GetApp()->getWindowWidth() / 2) - (optionFont->getStringWidth(menuItems[i]) / 2), MenuRects[i].StarPos().y, 0);
	}

	renderer->setRenderColour(0.5f, 0.5f, 0.5f, 0.5f);
	renderer->drawText(GetApp()->DefaultFont16(), "Game project by: Scott Ritchie", GetApp()->getWindowWidth() - 250, 25, 0);

}