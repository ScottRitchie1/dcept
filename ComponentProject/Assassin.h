#pragma once
#include "Character.h"
#include <Input.h>

class CharacterManager;

struct AssassinControls
{
	aie::EInputCodes upKey;
	aie::EInputCodes downKey;
	aie::EInputCodes leftKey;
	aie::EInputCodes rightKey;
	aie::EInputCodes attackKey;
};

class Assassin : public Character {
public:
	Assassin();
	Assassin(const char* textureFileName, float speed = 50.0f, Vector2 size = { 64, 64 }, Vector2 origin = { 0.5f, 0.0f });
	virtual ~Assassin();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	void SetInputs(aie::EInputCodes up, aie::EInputCodes down, aie::EInputCodes left, aie::EInputCodes right, aie::EInputCodes attack);
	AssassinControls* GetInputs();
	void SetCharacterManager(CharacterManager* mgr);

	void OnAttack();

	float attackRange;

protected:
	CharacterManager*m_characterManager;

	AssassinControls m_controls;

};
