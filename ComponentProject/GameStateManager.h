#pragma once
#include <map>
#include <vector>

class IState;

class GameStateManager
{
public:


	GameStateManager();
	~GameStateManager();


	void UpdateGameStates(float deltaTime);
	void DrawGameStates();

	void SetState(const char* name, IState *pState);
	void PushState(const char* name);
	void PopState();
	void Remove(const char* name);

	IState* GetTopState();
	IState* GetState(const char* name);
	bool IsStateRunning(const char* name);

private:

	std::map<const char*, IState*> m_registeredStates;
	std::map<const char*, IState*> m_stateStackv2;

	enum class ECommand {
		PUSH,
		POP,
		SET
	};

	struct Command
	{
		ECommand cmd;
		const char *name;
		IState *pState;
	};

	std::vector<Command> m_commands;

	void ProcessCommands();
};

