#include "PointSelector.h"
#include "Application2D.h"
#include <Input.h>

PointSelector::PointSelector() : IComponent (){

}
PointSelector::~PointSelector() {

}

void PointSelector::Update(float deltaTime) {
	aie::Input* input = GetApp()->GetInput();
	if (input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_LEFT)) {
		gameObject->transform.position.x = input->getMouseX();
		gameObject->transform.position.y = input->getMouseY();
		isActive = true;
	}
	else if (input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_RIGHT)) {
		isActive = false;
	}
}
void PointSelector::Draw(aie::Renderer2D* m_2dRenderer) {
	if (visualize && isActive) {
		m_2dRenderer->setRenderColour(0xDCDCDC4D);
		m_2dRenderer->drawCircle(gameObject->transform.position.x, gameObject->transform.position.y, OuterRadius, 10.0f);
		m_2dRenderer->drawCircle(gameObject->transform.position.x, gameObject->transform.position.y, InnerRadius, 10.0f);
		m_2dRenderer->setRenderColour(0xFF0000FF);
		m_2dRenderer->drawCircle(gameObject->transform.position.x, gameObject->transform.position.y, 5.0f, 10.0f);
		m_2dRenderer->setRenderColour(0xFFFFFFFFFF);
	}

}

bool PointSelector::IsPointWithinInnerRadius(Vector2 point) {
	if ((gameObject->transform.position - point).Length() < InnerRadius) return true;
	else return false;
}
bool PointSelector::IsPointWithinOuterRadius(Vector2 point) {
	if ((gameObject->transform.position - point).Length() < OuterRadius) return true;
	else return false;
}