#include "Character.h"
#include "Application2D.h"
#include <limits>

AnimatedSprite*& StateAnimationSet::GetStateDirectionAnim(int dir) {
	//assert(dir > 3 && "Index out of range");
	return (&UpAnim)[dir];
}


AnimatedSprite*& StateAnimationSet::GetStateDirectionAnim(E_Dir dir) {
	return GetStateDirectionAnim((int)dir);
}

Character::Character() : IComponent(){

	InitialiseDefaults();
}
Character::Character(const char* textureFileName, float speed, Vector2 size, Vector2 origin) : IComponent() {

	InitialiseDefaults();

	m_defaultTexture = new aie::Texture(textureFileName);
	this->speed = speed;
	m_defaultSize = size;
	m_defaultOrigin = origin;
}

Character::~Character() {
	for (auto iter = std::begin(StateAnimations); iter != std::end(StateAnimations); iter++)//NOTE we start at 1 index becasue we dont want to initialize the 0 indoex becasue it is the NONE state
	{
		delete (*iter).UpAnim;
		(*iter).UpAnim = nullptr;

		delete (*iter).UpAnim;
		(*iter).DownAnim = nullptr;

		delete (*iter).UpAnim;
		(*iter).LeftAnim = nullptr;

		delete (*iter).UpAnim;
		(*iter).RightAnim = nullptr;
	}

	delete m_defaultTexture;
	m_defaultTexture = nullptr;
}

void Character::Reset()
{
	isTagged = false;
	isDead = false;
	SetOverrideDepth();

	for (auto iter = std::begin(StateAnimations); iter != std::end(StateAnimations); iter++)//NOTE we start at 1 index becasue we dont want to initialize the 0 indoex becasue it is the NONE state
	{
		for (int i = 0; i < 4; i++)
		{
			if ((*iter).GetStateDirectionAnim(i) != nullptr) {
				(*iter).GetStateDirectionAnim(i)->JumpToFirstFrame();
			}
		}
	}
}

void Character::InitialiseDefaults()
{
	m_defaultTexture = nullptr;
	for (auto iter = std::begin(StateAnimations); iter != std::end(StateAnimations); iter++)//NOTE we start at 1 index becasue we dont want to initialize the 0 indoex becasue it is the NONE state
	{
		for (int i = 0; i < 4; i++)
		{
			(*iter).GetStateDirectionAnim(i) = nullptr;
		}
	}

	speed = 10.0f;
	isDead = false;
	isTagged = false;
	currentState = IDLE;
	currentDir = RIGHT;
	m_currentAnimation = StateAnimations[(int)currentState].GetStateDirectionAnim(currentDir);
	m_defaultSize = Vector2(64.f, 64.f);
	m_defaultOrigin = Vector2(0.5f, 0.f);
	overrideDepth = std::numeric_limits<float>::max();
}

void Character::SetAnimation(E_State state, E_Dir dir, const char* textureFileName, unsigned int numFrames, float fps, bool loop) {
	AnimatedSprite* newAnimSprite = new AnimatedSprite(textureFileName, numFrames, fps, loop, m_defaultSize, m_defaultOrigin);
	newAnimSprite->gameObject = gameObject;
	StateAnimations[(int)state].GetStateDirectionAnim(dir) = newAnimSprite;
}

void Character::SetAnimation(E_State state, const char* textureFileName, unsigned int numFrames, float fps, bool loop) {
	AnimatedSprite* newAnimSprite = new AnimatedSprite(textureFileName, numFrames, fps, loop, m_defaultSize, m_defaultOrigin);
	newAnimSprite->gameObject = gameObject;
	StateAnimations[(int)state].UpAnim = newAnimSprite;
	StateAnimations[(int)state].DownAnim = newAnimSprite;
	StateAnimations[(int)state].RightAnim = newAnimSprite;
	StateAnimations[(int)state].LeftAnim = newAnimSprite;
}

void Character::InitializeCharacter(Vector2 startPos, E_State startState) {
	gameObject->transform.position = startPos;
	currentState = startState;
}

void Character::Draw(aie::Renderer2D* m_2dRenderer) {

	if (this->isTagged == true && currentState != DEAD) {
		m_2dRenderer->setRenderColour(0.0f, 1.0f, 0.0f, 1.0f);
	}
	else {
		m_2dRenderer->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
	}


	int stateInt = (int)currentState;
	m_currentAnimation = StateAnimations[stateInt].GetStateDirectionAnim(currentDir);
	float depth;
	if (overrideDepth != std::numeric_limits<float>::max()) {
		depth = overrideDepth;
	}
	else {
		depth = gameObject->transform.position.y / GetApp()->getWindowHeight();
	}
		

	if (m_currentAnimation != nullptr) {// if the state we are in has an animation; ie not -1
		m_currentAnimation->SetDepth(depth);
		m_currentAnimation->Draw(m_2dRenderer);
	}
	else
	{
		Matrix3 worldTransform = gameObject->WorldTransform();
		m_2dRenderer->drawSprite(m_defaultTexture,
			worldTransform.position.x,
			worldTransform.position.y,
			m_defaultSize.x * worldTransform.GetScale().x,
			m_defaultSize.y * worldTransform.GetScale().y,
			worldTransform.GetRotation(),
			depth,
			m_defaultOrigin.x,
			m_defaultOrigin.y);
	}
}
void Character::Update(float deltaTime) {
	UpdateState();
}

void Character::UpdateState() {
	if (isDead) {
		currentState = DEAD;
		//currentDir = DOWN;
	}
	else {
		float highestMoveVelocityDirection = std::fmaxf(std::abs(gameObject->velocity.y), std::abs(gameObject->velocity.x));
		if (highestMoveVelocityDirection > 10.f) {
			currentState = WALK;

			if (std::abs(gameObject->velocity.normalised().x)*1.5f >= std::abs(gameObject->velocity.normalised().y)) {
				if (gameObject->velocity.normalised().x > 0) {
					currentDir = RIGHT;
				}
				else {
					currentDir = LEFT;
				}
			}
			else {
				if (gameObject->velocity.normalised().y > 0) {
					currentDir = UP;
				}
				else
				{
					currentDir = DOWN;
				}
			}
		}
		else {
			currentState = IDLE;
		}
	}
}

void Character::ScaleSpeed(float amount) {
	float speedChange = amount / speed;
	this->speed *= speedChange;

	for (auto iter = std::begin(StateAnimations); iter != std::end(StateAnimations); iter++)//NOTE we start at 1 index becasue we dont want to initialize the 0 indoex becasue it is the NONE state
	{
		for (int i = 0; i < 4; i++)
		{
			if ((*iter).GetStateDirectionAnim(i) != nullptr) {
				(*iter).GetStateDirectionAnim(i)->fps *= speedChange;
			}
		}
	}
}

void Character::SetCharacterDeath(bool _isDead) {
	isDead = _isDead;
	currentState = DEAD;
	currentDir = DOWN;
}

void Character::SetOverrideDepth(float newDepth) {
	overrideDepth = newDepth;
}

