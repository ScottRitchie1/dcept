#include "SplashState.h"
#include "Application2D.h"
#include "GameStateManager.h"

SplashState::SplashState(Application2D *pApp) : IState(pApp) {
	OnLoad();
}

SplashState::~SplashState() {

}

void SplashState::Update(float deltaTime) {
	m_passedTime -= deltaTime;
	if (m_passedTime <= 0.0f) {
		GetApp()->GetGameStateManager()->PopState();
		GetApp()->GetGameStateManager()->PushState("GameState");
	}

}

void SplashState::Draw() {
	char buffer[32];
	sprintf(buffer, "%0.2f", m_passedTime);

	GetApp()->GetRender2D()->drawText(GetApp()->DebugFont(), "Splash Screen", 10, 10);
	GetApp()->GetRender2D()->drawText(GetApp()->DebugFont(), buffer, 300, 10);

}

void SplashState::OnLoad()
{
	m_passedTime = 5.0f;
}
