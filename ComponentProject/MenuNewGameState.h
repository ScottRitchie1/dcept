#pragma once
#include "IState.h"
#include "Rect.h"

namespace aie { class Font; class Input;};

class MenuNewGameState : public IState
{
public:

	MenuNewGameState(Application2D *pApp);
	virtual ~MenuNewGameState();

	void OnLoad();
	virtual void Update(float deltaTime);
	virtual void Draw();

private:
	aie::Input* input;


	aie::Font* titleFont;
	aie::Font* optionFont;

	aie::Font* startFont;
	Rect startUIRect;
	Rect backUIRect;




	Rect civCountRightUIRect;
	Rect civCountLeftUIRect;

	Rect hunterAIToggleUIRect;

	Rect hunterDifficultyUIRect;



	float optionTextToButtonSpacing;
	float optionLineSpacing;


	bool isHunterAI;
	unsigned int minCivilians;
	unsigned int maxCivilians;
	unsigned int currentCivilians;
	unsigned int civiliansIncrement;


	float skipAlphaMax;
	float skipAlphaMin;
	float skipAlphaCurrent;
	float skipAlphaChange;
};