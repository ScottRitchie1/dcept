#pragma once

#include "Matrix3.h"
#include <vector>
#include "IState.h"


class IComponent;
class Application2D;

namespace aie
{
	class Renderer2D;
}

class GameObject {
public:
	GameObject(IState* state);
	virtual ~GameObject();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	void ApplyForce(const Vector2 &force);
	//void SetPosition(Vector2 pos);
	//void SetVelocity(Vector2 vel);
	
	template<typename T, typename... TArgs>
	T* AddComponent(TArgs ...args);

	template<typename T>
	T* GetComponent();
	
	template<typename T>
	T *GetCurrentStateAs() {
		return dynamic_cast<T *>(currentState);
	}

	Matrix3 WorldTransform();
	IState* GetCurrentState();
	Application2D *GetApp();

	bool useForces;
	Matrix3 transform;
	Vector2 velocity;
	Vector2 acceleration;
	float drag;

protected:

	GameObject * m_parent;
	std::vector<IComponent *> m_components;
	
private:

	void AddComponent(IComponent* component);

	IState* currentState;
};

template<typename T, typename... TArgs>
T* GameObject::AddComponent(TArgs ...args) {
	T* component = new T(args...);
	AddComponent(component);
	return component;
}

template<typename T>
T* GameObject::GetComponent() {
	for (auto iter = m_components.begin(); iter != m_components.end(); iter++) {
		T* component = dynamic_cast<T*>(*iter);
		if (component != nullptr) {
			return component;
		}
	}

	return nullptr;
}
