#pragma once
#include "IState.h"
#include "Vec2.h"

namespace aie { class Font; };

class PauseState : public IState
{
public:
	PauseState(Application2D *pApp);
	virtual ~PauseState();

	void OnLoad();
	virtual void Update(float deltaTime);
	virtual void Draw();

private:
	aie::Font* menuItemFont = nullptr;

	float menuSpacing = 50.f;
	int menuIndex = 0;
	const unsigned int MENU_ITEMS_COUNT = 4;
	const char* menuItems[4] = { "Resume", "Restart", "Exit To Menu", "Quit"};

	bool IsMouseSelecting = false;
	Vector2 lastMousePos;

	float menuItemAlphaCurrent;
	float menuItemAlphaMax;
	float menuItemAlphaMin;
	float menuItemAlphaChange;
};