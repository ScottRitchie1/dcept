#include "Hunter.h"
#include "AStarPathFinder.h"
#include "CharacterManager.h"

Hunter::Hunter() : Character() {
	attackRange = 50;
	tagRange = 50;
	m_characterManager = nullptr;
	m_light = nullptr;
	m_graph = nullptr;
	targetToKill = nullptr;
	targetToTag = nullptr;
}
Hunter::Hunter(const char* textureFileName, float speed, Vector2 size, Vector2 origin) : Character(textureFileName, speed, size, origin) {
	this->attackRange = 50;
	this->tagRange = 50;
	m_characterManager = nullptr;
	m_light = nullptr;
	m_graph = nullptr;
	targetToKill = nullptr;
	targetToTag = nullptr;
}
Hunter::~Hunter() {

}

void Hunter::Update(float deltaTime){
	aie::Input* input = GetApp()->GetInput();
	if (input->isMouseButtonDown(moveKey)) {
		if (m_graph != nullptr) {
			AStarPathFinder::CalculatePath(gameObject->transform.position, Vector2(input->getMouseX(), input->getMouseY()), &m_path, m_graph, false, false);
			targetToKill = nullptr;
			targetToTag = nullptr;
		}
	}

	if (m_path.IsEmpty() == false) {
		Vector2 dir = AStarPathFinder::GetCurrentPathPointDirection(&m_path, gameObject->transform.position);
		gameObject->ApplyForce(dir * speed);
	}


	OnAttack();
	OnTag();
	CheckDiscovery();
	UpdateState();
}

void Hunter::Draw(aie::Renderer2D* m_2dRenderer) {
	if (GetApp()->debugMode) {
		AStarPathFinder::DrawPath(&m_path, m_2dRenderer);
	}
	Character::Draw(m_2dRenderer);
}


void Hunter::SetCharacterManager(CharacterManager* mgr) {
	m_characterManager = mgr;
}
void Hunter::SetLight(HunterLight* light) {
	m_light = light;
}

void Hunter::SetInputs(aie::EInputCodes attack, aie::EInputCodes tag, aie::EInputCodes move) {
	attackKey = attack;
	tagKey = tag;
	moveKey = move;
}


void Hunter::OnAttack() {
	aie::Input* input = GetApp()->GetInput();
	if (m_characterManager != nullptr && GetApp()->GetInput()->wasMouseButtonPressed(attackKey)) {
		targetToKill = m_characterManager->GetCharacterAtPoint(Vector2(input->getMouseX(), input->getMouseY()));

		if (targetToKill == nullptr) {
			AStarPathFinder::CalculatePath(gameObject->transform.position, Vector2(input->getMouseX(), input->getMouseY()), &m_path, m_graph, false, false);
		}
	}

	if (targetToKill != nullptr) {
		AStarPathFinder::CalculatePath(gameObject->transform.position, targetToKill->character->gameObject->transform.position, &m_path, m_graph, false, false);

		if (gameObject->transform.position.DistanceFrom(targetToKill->character->gameObject->transform.position) < attackRange) {
			targetToKill->character->SetCharacterDeath(true);
			targetToKill = nullptr;
		}
	}
}
void Hunter::OnTag() {
	aie::Input* input = GetApp()->GetInput();
	if (m_characterManager != nullptr && m_graph != nullptr && GetApp()->GetInput()->wasMouseButtonPressed(tagKey)) {
		targetToTag = m_characterManager->GetCharacterAtPoint(Vector2(input->getMouseX(), input->getMouseY()));

		if (targetToTag == nullptr) {
			AStarPathFinder::CalculatePath(gameObject->transform.position, Vector2(input->getMouseX(), input->getMouseY()), &m_path, m_graph, false, false);
		}
	}

	if (targetToTag != nullptr) {
		AStarPathFinder::CalculatePath(gameObject->transform.position, targetToTag->character->gameObject->transform.position, &m_path, m_graph, false, false);

		if (gameObject->transform.position.DistanceFrom(targetToTag->character->gameObject->transform.position) < tagRange) {
			targetToTag->character->isTagged = true;
			targetToTag = nullptr;
		}
	}
}
void Hunter::CheckDiscovery() {
	if (m_characterManager != nullptr && m_light != nullptr) {
		auto m_tracking = m_characterManager->GetCharList();
		for (size_t i = 0; i < m_tracking->size(); i++)
		{
			if ((*m_tracking)[i]->character->isDead && (*m_tracking)[i]->isDiscovered == false) {
				Vector2 ClosestPositionOnCharacter;
				if ((*m_tracking)[i]->character->gameObject->transform.position.DistanceFrom(m_light->GetWorldCenter()) < m_light->range / 2) {
					(*m_tracking)[i]->isDiscovered = true;
					(*m_tracking)[i]->character->SetCharacterDeath(true);

					if (m_characterManager != nullptr) {
						m_characterManager->DistributeProbability((*m_tracking)[i]);
					}
				}
			}
		}
	}
}

void Hunter::SetGraph(Graph2D* graph) {
	m_graph = graph;
}

void Hunter::ClearHunterTargets()
{
	targetToKill = nullptr;
	targetToTag = nullptr;
	m_path.ClearPath();
}
