#include "Civilian.h"

Civilian::Civilian() : Character(){
	m_idleChance = 0.5f;
	m_idleTimeMin = 3.f;
	m_idleTimeMax = 10.f;
	m_curTime = 0;
}
Civilian::Civilian(const char* textureFileName, float speed, float idleChance, Vector2 size, Vector2 origin) : Character(textureFileName, speed, size, origin){
	m_idleChance = idleChance;
	m_idleTimeMin = 3.f;
	m_idleTimeMax = 10.f;
	m_curTime = 0;
}
Civilian::~Civilian() {}



void Civilian::Update(float deltaTime) {
	Vector2 dir;

	if (isDead == false) {
		bool finishedPathing = m_path.IsPathComplete();
		if (finishedPathing) {
				bool wasIdle = (m_curTime > 0);
				m_curTime -= deltaTime;	
				float randNum = ((float)rand()) / (float)(RAND_MAX);
				if (m_graph == nullptr || ((m_curTime < 0 && wasIdle == false) && randNum < m_idleChance)) {
					randNum = ((float)rand()) / (float)(RAND_MAX);// new rando number for time
					m_curTime = m_idleTimeMin + (m_idleTimeMax - m_idleTimeMin) * randNum;
				}
				else {
					if (m_curTime <= 0) {
						AStarPathFinder::CalculatePath(gameObject->transform.position, m_graph->GetRandomNodePosition(), &m_path, m_graph);
					}
				}
		}
		else {
			if (m_path.IsEmpty() == false) {
				dir = AStarPathFinder::GetCurrentPathPointDirection(&m_path, gameObject->transform.position);
			}
		}

		gameObject->ApplyForce(dir * speed);

	}
	UpdateState();
}

void Civilian::Reset()
{
	Character::Reset();
	ClearPath();
}

void Civilian::SetGraph(Graph2D* graph) {m_graph = graph;}

void Civilian::ClearPath()
{
	m_path.ClearPath();
}


void Civilian::SetCharacterDeath(bool _isDead) {
	if (isDead == false) {
		if (_isDead == true) {
			if (m_graph != nullptr) {
				Graph2D::Node* node;
				m_graph->GetClosestNode(gameObject->transform.position, node);

				for (auto iter = node->connections.begin(); iter != node->connections.end(); iter++)
				{
					(*iter).dataMultiplier = 20;
				}
			}
		}
		isDead = _isDead;
	}
	else
	{
		if (_isDead == false) {
			if (m_graph != nullptr) {
				Graph2D::Node* node;
				m_graph->GetClosestNode(gameObject->transform.position, node);
				for (auto iter = node->connections.begin(); iter != node->connections.end(); iter++)
				{
					(*iter).dataMultiplier = 1;
				}

				//for (auto allNodeIter = node->connections.begin(); allNodeIter != node->connections.end(); allNodeIter++)
				//{
				//	for (auto iter = (*allNodeIter).to->connections.begin(); iter != (*allNodeIter).to->connections.end(); iter++)
				//	{
				//		(*iter).data = ((*allNodeIter).to->data - (*iter).to->data).Length();
				//
				//	}
				//}
			}
		}
		isDead = _isDead;
	}
}

