#include "ExamplePlayerController.h"
#include "SInWaveSway.h"

ExamplePlayerController::ExamplePlayerController() {

}
ExamplePlayerController::~ExamplePlayerController() {

}

void ExamplePlayerController::Update(float deltaTime) {
	aie::Input *input = GetApp()->GetInput();

	if (input->isKeyDown(aie::EInputCodes::INPUT_KEY_LEFT)) {
		gameObject->transform = gameObject->transform * Matrix3::MakeRotationZ(90 * 3.14159f / 180 * deltaTime);
	}
	else if (input->isKeyDown(aie::EInputCodes::INPUT_KEY_RIGHT))
	{
		gameObject->transform = gameObject->transform * Matrix3::MakeRotationZ(-90 * 3.14159f / 180 * deltaTime);
	}

	if (input->wasKeyPressed(aie::EInputCodes::INPUT_KEY_BACKSPACE)) {
		auto sws = gameObject->GetComponent<SinWaveSway>();
		sws->SetEnabled(!sws->GetEnabled());
	}
}
void ExamplePlayerController::Draw(aie::Renderer2D* m_2dRenderer) {

}