#pragma once
#include "IState.h"
#include "Vec2.h"

namespace aie { class Font; };
class GameState;
class Assassin;

class GameBeginState : public IState
{
public:

	GameBeginState(Application2D *pApp);
	virtual ~GameBeginState();

	void OnLoad();
	virtual void Update(float deltaTime);
	virtual void Draw();

private:
	GameState * m_gameState;
	Assassin* m_assassin;
	float backgroundCurrent;
	float currentVisabilityTimer;

	float postReadyStartCountDown;
	float currentpostReadyStartCountDown;

	bool isAssassinReady;
	bool hunterIsAi;
	bool isHunterReady;

	aie::Font* readyStatusFont;
	aie::Font* readyStatusFontSmall;
	aie::Font* headingFont;

	float skipAlphaMax;
	float skipAlphaMin;
	float skipAlphaCurrent;
	float skipAlphaChange;
};
