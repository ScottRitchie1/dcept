#include "IComponent.h"

IComponent::IComponent() {
	gameObject = nullptr;
	m_enabled = true;
}
IComponent::~IComponent() {

}

void IComponent::SetEnabled(bool enable) {
	if (m_enabled != enable) {
		m_enabled = enable;
		if (m_enabled) {
			OnEnabled();
		}
		else {
			OnDisabled();
		}
	}
}
bool IComponent::GetEnabled() {
	return m_enabled;
}

void IComponent::OnEnabled() {

}
void IComponent::OnDisabled() {

}

void IComponent::Initialize() {

}
void IComponent::Update(float deltaTime) {

}
void IComponent::Draw(aie::Renderer2D* m_2dRenderer) {

}

Application2D* IComponent::GetApp() {
	return gameObject->GetApp();
}