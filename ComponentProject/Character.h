#pragma once
#include "IComponent.h"
#include "Sprite.h"
#include <assert.h>

enum E_Dir
{
	UP = 0,
	DOWN,
	LEFT,
	RIGHT,
};

enum E_State
{
	DEAD = 0,
	IDLE,
	WALK,
};

struct StateAnimationSet {
	AnimatedSprite* UpAnim;
	AnimatedSprite* DownAnim;
	AnimatedSprite* LeftAnim;
	AnimatedSprite* RightAnim;

	AnimatedSprite*& GetStateDirectionAnim(E_Dir dir);
	AnimatedSprite*& GetStateDirectionAnim(int dir);
};

class Character : public IComponent {
public:
	bool isTagged = false;
	bool isDead = false;

	float speed;

	E_Dir currentDir;
	E_State currentState;

	StateAnimationSet StateAnimations[3];

	Character();
	Character(const char* textureFileName, float speed = 50.0f, Vector2 size = { 64, 64 }, Vector2 origin = { 0.5f, 0.0f });
	virtual ~Character();

	virtual void Reset();
	
	virtual void SetAnimation(E_State state, E_Dir dir, const char* textureFileName, unsigned int numFrames, float fps = 1.0f, bool loop = true);
	virtual void SetAnimation(E_State state, const char* textureFileName, unsigned int numFrames, float fps = 1.0f, bool loop = true);
	virtual void InitializeCharacter(Vector2 startPos, E_State startState = E_State::IDLE);

	virtual void Draw(aie::Renderer2D* m_2dRenderer);
	virtual void Update(float deltaTime);
	virtual void UpdateState();

	virtual void SetOverrideDepth(float newDepth = std::numeric_limits<float>::max());
	virtual void SetCharacterDeath(bool _isDead);

	Vector2 GetOrigin() { return m_defaultOrigin; }
	Vector2 GetSize() { return m_defaultSize; }
	AnimatedSprite* GetCurrentAnimation() { return m_currentAnimation; }


	void ScaleSpeed(float amount);

protected:

	float overrideDepth;

	void InitialiseDefaults();

	aie::Texture* m_defaultTexture;
	AnimatedSprite* m_currentAnimation;
	Vector2 m_defaultSize;
	Vector2 m_defaultOrigin;
};