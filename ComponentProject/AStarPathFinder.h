#pragma once
#include "Path.h"
#include "Graph2D.h"
#include "Application2D.h"
#include "Vec2.h"
#include <list>



class AStarPathFinder
{
public:

	struct PathFindNode
	{
		Graph2D::Node *node = nullptr;
		PathFindNode* parent = nullptr;
		float gScore;
	};

	static void DrawPath(Path* path, aie::Renderer2D* m_2dRenderer);
	static bool CalculatePath(Vector2 startPoint, Vector2 endPoint, Path* path, Graph2D* graph, bool includeEndAsNodes = false, bool useDataMultipliers = true);
	static void CheckPathAndRemoveBadNodes(Path* path, Vector2 curPos);//this removes nodes that are past our current point
	static Vector2 GetCurrentPathPointDirection(Path* path, Vector2 curPos);
	static Vector2 GetCurrentPathPoint(Path* path);

private:
	static PathFindNode * GetNodeInList(std::list<PathFindNode*> &list, Graph2D::Node* node);

};

