#pragma once
#include "IComponent.h"

class SinWaveSway : public IComponent {
public:
	SinWaveSway();
	virtual ~SinWaveSway();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	virtual void OnEnabled();
	virtual void OnDisabled();

private:
	float m_totalTime;
};
