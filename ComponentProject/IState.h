#pragma once

class Application2D;

class IState {

public:

	IState(Application2D *pApp) : m_pApp(pApp) {}
	virtual ~IState() {}

	virtual void OnLoad() = 0;
	virtual void Update(float deltaTime) = 0;
	virtual void Draw() = 0;

	Application2D *GetApp() { return m_pApp; }

protected:
private:
	Application2D * m_pApp;
};