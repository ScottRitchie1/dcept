#include "AI_FollowPath.h"
#include "DefaultController.h"
#include <Renderer2D.h>


AI_FollowPath::AI_FollowPath() : AI_Behaviour(){
	path = nullptr;
	currentPathIndex = 0;
	speed = 1;
}

AI_FollowPath::AI_FollowPath(Path* p) {
	path = p;
	currentPathIndex = 0;
	speed = 1;
}
AI_FollowPath::~AI_FollowPath() {

}

void AI_FollowPath::Initialize() {
	speed = gameObject->GetComponent<DefaultController>()->moveSpeed;
}

void AI_FollowPath::Update(float deltaTime) {
	if (path->IsEmpty() == false) {
		auto & _path = path->GetPath();

		Vector2 point = _path[currentPathIndex];
		if ((point - gameObject->transform.position).Length() < 10) {
			currentPathIndex++;
			currentPathIndex = currentPathIndex % _path.size();
			point = _path[currentPathIndex];
		}

		

		

		Vector2 dirPoint = (point - gameObject->transform.position).normalised();
		gameObject->ApplyForce(dirPoint * speed);
	}
}
void AI_FollowPath::CheckForBehaviourChange() {

}
void AI_FollowPath::Draw(aie::Renderer2D* m_2dRenderer) {
	if (path != nullptr && path->IsEmpty() == false) {
		m_2dRenderer->setRenderColour(0xFFD7004D);
		auto & _path = path->GetPath();
		m_2dRenderer->drawCircle(_path[currentPathIndex].x, _path[currentPathIndex].y, 10.0f);
		m_2dRenderer->setRenderColour(0xFF4500FF);
		m_2dRenderer->drawLine(_path[currentPathIndex].x, _path[currentPathIndex].y, gameObject->transform.position.x, gameObject->transform.position.y);
		m_2dRenderer->setRenderColour(0xFFFFFFFFFF);

	}
}

Path* AI_FollowPath::GetPath() {
	return path;
}
void AI_FollowPath::SetPath(Path* p) {
	path = p;
}