#pragma once
#include "AI_BehaviourController.h"
#include "PointSelector.h"

class AI_Seek : public AI_Behaviour {
public:
	AI_Seek();
	virtual ~AI_Seek();

	virtual void Initialize();

	virtual void Update(float deltaTime);
	virtual void CheckForBehaviourChange();
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	float weight;
	float speed;
protected:
private:
	PointSelector * point;
};