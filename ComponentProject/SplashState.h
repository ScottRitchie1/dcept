#pragma once
#include "IState.h"

class  SplashState : public IState
{
public:
	SplashState(Application2D *pApp);
	virtual ~SplashState();


	virtual void Update(float deltaTime);
	virtual void Draw();
	virtual void OnLoad();

protected:

	float m_passedTime;
private:

};

