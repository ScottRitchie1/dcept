#pragma once
#include "IComponent.h"
#include "Graph2D.h"

class Character;
class Assassin;
class Civilian;
class Hunter_AI;
class Hunter;

struct CharacterTracking
{
	bool isDiscovered;
	Character* character;
	float probability;
	float inactiveTime;
	unsigned int numNeighbours;
	bool isAssassin;

	void Reset(float defaultProb);
};

class CharacterManager : public IComponent {
public:
	CharacterManager();
	virtual ~CharacterManager();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);
	virtual CharacterTracking* FindClosestCivilian(Vector2 Pos, bool includeDead = false);
	virtual CharacterTracking* GetCharacterAtPoint(Vector2 Pos, bool includeDead = false);

	virtual void AddAssassin();
	std::vector<CharacterTracking*>* GetCharList();
	CharacterTracking* GetAssassin();
	Hunter* GetHunter();
	Hunter_AI* GetHunter_AI();


	virtual void SetCivilianCount(unsigned int num);
	virtual void IncreaseCivilianCount(int num);
	virtual Civilian* CreateNewCivilian();
	virtual Assassin* CreateNewAssassin();
	virtual void CreateNewHunter(bool isAI);


	virtual CharacterTracking* CreateNewCharTracker(bool isAssassin = false, Civilian* civ = nullptr);
	virtual void ResetCivilians();
	virtual void ResetAssassin();
	virtual void ResetHunter();

	void SetGraph(Graph2D * graph);


	void DistributeProbability(CharacterTracking* charToDistribute, float amount = 0.f, std::vector<CharacterTracking*>* list = nullptr);
	void ScaleProbability(CharacterTracking* charToDistribute, float amount);
	unsigned int GetActiveCharacterCount();
	unsigned int GetAliveCharacterCount();
	unsigned int GetCivilianCount();
	unsigned int GetDeadCivilianCount();
	unsigned int GetCharacterCount();
	unsigned int GetTaggedCivilianCount();
	bool IsHunterAI();



	CharacterTracking* GetHighestProbability();
	std::vector<CharacterTracking*>* GetCharatersWithinRange(Vector2 position, float range);
	std::vector<CharacterTracking*>* GetCharatersOutsideRange(Vector2 position, float range);


private:
	const float INITIAL_PROBABILITY = 1;
	const float INITIAL_CIV_SPEED = 50;
	const float INITIAL_CIV_IDLE_CHANCE = 0.5f;

	Graph2D * m_graph;
	std::vector<CharacterTracking*> m_charTracking;
	CharacterTracking* m_assassin;
	Hunter* m_hunter;
};