#include "TestState.h"
#include "Application2D.h"
#include "GameStateManager.h"
#include "Renderer2D.h"
#include "Texture.h"


TestState::TestState(Application2D *pApp) : IState(pApp) {

	m_texture = new aie::Texture("./textures/numbered_grid.tga");
	m_shipTexture = new aie::Texture("./textures/ship.png");
	m_timer = 0;

}

TestState::~TestState() {
	delete m_texture;
	delete m_shipTexture;
}

void TestState::Update(float deltaTime) {

	if (GetApp()->GetGameStateManager()->GetTopState() != this) {
		return;
	}

	if (GetApp()->GetInput()->wasKeyPressed(aie::EInputCodes::INPUT_KEY_P)) {
		GetApp()->GetGameStateManager()->PushState("PauseState");
	}

	aie::Input* input = aie::Input::getInstance();

	m_timer += deltaTime;
	// use arrow keys to move camera
	if (input->isKeyDown(aie::INPUT_KEY_UP))
		GetApp()->m_cameraY += 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_DOWN))
		GetApp()->m_cameraY -= 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_LEFT))
		GetApp()->m_cameraX -= 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
		GetApp()->m_cameraX += 500.0f * deltaTime;

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		GetApp()->quit();

}

void TestState::Draw() {

	aie::Renderer2D* m_2dRenderer = GetApp()->GetRender2D();
	
	// demonstrate animation
	m_2dRenderer->setUVRect(int(m_timer) % 8 / 8.0f, 0, 1.f / 8, 1.f / 8);
	m_2dRenderer->drawSprite(m_texture, 200, 200, 100, 100);

	// demonstrate spinning sprite
	m_2dRenderer->setUVRect(0, 0, 1, 1);
	m_2dRenderer->drawSprite(m_shipTexture, 600, 400, 0, 0, m_timer, 1);

	// draw a thin line
	m_2dRenderer->drawLine(300, 300, 600, 400, 2, 1);

	// draw a moving purple circle
	m_2dRenderer->setRenderColour(1, 0, 1, 1);
	m_2dRenderer->drawCircle(sin(m_timer) * 100 + 600, 150, 50);

	// draw a rotating red box
	m_2dRenderer->setRenderColour(1, 0, 0, 1);
	m_2dRenderer->drawBox(600, 500, 60, 20, m_timer);

	// draw a slightly rotated sprite with no texture, coloured yellow
	m_2dRenderer->setRenderColour(1, 1, 0, 1);
	m_2dRenderer->drawSprite(nullptr, 400, 400, 50, 50, 3.14159f * 0.25f, 1);

	// output some text, uses the last used colour
	char fps[32];
	sprintf_s(fps, 32, "FPS: %i", GetApp()->getFPS());
	m_2dRenderer->drawText(GetApp()->DebugFont(), fps, 0, 720 - 32);
	m_2dRenderer->drawText(GetApp()->DebugFont(), "Press ESC to quit!", 0, 720 - 64);

}