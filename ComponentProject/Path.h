#pragma once
#include <vector>
#include "Vec2.h"

class Path
{
friend class AStarPathFinder;
public:
	Path();
	~Path();

	void PushPathSegment(const Vector2& point);
	void PopPathSegment();

	bool IsEmpty();
	void InvertPath();
	void ResetPathProgress();
	bool IsPathComplete();
	bool wasPathSuccsessful();

	std::vector<Vector2> &GetPath();

	void ClearPath();

	unsigned int curIndex;
	float arrivalRange;
protected:
	std::vector<Vector2> m_pathPoints;
	bool m_succsessful;
};
