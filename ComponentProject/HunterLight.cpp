#include "HunterLight.h"
#include "Application2D.h"

HunterLight::HunterLight() : IComponent(){
	range = 100.0f;
	darknessStrenght = 0.5f;
	m_texture = nullptr;
	offSet = Vector2(0, 0);
	depth = 1.0f;
}

HunterLight::HunterLight(const char* textureFileName, float r, float s, float d, Vector2 _OffSet) : IComponent() {
	m_texture = new aie::Texture(textureFileName);
	range = r;
	darknessStrenght = s;
	offSet = _OffSet;
	depth = d;
}
HunterLight::~HunterLight() {
}

void HunterLight::Draw(aie::Renderer2D* m_2dRenderer) {
	if (!GetApp()->debugMode) {
		m_2dRenderer->setRenderColour(0.0f, 0.0f, 0.0f, darknessStrenght);

		int ww = GetApp()->getWindowWidth();
		int wh = GetApp()->getWindowHeight();

		m_2dRenderer->drawSprite(m_texture,
			gameObject->transform.position.x + offSet.x,
			gameObject->transform.position.y + offSet.y,
			range, range, 0, depth, 0.5f, 0.5f);//Center Light Source

		m_2dRenderer->drawBox(
			(gameObject->transform.position.x - range / 2 + offSet.x) / 2,
			wh / 2,
			gameObject->transform.position.x - (range / 2.0f) + offSet.x,
			wh,
			0, depth);//Left Side Light source Darkness


		m_2dRenderer->drawBox(
			ww / 2 + (gameObject->transform.position.x + range / 2) / 2 + offSet.x*1.5f,
			wh / 2,
			ww - (gameObject->transform.position.x + (range / 2.0f)) + offSet.x,
			wh,
			0, depth);//Right Side Light source Darkness


		m_2dRenderer->drawBox(
			gameObject->transform.position.x + offSet.x,
			wh / 2 + (gameObject->transform.position.y + range / 2.0f + offSet.y) / 2,
			range,
			wh - (gameObject->transform.position.y + (range / 2.0f)) - offSet.y,
			0, depth);//Top Light source Darkness


		m_2dRenderer->drawBox(
			gameObject->transform.position.x + offSet.x,
			(gameObject->transform.position.y - range / 2 + offSet.y) / 2,
			range,
			(gameObject->transform.position.y - (range / 2.0f)) + offSet.y,
			0, depth);//Bottom Light source Darkness

		m_2dRenderer->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
	}
}

Vector2 HunterLight::GetWorldCenter() {
	return (gameObject->WorldTransform().position + offSet);
}