#include "AI_Seek.h"
#include "GameState.h"
#include "DefaultController.h"

AI_Seek::AI_Seek() : AI_Behaviour() {
	point = nullptr;	
	speed = 1;
	weight = 0;
}

AI_Seek::~AI_Seek() {

}

void AI_Seek::Initialize() {
	if (gameObject->GetCurrentStateAs<GameState>()->point) {
		point = gameObject->GetCurrentStateAs<GameState>()->point->GetComponent<PointSelector>();
	}
	speed = gameObject->GetComponent<DefaultController>()->moveSpeed;
}

void AI_Seek::Update(float deltaTime) {
	Vector2 dist = Vector2();
	if (point != nullptr && point->isActive) {
		dist = (point->gameObject->transform.position - gameObject->transform.position).normalised();
		if (weight == 0) {
			weight = 1;
		}
	}

	gameObject->ApplyForce(dist * speed * weight);
}

void AI_Seek::Draw(aie::Renderer2D* m_2dRenderer) {

}


void AI_Seek::CheckForBehaviourChange() {
	if (point != nullptr) {
		if (point->IsPointWithinInnerRadius(gameObject->transform.position) && weight > 0) {
			weight = -weight;
		}
		else if(point->IsPointWithinOuterRadius(gameObject->transform.position) == false && weight < 0){
			weight = 0;
			point->isActive = false;
		}
	}
	else {
		weight = 0;
	}
}