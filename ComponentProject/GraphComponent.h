#pragma once
#include "IComponent.h"
#include "Graph2D.h"


class Graph2D;

class GraphComponent : public IComponent {
public:
	GraphComponent();
	virtual ~GraphComponent();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	Graph2D* GetGraph();
	void SetGraph(Graph2D* graph);
	bool ClearNodeDataMultipliers();

	bool editable = true;

protected:
private:
	Graph2D * m_graph;

};

