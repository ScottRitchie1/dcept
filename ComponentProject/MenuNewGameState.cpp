#include "MenuNewGameState.h"
#include "GameState.h"
#include "GameStateManager.h"
#include "Application2D.h"
#include "Font.h"
#include <string>
#include "Lerp.h"



MenuNewGameState::MenuNewGameState(Application2D *pApp) : IState(pApp) {
	input = nullptr;
	
	titleFont = nullptr;
	optionFont = nullptr;
	startFont = nullptr;
	optionTextToButtonSpacing = 20;
	optionLineSpacing = 60;
	civiliansIncrement = 5;
	minCivilians = 14;
	maxCivilians = 99;
	isHunterAI = false;

	skipAlphaMin = 0.1f;
	skipAlphaMax = 0.9f;
	skipAlphaCurrent = 0.5f;
	skipAlphaChange = 1.f;
}

MenuNewGameState::~MenuNewGameState() {

}

void MenuNewGameState::OnLoad() {
	titleFont = GetApp()->DefaultFont72();
	optionFont = GetApp()->DefaultFont36();
	startFont = GetApp()->DefaultFont48();
	currentCivilians = GetApp()->gameSettings.CivilianCount;

	input = GetApp()->GetInput();

}
void MenuNewGameState::Update(float deltaTime) {

	if (skipAlphaCurrent > skipAlphaMax || skipAlphaCurrent < skipAlphaMin) {
		skipAlphaChange = -skipAlphaChange;
		if (skipAlphaCurrent > skipAlphaMax) {
			skipAlphaCurrent = skipAlphaMax;
		}
		else {
			skipAlphaCurrent = skipAlphaMin;
		}
	}
	skipAlphaCurrent += deltaTime * skipAlphaChange;

	if (input->wasKeyPressed(aie::INPUT_KEY_SPACE) || (hunterAIToggleUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY())) && input->wasMouseButtonPressed(0))) {
		isHunterAI = !isHunterAI;
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_D) || (civCountRightUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY())) && input->wasMouseButtonPressed(0))) {
		currentCivilians += civiliansIncrement;
		if (currentCivilians > maxCivilians) {
			currentCivilians = maxCivilians;
		}
	}
	else if (input->wasKeyPressed(aie::INPUT_KEY_A) || (civCountLeftUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY())) && input->wasMouseButtonPressed(0))) {
		currentCivilians -= civiliansIncrement;
		if (currentCivilians < minCivilians) {
			currentCivilians = minCivilians;
		}
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_ENTER) || (startUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY())) && input->wasMouseButtonPressed(0))) {
		GameSettings* gs = &GetApp()->gameSettings;
		gs->hunterIsAI = isHunterAI;
		gs->CivilianCount = currentCivilians;

		GetApp()->GetGameStateManager()->Remove("MenuNewGameState");
		GetApp()->GetGameStateManager()->PushState("SplashState");
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_BACKSPACE) || (backUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY())) && input->wasMouseButtonPressed(0))) {
		GetApp()->GetGameStateManager()->Remove("MenuNewGameState");
		GetApp()->GetGameStateManager()->PushState("MenuTitleState");
	}
}
void MenuNewGameState::Draw() {
	auto renderer = GetApp()->GetRender2D();
	float currentY = GetApp()->getWindowHeight() - 100;
	float currentX = 150;


	renderer->setRenderColour(1, 1, 1, 1);
	renderer->drawText(titleFont, "NEW GAME", currentX, currentY, 0);
	currentY -= 100;

	

	currentX = 200;
	renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
	renderer->drawText(optionFont, "Number of Assassins: ", currentX, currentY, 0);
	currentX += optionTextToButtonSpacing + optionFont->getStringWidth("Number of Assassins: ");
	renderer->drawText(optionFont, "Not Available ", currentX, currentY, 0);
	currentY -= optionLineSpacing;



	char civCount[32];
	sprintf_s(civCount, 32, "%i", currentCivilians);
	currentX = 200;
	renderer->drawText(optionFont, "Number of Civilians: ", currentX, currentY, 0);
	currentX += optionTextToButtonSpacing + optionFont->getStringWidth("Number of Assassins: ");
	civCountLeftUIRect = Rect(Vector2(currentX, currentY), Vector2(optionFont->getStringWidth("<"), optionFont->getStringHeight("<")));
	if (civCountLeftUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY()))) {
		renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
	}
	else {
		renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
	}
	renderer->drawText(optionFont, "<", currentX, currentY, 0);
	renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
	currentX += 5 + optionFont->getStringWidth("<");
	renderer->drawText(optionFont, civCount, currentX, currentY, 0);
	currentX += 5 + optionFont->getStringWidth(civCount);
	civCountRightUIRect = Rect(Vector2(currentX, currentY), Vector2(optionFont->getStringWidth(">"), optionFont->getStringHeight(">")));
	if (civCountRightUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY()))) {
		renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
	}
	else {
		renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
	}
	renderer->drawText(optionFont, ">", currentX, currentY, 0);
	renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
	currentY -= optionLineSpacing;


	currentX = 200;
	renderer->drawText(optionFont, "Enable AI Hunter: ", currentX, currentY, 0);
	currentX += optionTextToButtonSpacing + optionFont->getStringWidth("Enable AI Hunter: ");
	const char* isAIString = (isHunterAI ? "True" : "False");
	hunterAIToggleUIRect = Rect(Vector2(currentX, currentY), Vector2(optionFont->getStringWidth(isAIString), optionFont->getStringHeight(isAIString)));
	if (hunterAIToggleUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY()))) {
		renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
	}
	else {
		renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
	}
	renderer->drawText(optionFont, isAIString, currentX, currentY, 0);
	renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
	currentY -= optionLineSpacing;

	if (isHunterAI) {
		currentX = 250;
		renderer->drawText(optionFont, "Hunter Difficulty: ", currentX, currentY, 0);
		currentX += optionTextToButtonSpacing + optionFont->getStringWidth("Hunter Difficulty: ");
		const char* aiDifficultyString = "HARD";

		hunterDifficultyUIRect = Rect(Vector2(currentX, currentY), Vector2(optionFont->getStringWidth(aiDifficultyString), optionFont->getStringHeight(aiDifficultyString)));
		if (hunterDifficultyUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY()))) {
			renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
		}
		else {
			renderer->setRenderColour(0.8f, 0.8f, 0.8f, 1);
		}
		renderer->drawText(optionFont, aiDifficultyString, currentX, currentY, 0);
		currentY -= optionLineSpacing;
	}

	if (startUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY()))) {
		renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
		skipAlphaCurrent = 1;
	}
	else {
		renderer->setRenderColour(0.8f, 0.8f, 0.8f, skipAlphaCurrent);
	}

	startUIRect = Rect(Vector2(GetApp()->getWindowWidth() - startFont->getStringWidth("START") - 100, 50), Vector2(startFont->getStringWidth("START"), startFont->getStringHeight("START")));
	renderer->drawText(startFont, "START",startUIRect.StarPos().x, startUIRect.StarPos().y, 0);

	if (backUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY()))) {
		renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
	}
	else {
		renderer->setRenderColour(0.4f, 0.4f, 0.4f, 1.0f);
	}
	backUIRect = Rect(Vector2(100, 50), Vector2(startFont->getStringWidth("BACK"), startFont->getStringHeight("BACK")));
	renderer->drawText(startFont, "BACK", backUIRect.StarPos().x, backUIRect.StarPos().y, 0);

}