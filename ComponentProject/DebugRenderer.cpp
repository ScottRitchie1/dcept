#include "DebugRender.h"
#include <Renderer2D.h>
DebugRenderer::DebugRenderer() : IComponent(){

}

DebugRenderer::DebugRenderer(const unsigned int _color, float _width, float _height) : IComponent() {
	color = _color;
	width = _width;
	height = _height;
}

DebugRenderer::~DebugRenderer() {

}

void DebugRenderer::Update(float deltaTime) {

}
void DebugRenderer::Draw(aie::Renderer2D* m_2dRenderer) {

	Matrix3 worldTransform = gameObject->WorldTransform();
	Vector2 scale = worldTransform.GetScale();

	m_2dRenderer->setRenderColour(color);

	m_2dRenderer->drawBox(worldTransform.position.x,
		worldTransform.position.y,
		width * scale.x,
		height * scale.y,
		worldTransform.right.Rotation());

	m_2dRenderer->drawLine(worldTransform.position.x,
		worldTransform.position.y,
		worldTransform.position.x + (worldTransform.up.x * width),
		worldTransform.position.y + (worldTransform.up.y * height));

	m_2dRenderer->setRenderColour(0xFF7F50FF);

	m_2dRenderer->drawLine(worldTransform.position.x,
		worldTransform.position.y,
		worldTransform.position.x + (gameObject->velocity.normalised().x * 20),
		worldTransform.position.y + (gameObject->velocity.normalised().y * 20));

	m_2dRenderer->setRenderColour(0xFFFFFFFF);
}