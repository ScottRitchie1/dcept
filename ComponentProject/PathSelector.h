#pragma once
#include "IComponent.h"
#include "Path.h"

class PathSelector : public IComponent {
public:
	PathSelector();
	virtual ~PathSelector();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	Path* GetPath();

	bool visualize = false;
	float radius = 10;

protected:
private:

	Path * path;
};
