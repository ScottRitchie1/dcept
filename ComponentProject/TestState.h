#pragma once
#include "IState.h"
#include "Application.h"
#include "Renderer2D.h"

class TestState : public IState
{
public:
	TestState(Application2D *pApp);
	virtual ~TestState();

	virtual void Update(float deltaTime);
	virtual void Draw();

private:

	aie::Texture*		m_texture;
	aie::Texture*		m_shipTexture;

	float m_timer;
};