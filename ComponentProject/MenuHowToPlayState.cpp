#include "MenuHowToPlayState.h"
#include "GameState.h"
#include "Application2D.h"
#include "GameStateManager.h"
#include "Font.h"
#include "Rect.h"
#include <string>

MenuHowToPlayState::MenuHowToPlayState(Application2D *pApp) : IState(pApp) {
	titleFont = nullptr;
	bodyTextFont = nullptr;
	headingFont = nullptr;

	input = nullptr;
}
MenuHowToPlayState::~MenuHowToPlayState() {

}

void MenuHowToPlayState::OnLoad() {
	input = GetApp()->GetInput();
	titleFont = GetApp()->DefaultFont72();
	headingFont = GetApp()->DefaultFont36();
	bodyTextFont = GetApp()->DefaultFont24();

}
void MenuHowToPlayState::Update(float deltaTime) {
	if (input->wasKeyPressed(aie::INPUT_KEY_BACKSPACE) || (backUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY())) && input->wasMouseButtonPressed(0))) {
		GetApp()->GetGameStateManager()->Remove("MenuHowToPlayState");
		GetApp()->GetGameStateManager()->PushState("MenuTitleState");
	}
}
void MenuHowToPlayState::Draw() {
	auto renderer = GetApp()->GetRender2D();

	renderer->setRenderColour(1, 1, 1, 1);
	renderer->drawText(titleFont, "HOW TO PLAY?", 100, GetApp()->getWindowHeight() - 100, 0);

	if (backUIRect.IsPointInRect(Vector2(input->getMouseX(), input->getMouseY()))) {
		renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
	}
	else {
		renderer->setRenderColour(0.4f, 0.4f, 0.4f, 1.0f);
	}
	backUIRect = Rect(Vector2(100, 50), Vector2(headingFont->getStringWidth("BACK"), headingFont->getStringHeight("BACK")));
	renderer->drawText(headingFont, "BACK", backUIRect.StarPos().x, backUIRect.StarPos().y, 0);
}