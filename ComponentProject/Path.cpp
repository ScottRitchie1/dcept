#include "Path.h"
#include <algorithm>
Path::Path() {
	this->curIndex = 0;
	this->arrivalRange = 25;
}
Path::~Path() {

}

void Path::PushPathSegment(const Vector2& point) {
	m_pathPoints.push_back(point);
}
void Path::PopPathSegment() {
	if (m_pathPoints.empty() == false) {
		m_pathPoints.pop_back();
	}
}

std::vector<Vector2>& Path::GetPath() {
	return m_pathPoints;
}

void Path::ClearPath() {
	m_pathPoints.clear();
	curIndex = 0;
}

bool Path::IsEmpty() {
	return m_pathPoints.empty();
}

void Path::InvertPath() {
	std::reverse(this->GetPath().begin(), this->GetPath().end());
}

void Path::ResetPathProgress() {
	this->curIndex = 0;
}

bool Path::IsPathComplete() {
	int size = m_pathPoints.size();
	if (size == 0) {
		return true;
	}
	return (curIndex >= m_pathPoints.size());
}

bool Path::wasPathSuccsessful() {
	return m_succsessful;
}
