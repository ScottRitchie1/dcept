#pragma once
#include "IState.h"
#include "Rect.h"

namespace aie { class Font; class Input; };

class MenuHowToPlayState : public IState
{
public:

	MenuHowToPlayState(Application2D *pApp);
	virtual ~MenuHowToPlayState();

	void OnLoad();
	virtual void Update(float deltaTime);
	virtual void Draw();

private:
	aie::Input* input;


	aie::Font* titleFont;
	aie::Font* headingFont;
	aie::Font* bodyTextFont;


	Rect titleUIRect;
	Rect backUIRect;
};
