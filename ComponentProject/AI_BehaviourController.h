#pragma once
#include "IComponent.h"
#include "DefaultController.h"

class AI_Behaviour;


class AI_BehaviourController : public IComponent {
public:
	AI_BehaviourController();
	virtual ~AI_BehaviourController();

	virtual void Initialize();
	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	AI_Behaviour * GetBehaviour();
	void SetBehaviour(AI_Behaviour * behaviour);
	
protected:
private:
	AI_Behaviour * currentBehaviour;
	DefaultController* controller;
	
};

class AI_Behaviour : public IComponent {
public:
	AI_Behaviour() { myController = nullptr; };

	AI_BehaviourController * GetBehaviourController() { return myController; };
	void SetBehaviourController(AI_BehaviourController* controller) { myController = controller; };

	virtual void CheckForBehaviourChange() {};

	AI_BehaviourController * myController;
};
