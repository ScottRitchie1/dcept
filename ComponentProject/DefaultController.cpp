#include "DefaultController.h"
#include "SInWaveSway.h"
#include "Application2D.h"
#include <Input.h>

DefaultController::DefaultController(int up, int down, int left, int right, int tRight, int tLeft) : IComponent() {
	upKey = up;
	downKey = down;
	rightKey = right;
	leftKey = left;

	turnRight = tRight;
	turnLeft = tLeft;

	rotSpeed = 90;
	moveSpeed = 25;
}
DefaultController::~DefaultController() {

}

void DefaultController::SetKeys(int up, int down, int right, int left, int tRight, int tLeft) {
	upKey = up;
	downKey = down;
	rightKey = right;
	leftKey = left;

	turnRight = tRight;
	turnLeft = tLeft;
}


void DefaultController::Update(float deltaTime) {
	aie::Input *input = GetApp()->GetInput();
	m_usingInputs = false;

	

	//gameObject->transform.SetRotationFromVector(normilizedVel);


	if (input->isKeyDown(turnLeft)) {
		gameObject->transform = gameObject->transform * Matrix3::MakeRotationZ(rotSpeed * 3.14159f / 180 * deltaTime);
	}
	else if (input->isKeyDown(turnRight))
	{
		gameObject->transform = gameObject->transform * Matrix3::MakeRotationZ(-rotSpeed * 3.14159f / 180 * deltaTime);
	}

	if (GetApp()->GetInput()->isKeyDown(upKey)) {
		gameObject->ApplyForce(Vector2::Up() * moveSpeed);
		m_usingInputs = true;
	}
	else if (GetApp()->GetInput()->isKeyDown(downKey)) {
		gameObject->ApplyForce(Vector2::Up() * -moveSpeed);
		m_usingInputs = true;

	}

	if (GetApp()->GetInput()->isKeyDown(rightKey)) {
		gameObject->ApplyForce(Vector2::Right() * moveSpeed);
		m_usingInputs = true;

	}
	else if (GetApp()->GetInput()->isKeyDown(leftKey)) {
		gameObject->ApplyForce(Vector2::Right() * -moveSpeed);
		m_usingInputs = true;

	}

	Vector2 normilizedVel = gameObject->velocity.normalised();
	float radians = std::atan2f(normilizedVel.y, normilizedVel.x);
	//gameObject->transform.SetRotation(radians);

}

bool DefaultController::IsUsingInputs() {
	return m_usingInputs;
}
