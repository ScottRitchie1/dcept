#pragma once
#include "Character.h"
#include "HunterLight.h"
#include "Path.h"
#include <Input.h>

struct CharacterTracking;
class CharacterManager;
class Graph2D;

class Hunter : public Character {
public:
	Hunter();
	Hunter(const char* textureFileName, float speed = 50.0f, Vector2 size = { 64, 64 }, Vector2 origin = { 0.5f, 0.0f });
	virtual ~Hunter();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);


	void SetInputs(aie::EInputCodes attack, aie::EInputCodes tag, aie::EInputCodes move);

	void SetCharacterManager(CharacterManager* mgr);
	void SetLight(HunterLight* light);
	void SetGraph(Graph2D* graph);
	virtual void ClearHunterTargets();

	virtual void OnAttack();
	void OnTag();
	virtual void CheckDiscovery();

	CharacterTracking* targetToKill;
	CharacterTracking* targetToTag;

	float attackRange;
	float tagRange;

protected:
	HunterLight * m_light;
	CharacterManager* m_characterManager;
	Path m_path;
	Graph2D* m_graph;

	aie::EInputCodes attackKey;
	aie::EInputCodes tagKey;
	aie::EInputCodes moveKey;
};