#pragma once
#include "IState.h"
#include "Rect.h"

namespace aie { class Font; class Input; };

class MenuTitleState : public IState
{
public:

	MenuTitleState(Application2D *pApp);
	virtual ~MenuTitleState();

	void OnLoad();
	virtual void Update(float deltaTime);
	virtual void Draw();

private:
	aie::Input* input;


	aie::Font* titleFont;
	aie::Font* optionFont;

	Rect titleUIRect;

	float menuSpacing;
	int menuIndex;
	const unsigned int MENU_ITEMS_COUNT = 3;
	const char* menuItems[3] = { "Play", "How to play", "Quit" };
	Rect MenuRects[3];

	bool IsMouseSelecting;
	Vector2 lastMousePos;


};
