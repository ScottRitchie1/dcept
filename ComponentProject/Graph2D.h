#pragma once
#include "Graph.h"
#include "Vec2.h"

class Graph2D : public Graph<Vector2, float> {
public:
	Graph2D();
	virtual ~Graph2D();

	void GetNearbyNodes(const Vector2 pos, float radius, std::vector<Graph2D::Node*> & out_Nodes);
	void GetClosestNode(const Vector2 pos, Graph2D::Node* &node);
	Vector2 GetClosestNodePoint(const Vector2 pos);

	Vector2 GetRandomNodePosition();

	void GenerateGraphGrid(int graphRows = 10, int graphCols = 15, float graphSpacing = 50, float edgeLenght = 75, float graphOffsetX = 50, float graphOffsetY = 50);
	void GenerateGraphFromTexture(const char* textureFileName, int graphRows, float graphSpacing = 50, float edgeLenght = 75, float graphOffsetX = 0, float graphOffsetY = 0);


protected:
private:

};