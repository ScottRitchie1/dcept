#pragma once
#include "Vec3.h"
#include "Vec2.h"
class Matrix3
{
public:
	static Matrix3 MakeIdentity();
	static Matrix3 MakeTranslation(float x, float y);
	static Matrix3 MakeScale(float sx, float sy);
	static Matrix3 MakeRotationZ(float radians);
	float GetRotation();
	void SetRotation(float radians);

	Vector2 GetScale();

	union
	{
		struct 
		{
			float	m0, m1, m2,
					m3, m4, m5,
					m6, m7, m8;
		};

		struct 
		{
			Vector3 xAxis;
			Vector3 yAxis;
			Vector3 zAxis;
		};

		struct
		{
			struct { Vector2 right; unsigned int : 32;}; 
			struct { Vector2 up; unsigned int : 32; };
			struct { Vector2 position; unsigned int : 32; };
		};
	};
	

	Matrix3();
	Matrix3(	float M0, float M1, float M2, 
				float M3, float M4, float M5, 
				float M6, float M7, float M8);
	~Matrix3();

	Matrix3 operator * (const Matrix3& rhs)const;
	Vector3 operator * (const Vector3& rhs)const;
	Matrix3& operator *=(const Matrix3& rhs);

	bool operator == (const Matrix3& rhs) const;
	bool operator != (const Matrix3& rhs) const;

	float& operator [] (const unsigned int index);

private:

};



