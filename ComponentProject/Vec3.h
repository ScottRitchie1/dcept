#pragma once
#include <cmath>

class Vector3
{
public:
	Vector3();
	Vector3(float x, float y, float z);
	~Vector3();

	float Length() const;
	Vector3& Normalise();

	Vector3 normalised();
	Vector3 GetNormalised();
	float Dot(const Vector3& rhs) const;
	Vector3 Cross(Vector3& rhs) const;

	Vector3 operator * (const float &f) const;
	Vector3 operator+(const Vector3 &rhs) const;
	Vector3& operator+=(const Vector3 &rhs);
	Vector3 operator-(const Vector3 &rhs) const;
	Vector3 operator - () const;
	Vector3& operator-=(const Vector3 &rhs);
	bool operator == (const Vector3 &rhs) const;
	bool operator != (const Vector3 &rhs) const;
	float operator [] (const unsigned int index) const;

	

	float x, y, z;

	static Vector3 Right;
	static Vector3 Up;
	static Vector3 Forward;
private:

};

// global operator overload
Vector3 operator + (const float &lhs, const Vector3 &rhs);
Vector3 operator - (const float &lhs, const Vector3 &rhs);
Vector3 operator * (const float &lhs, const Vector3 &rhs);
