#pragma once
#include "IComponent.h"

class PointSelector : public IComponent {
public:
	PointSelector();
	virtual ~PointSelector();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	bool IsPointWithinInnerRadius(Vector2 point);
	bool IsPointWithinOuterRadius(Vector2 point);

	bool visualize = false;
	bool isActive = true;

protected:
private:
	
	float InnerRadius = 15.0f;
	float OuterRadius = 150.0f;
};
