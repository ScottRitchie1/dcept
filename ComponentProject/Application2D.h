#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "Input.h"
#include "GameSettings.h"

class IState;
class GameStateManager;
class GameSettings;

class Application2D : public aie::Application {
public:

	bool debugMode = false;
	GameSettings gameSettings;

	float m_cameraX, m_cameraY;

	Application2D();
	virtual ~Application2D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	aie::Renderer2D *GetRender2D() { return m_2dRenderer; };
	void LoadDefaultFont(const char* fontFile);
	aie::Font *DefaultFont16() { return m_font16; };
	aie::Font *DefaultFont24() { return m_font24; };
	aie::Font *DefaultFont36() { return m_font36; };
	aie::Font *DefaultFont48() { return m_font48; };
	aie::Font *DefaultFont72() { return m_font72; };
	aie::Font *DebugFont() { return m_debugFont; };


	GameStateManager* GetGameStateManager() { return m_gameStateManager; };

	aie::Input* GetInput() { return m_input; };

	void SetCameraPosition(float x, float y) { m_cameraX = x; m_cameraY = y; };

protected:

	aie::Renderer2D*	m_2dRenderer;
	GameStateManager*	m_gameStateManager;
	aie::Input*			m_input;

	aie::Font*			m_debugFont;
	aie::Font*			m_font16;
	aie::Font*			m_font24;
	aie::Font*			m_font36;
	aie::Font*			m_font48;
	aie::Font*			m_font72;
};