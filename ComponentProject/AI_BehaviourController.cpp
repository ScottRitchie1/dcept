#include "AI_BehaviourController.h"

AI_BehaviourController::AI_BehaviourController() : IComponent() {
	currentBehaviour = nullptr;
	controller = nullptr;
}
AI_BehaviourController::~AI_BehaviourController() {
	if (currentBehaviour != nullptr) {
		delete currentBehaviour;
	}
}

void AI_BehaviourController::Initialize() {
	controller = gameObject->GetComponent<DefaultController>();
}

void AI_BehaviourController::Update(float deltaTime) {

	bool usingInputs = (controller != nullptr && controller->IsUsingInputs());

	if (currentBehaviour != nullptr && usingInputs == false) {
		if (currentBehaviour->GetEnabled()) {

			currentBehaviour->CheckForBehaviourChange();
			currentBehaviour->Update(deltaTime);
		}
	}
}

void AI_BehaviourController::Draw(aie::Renderer2D* m_2dRenderer) {

	if (currentBehaviour->GetEnabled()) {

		currentBehaviour->Draw(m_2dRenderer);
	}
}

AI_Behaviour * AI_BehaviourController::GetBehaviour() {
	return currentBehaviour;
}
void AI_BehaviourController::SetBehaviour(AI_Behaviour * behaviour) {
	if (currentBehaviour != nullptr) {
		delete currentBehaviour;
		currentBehaviour = nullptr;
	}
	if (behaviour != nullptr) {
		currentBehaviour = behaviour;
		behaviour->gameObject = gameObject;
		behaviour->Initialize();
		currentBehaviour->SetBehaviourController(this);
	}
}
