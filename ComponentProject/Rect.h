#pragma once
#include "Vec2.h"

class Rect {
public:
	Rect();
	Rect(Vector2 start, Vector2 size);
	~Rect();

	bool IsPointInRect(Vector2 point);
	float Top();
	float Bottom();
	float Right();
	float Left();
	Vector2 Center();

	Vector2 StarPos();
	Vector2 Size();

private:
	Vector2 m_start;
	Vector2 m_size;
};