#include "PauseState.h"
#include "GameState.h"
#include "Application2D.h"
#include "Font.h"
#include "GameStateManager.h"
#include "Lerp.h"

PauseState::PauseState(Application2D *pApp) : IState(pApp) {
	OnLoad();
}

PauseState::~PauseState() {

}

void PauseState::OnLoad()
{
	menuIndex = 0;
	menuItemFont = GetApp()->DefaultFont36();
	lastMousePos = Vector2();
}

void PauseState::Update(float deltaTime) {
	aie::Input* input = GetApp()->GetInput();

	if (input->wasKeyPressed(aie::INPUT_KEY_ESCAPE)) {
		GetApp()->GetGameStateManager()->Remove("PauseState");
		return;
	}



	if (input->wasKeyPressed(aie::INPUT_KEY_DOWN)) {
		menuIndex++;
		menuIndex = menuIndex % MENU_ITEMS_COUNT;
	}
	else if (input->wasKeyPressed(aie::INPUT_KEY_UP)) {
		menuIndex--;
		if (menuIndex < 0) {
			menuIndex = MENU_ITEMS_COUNT - 1;
		}
	}

	Vector2 mouseXY = Vector2(input->getMouseX(), input->getMouseY());
	if (mouseXY.DistanceFrom(lastMousePos) > 5) {
		for (int i = 0; i < MENU_ITEMS_COUNT; i++)
		{
			float menuItemBottom = (GetApp()->getWindowHeight() / 2) - (menuSpacing * i) + ((menuSpacing * MENU_ITEMS_COUNT) / 2);
			float menuItemTop = menuItemBottom + menuItemFont->getStringHeight(menuItems[i]);

			float menuItemLeft = (GetApp()->getWindowWidth() / 2) - (menuItemFont->getStringWidth(menuItems[i]) / 2);
			float menuItemRight = menuItemLeft + (menuItemFont->getStringWidth(menuItems[i]));
			if (mouseXY.y < menuItemTop && mouseXY.y > menuItemBottom
				&& mouseXY.x > menuItemLeft && mouseXY.x < menuItemRight) {
				menuIndex = i;
				IsMouseSelecting = true;
				break;
			}
			else {
				IsMouseSelecting = false;
			}
		}
		lastMousePos = mouseXY;
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_ENTER) || (IsMouseSelecting && input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_LEFT))) {
		switch (menuIndex)
		{
		case 0: {//resume
			GetApp()->GetGameStateManager()->Remove("PauseState");
			return;break;}
		case 1: {//restart
			auto gameState = dynamic_cast<GameState*>(GetApp()->GetGameStateManager()->GetState("GameState"));
			if (gameState != nullptr) {
				gameState->RestartGame();
			}
			GetApp()->GetGameStateManager()->Remove("PauseState");
			return;break;}
		case 2://menu
		{
			GetApp()->GetGameStateManager()->Remove("PauseState");
			GetApp()->GetGameStateManager()->Remove("GameState");
			GetApp()->GetGameStateManager()->PushState("MenuNewGameState");
			return;break;}
		default: {//quit
			GetApp()->quit();
			GetApp()->GetGameStateManager()->Remove("PauseState");
			return;break;}
		}

	}
}

void PauseState::Draw() {

	auto renderer = GetApp()->GetRender2D();

	renderer->setRenderColour(0, 0, 0, 0.8f);
	renderer->drawBox((GetApp()->getWindowWidth()/2),(GetApp()->getWindowHeight()/2), GetApp()->getWindowWidth(), GetApp()->getWindowHeight(), 0, 0);
	renderer->setRenderColour(255, 255, 255, 255);


	float totalHeight = menuSpacing * MENU_ITEMS_COUNT;
	if ((GetApp()->getWindowHeight() / 2) > totalHeight + (menuSpacing * 2)) {
		renderer->drawText(GetApp()->DefaultFont48(), "PAUSED", (GetApp()->getWindowWidth() / 2) - (GetApp()->DefaultFont48()->getStringWidth("PAUSED") / 2), (GetApp()->getWindowHeight() / 2) + (totalHeight / 2) + (menuSpacing * 2));
	}
	for (int i = 0; i < MENU_ITEMS_COUNT; i++)
	{
		if (menuIndex == i) {
			renderer->setRenderColour(0.9f, 0.4f, 0.4f, 1.0f);
		}else {
			renderer->setRenderColour(0.3f, 0.3f, 0.3f, 0.6f);
		}
		renderer->drawText(menuItemFont, menuItems[i], (GetApp()->getWindowWidth() / 2) - (menuItemFont->getStringWidth(menuItems[i]) / 2), (GetApp()->getWindowHeight() / 2) - (menuSpacing * i) + (totalHeight/2));
	}
}