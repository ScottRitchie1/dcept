#pragma once
#include "AI_BehaviourController.h"
#include "Path.h"

class AI_FollowPath : public AI_Behaviour {
public:
	AI_FollowPath();
	AI_FollowPath(Path* p);
	virtual ~AI_FollowPath();

	virtual void Initialize();

	virtual void Update(float deltaTime);
	virtual void CheckForBehaviourChange();
	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	Path* GetPath();
	void SetPath(Path* p);

	float speed;
protected:
	int currentPathIndex;
	Path * path;

private:
};