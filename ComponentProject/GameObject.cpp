#include "GameObject.h"
#include "Application2D.h"
#include "IComponent.h"

GameObject::GameObject(IState* state) {
	m_parent = nullptr;
	currentState = state;
	useForces = false;
}
GameObject::~GameObject() {
	for (auto iter = m_components.begin(); iter != m_components.end(); iter++) {
		delete (*iter);
	}
	m_components.clear();
}

void GameObject::Update(float deltaTime) {
	if (useForces) {
		ApplyForce(-velocity);
		velocity += acceleration;
		transform.position += (velocity) * deltaTime;
		acceleration = Vector2();
	}
	for (auto iter = m_components.begin(); iter != m_components.end(); iter++)
	{
		if ((*iter)->GetEnabled()) {
			(*iter)->Update(deltaTime);
		}
	}
}
void GameObject::Draw(aie::Renderer2D* m_2dRenderer) {

	for (auto iter = m_components.begin(); iter != m_components.end(); iter++)
	{
		if ((*iter)->GetEnabled()) {
			(*iter)->Draw(m_2dRenderer);
		}
	}
}

void GameObject::ApplyForce(const Vector2 &force) {
	acceleration += force * (drag * drag);
}

IState* GameObject::GetCurrentState() {
	return currentState;
}
Application2D* GameObject::GetApp() {
	if (currentState != nullptr) {
		return currentState->GetApp();
	}
	return nullptr;
}

Matrix3 GameObject::WorldTransform() {
	if (m_parent != nullptr) {
		return m_parent->WorldTransform() * transform;
	}

	return transform;
}

void GameObject::AddComponent(IComponent* component) {
	component->gameObject = this;
	m_components.push_back(component);
	component->Initialize();
}