#pragma once
#include "IComponent.h"
#include "Sprite.h"

class HunterLight : public IComponent {
public:

	HunterLight();
	HunterLight(const char* textureFileName, float r = 256.0f, float s = 0.3f, float d = 1.0f, Vector2 _OffSet = Vector2(0,0));
	virtual ~HunterLight();

	virtual void Draw(aie::Renderer2D* m_2dRenderer);

	Vector2 GetWorldCenter();

	float range;
	float darknessStrenght;
	float depth;
	Vector2 offSet;

private:
	aie::Texture* m_texture;
};


