#include "Assassin.h"
#include "Application2D.h"
#include "CharacterManager.h"
#include "Hunter_AI.h"
#include "GameStateManager.h"

Assassin::Assassin() :Character() { m_characterManager = nullptr; 	attackRange = 50;}
Assassin::Assassin(const char* textureFileName, float speed, Vector2 size, Vector2 origin) :Character(textureFileName, speed, size, origin) {
	m_characterManager = nullptr;
	attackRange = 50;
}
Assassin::~Assassin() {}

void Assassin::Update(float deltaTime) {
	auto input = GetApp()->GetInput();
	if (input->wasKeyPressed(m_controls.attackKey)) {
		OnAttack();
	}

	Vector2 moveDir;
	if (GetApp()->GetInput()->isKeyDown(m_controls.upKey)) {
		moveDir += Vector2::Up();
	}
	if (GetApp()->GetInput()->isKeyDown(m_controls.downKey)) {
		moveDir -= Vector2::Up();
	}
	if (GetApp()->GetInput()->isKeyDown(m_controls.leftKey)) {
		moveDir -= Vector2::Right();
	}
	if (GetApp()->GetInput()->isKeyDown(m_controls.rightKey)) {
		moveDir += Vector2::Right();
	}

	gameObject->ApplyForce(moveDir.normalised() * speed);
	Character::Update(deltaTime);
}
void Assassin::Draw(aie::Renderer2D* m_2dRenderer) {
	if (GetApp()->GetGameStateManager()->IsStateRunning("GameOverState") == false) {
		Character::Draw(m_2dRenderer);
	}
	else {
		m_2dRenderer->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
		int stateInt = (int)currentState;
		m_currentAnimation = StateAnimations[stateInt].GetStateDirectionAnim(currentDir);
		float depth = -1;

		if (m_currentAnimation != nullptr) {// if the state we are in has an animation; ie not -1
			m_currentAnimation->SetDepth(depth);
			m_currentAnimation->Draw(m_2dRenderer);
		}
		else
		{
			Matrix3 worldTransform = gameObject->WorldTransform();
			m_2dRenderer->drawSprite(m_defaultTexture,
				worldTransform.position.x,
				worldTransform.position.y,
				m_defaultSize.x * worldTransform.GetScale().x,
				m_defaultSize.y * worldTransform.GetScale().y,
				worldTransform.GetRotation(),
				depth,
				m_defaultOrigin.x,
				m_defaultOrigin.y);
		}
	}
}

void Assassin::SetInputs(aie::EInputCodes up, aie::EInputCodes down, aie::EInputCodes left, aie::EInputCodes right, aie::EInputCodes attack) {
	m_controls.upKey = up;
	m_controls.downKey = down;
	m_controls.leftKey = left;
	m_controls.rightKey = right;
	m_controls.attackKey = attack;
}

AssassinControls* Assassin::GetInputs() {
	return &m_controls;
}


void Assassin::SetCharacterManager(CharacterManager* mgr) {
	m_characterManager = mgr;
}

void Assassin::OnAttack() {
	if (m_characterManager != nullptr) {
		auto targetToKill = m_characterManager->FindClosestCivilian(gameObject->transform.position);

		if (targetToKill != nullptr && targetToKill->character->gameObject->transform.position.DistanceFrom(gameObject->transform.position) < attackRange) {
			targetToKill->character->SetCharacterDeath(true);
			
			auto hunter = m_characterManager->GetHunter_AI();
			if (hunter != nullptr) {
				hunter->OnNewDeath(targetToKill);
			}
		}
	}
}