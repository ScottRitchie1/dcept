#include "GameStateManager.h"
#include "IState.h"
#include <iostream>

GameStateManager::GameStateManager() {

}
GameStateManager::~GameStateManager() {
	for (auto iter = m_registeredStates.begin(); iter != m_registeredStates.end(); iter++)
	{
		delete iter->second;
	}
	m_registeredStates.clear();
}


void GameStateManager::UpdateGameStates(float deltaTime) {

	ProcessCommands();
	for (auto iter = m_stateStackv2.begin(); iter != m_stateStackv2.end(); iter++)
	{
		(*iter).second->Update(deltaTime);
	}
}
void GameStateManager::DrawGameStates() {
	for (auto iter = m_stateStackv2.begin(); iter != m_stateStackv2.end(); iter++)
	{
		(*iter).second->Draw();
	}
}

void GameStateManager::SetState(const char* name, IState *pState) {

	Command cmd;
	cmd.cmd = ECommand::SET;
	cmd.name = name;
	cmd.pState = pState;

	m_commands.push_back(cmd);


	
}
void GameStateManager::PushState(const char* name) {

	Command cmd;
	cmd.cmd = ECommand::PUSH;
	cmd.name = name;
	cmd.pState = nullptr;
	
	m_commands.push_back(cmd);

}
void GameStateManager::PopState() {
	
	Command cmd;
	cmd.cmd = ECommand::POP;
	cmd.name = "";
	cmd.pState = nullptr;

	m_commands.push_back(cmd);
}

void GameStateManager::Remove(const char* name) {

	Command cmd;
	cmd.cmd = ECommand::POP;
	cmd.name = name;
	cmd.pState = nullptr;

	m_commands.push_back(cmd);
}

IState* GameStateManager::GetTopState() {
	if (m_stateStackv2.size() > 0) {
		return m_stateStackv2.rbegin()->second;
	}
	return nullptr;
}

IState * GameStateManager::GetState(const char * name)
{
	auto state = m_stateStackv2.find(name);
	if (state != m_stateStackv2.end()) {
		return (*state).second;
	}
	return nullptr;
}

bool GameStateManager::IsStateRunning(const char * name)
{
	

	auto state = m_stateStackv2.find(name);
	if (state != m_stateStackv2.end()) {
		return true;
	}
	return false;
}

void GameStateManager::ProcessCommands() {//COMMAND PROCESSING
	for (auto cmdIter = m_commands.begin(); cmdIter != m_commands.end(); cmdIter++)
	{
		Command &cmd = (*cmdIter);

		switch (cmd.cmd)
		{
		case ECommand::SET: {
			auto iter = m_registeredStates.find(cmd.name);
			if (iter != m_registeredStates.end()) {
				delete iter->second;
			}
			m_registeredStates[cmd.name] = cmd.pState;

			break;}


		case ECommand::PUSH: {
			auto iter = m_registeredStates.find(cmd.name);
			if (iter != m_registeredStates.end()) {
				m_stateStackv2[iter->first] = iter->second;
				m_stateStackv2[iter->first]->OnLoad();
			}

			break;}


		case ECommand::POP: {
			if (m_stateStackv2.size() > 0) {
				if (cmd.name == "") {
					m_stateStackv2.erase(m_stateStackv2.rbegin()->first);
				}
				else {
					m_stateStackv2.erase(cmd.name);
				}
			}
			break;}


		default: break;
		}
	}
	m_commands.clear();
}