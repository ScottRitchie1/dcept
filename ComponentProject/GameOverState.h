#pragma once
#include "IState.h"
#include "Vec2.h"

namespace aie { class Font; };
class GameState;

class GameOverState : public IState
{
public:

	GameOverState(Application2D *pApp);
	virtual ~GameOverState();

	void OnLoad();
	virtual void Update(float deltaTime);
	virtual void Draw();

private:
	GameState * gameState;

	aie::Font* menuItemFont;
	float menuSpacing;
	int menuIndex;
	const unsigned int MENU_ITEMS_COUNT = 3;
	const char* menuItems[3] = { "Restart", "Exit To Menu", "Quit" };

	bool IsMouseSelecting;
	Vector2 lastMousePos;

	float elapsedTime;

	bool backgroundAnimationFinished;

	float backgroundLerpStart;
	float backgroundCurrent;
	float backgroundLerpSpeed;
	float backgroundTargetY;

	bool GameOverAnimationFinished;
	float GameOverLerpStart;
	float GameOverCurrent;
	float GameOverLerpSpeed;
	float GameOverTargetY;

	bool bodyTextAnimationFinished;
	aie::Font* bodyTextFont;
	float bodyTextLerpStart;
	float bodyTextCurrent;
	float bodyTextLerpSpeed;
	float bodyTextMax;

	bool MenuItemAnimationFinished;
	float MenuItemLerpStart;
	float MenuItemCurrentY;
	float MenuItemLerpSpeed;
	float MenuItemTargetY;

	bool skipAlphaActive;
	float skipAlphaCurrent;
	float skipAlphaMin;
	float skipAlphaMax;
	float skipAlphaChange;
};