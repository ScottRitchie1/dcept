#pragma once
#include "IComponent.h"

class ExamplePlayerController : public IComponent {
public:
	ExamplePlayerController();
	virtual ~ExamplePlayerController();

	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* m_2dRenderer);
protected:
private:

};
