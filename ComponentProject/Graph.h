#pragma once
#include <vector>

template<class TNodeData, class TEdgeData>
class Graph {
public:

	struct Node;
	struct Edge;


	struct Node {
		TNodeData data;
		std::vector<Edge> connections;
	};

	struct Edge {
		Node *to;
		TEdgeData data;
		float dataMultiplier;
	};

	Graph() {

	}
	virtual ~Graph() {
		for (auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++)
		{
			delete (*iter);
		}

		m_nodes.clear();
	}

	Node* AddNode(const TNodeData &data) {
		Node* node = new Node();
		node->data = data;
		m_nodes.push_back(node);
		return node;
	}
	void AddEdge(Node* nodeA, Node* nodeB, const TEdgeData &data, bool bidirectional) {
		Edge e;
		e.data = data;
		e.to = nodeB;
		e.dataMultiplier = 1;
		nodeA->connections.push_back(e);

		if (bidirectional) {
			AddEdge(nodeB, nodeA, data, false);
		}
	}

	bool RemoveNode(Node* node) {
		for (auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++)
		{
			if ((*iter) == node) {
				for (auto edgeIter = (*iter)->connections.begin(); edgeIter != (*iter)->connections.end(); edgeIter++)
				{
					for (auto otherEdgeIter = (*edgeIter).to->connections.begin(); otherEdgeIter != (*edgeIter).to->connections.end(); otherEdgeIter++)
					{
						if ((*otherEdgeIter).to == (*iter)) {
							(*edgeIter).to->connections.erase(otherEdgeIter);
							break;
						}
					}
					
				}
				delete (*iter);
				m_nodes.erase(iter);
				return true;
			}
		}
		return false;
	}


	const std::vector<Node*> & GetNodes() {
		return m_nodes;
	}

protected:

	std::vector<Node*> m_nodes;

private:
};
