#include "PathSelector.h"
#include <Input.h>

#include "Application2D.h"
#include <Renderer2D.h>


PathSelector::PathSelector() {
	path = new Path();
}
PathSelector::~PathSelector() {
	delete path;
}

void PathSelector::Update(float deltaTime) {
	aie::Input* input = gameObject->GetApp()->GetInput();
	if (input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_MIDDLE)) {
		path->PushPathSegment(Vector2(input->getMouseX(), input->getMouseY()));
	}
}
void PathSelector::Draw(aie::Renderer2D* m_2dRenderer) {
	auto & _path = path->GetPath();
	
	if (visualize && path != nullptr && path->IsEmpty() == false) {
		Vector2 lastPos;
		for (auto iter = _path.begin(); iter != _path.end(); iter++)
		{
			Vector2 point = (*iter);
			//m_2dRenderer->setRenderColour(0xDCDCDC4D);
			//m_2dRenderer->drawCircle(point.x, point.y, radius);
			if (iter != _path.begin()) {
				m_2dRenderer->setRenderColour(0xFF00004D);
				m_2dRenderer->drawLine(lastPos.x, lastPos.y, point.x, point.y, 2.0f);
			}
			lastPos = (*iter);
		}
		m_2dRenderer->setRenderColour(0xFFFFFFFFFF);

	}
}

Path* PathSelector::GetPath() {
	return path;
}