#pragma once
#include <cmath>

class Vector2
{
public:
	Vector2();
	Vector2(float x, float y);
	~Vector2();

	static Vector2 Up();
	static Vector2 Right();
	static Vector2 Zero();

	Vector2 PerpLeft() const;
	Vector2 PerpRight() const;
	float Rotation() const;
	float AngleBetween(Vector2 &param);
	Vector2 Rotate(float radians) const;
	Vector2 RotateAround(Vector2 origin, float raidans) const;
	float DistanceFrom(Vector2 pos);
	float Length() const;
	Vector2 normalised();
	Vector2& Normalise();
	Vector2 GetNormalised();

	
	float Dot(const Vector2& rhs) const;

	Vector2 operator * (const float &f) const;
	Vector2 operator + (const Vector2 &rhs) const;
	Vector2& operator+=(const Vector2 &rhs);
	Vector2 operator-(const Vector2 &rhs) const;
	Vector2 operator-() const;
	Vector2& operator-=(const Vector2 &rhs);
	bool operator == (const Vector2 &rhs) const;
	bool operator != (const Vector2 &rhs)const ;

	float operator [] (const unsigned int index) const;

	float x, y;
private:

};


// global operator overload
Vector2 operator + (const float &lhs, const Vector2 &rhs);
Vector2 operator - (const float &lhs, const Vector2 &rhs);
Vector2 operator * (const float &lhs, const Vector2 &rhs);
