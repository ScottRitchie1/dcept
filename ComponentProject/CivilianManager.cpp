#include "CivilianManager.h"
#include "AI_BehaviourController.h"
#include "PathFinder.h"

CivilianManager::CivilianManager() {
	m_civs.clear();
	m_graph = nullptr;
}
CivilianManager::~CivilianManager() {
	for (auto iter = m_civs.begin(); iter != m_civs.end(); iter++)
	{
		delete (*iter)->gameObject;
		(*iter) = nullptr;
	}
}


void CivilianManager::Update(float deltaTime) {
	for (auto iter = m_civs.begin(); iter != m_civs.end(); iter++)
	{
		(*iter)->gameObject->Update(deltaTime);
	}
}
void CivilianManager::Draw(aie::Renderer2D* m_2dRenderer) {
	for (auto iter = m_civs.begin(); iter != m_civs.end(); iter++)
	{
		(*iter)->Draw(m_2dRenderer);
	}
}

Civilian* CivilianManager::FindClosestCivilian(Vector2 Pos, bool includeDead) {
	Civilian* closestCiv = nullptr;
	float closestCivDistance = 99999999999999.f;

	for (auto iter = m_civs.begin(); iter != m_civs.end(); iter++)
	{
		if ((*iter)->isDead == false || includeDead == true) {
			float dist = (*iter)->gameObject->transform.position.DistanceFrom(Pos);
			if (dist < closestCivDistance) {
				closestCiv = (*iter);
				closestCivDistance = dist;
			}
		}
	}

	return closestCiv;
}

Civilian* CivilianManager::CreateNewCiv() {
	GameObject* obj = new GameObject(gameObject->GetCurrentState());

	obj->useForces = true;
	obj->drag = 0.3f;

	obj->AddComponent<Civilian>("", 100, Vector2(64, 64), Vector2(0.5f, 0.0f));
	auto charComp = obj->GetComponent<Civilian>();
	charComp->SetGraph(m_graph);
	charComp->SetAnimation(E_State::DEAD, "./textures/Characters/Skeleton.png", 14, 10, false);

	charComp->SetAnimation(E_State::WALK, E_Dir::UP, "./textures/Characters/Hunter/Hunter_WalkUp.png", 4.f, 3.5f);
	charComp->SetAnimation(E_State::WALK, E_Dir::DOWN, "./textures/Characters/Hunter/Hunter_WalkDown.png", 4.f, 3.5f);
	charComp->SetAnimation(E_State::WALK, E_Dir::RIGHT, "./textures/Characters/Hunter/Hunter_WalkRight.png", 4.f, 3.5f);
	charComp->SetAnimation(E_State::WALK, E_Dir::LEFT, "./textures/Characters/Hunter/Hunter_WalkLeft.png", 4.f, 3.5f);

	charComp->SetAnimation(E_State::IDLE, E_Dir::UP, "./textures/Characters/Hunter/Hunter_IdleUp.png", 2.f);
	charComp->SetAnimation(E_State::IDLE, E_Dir::DOWN, "./textures/Characters/Hunter/Hunter_IdleDown.png", 2.f);
	charComp->SetAnimation(E_State::IDLE, E_Dir::RIGHT, "./textures/Characters/Hunter/Hunter_IdleRight.png", 2.f);
	charComp->SetAnimation(E_State::IDLE, E_Dir::LEFT, "./textures/Characters/Hunter/Hunter_IdleLeft.png", 2.f);

	return charComp;
}

void CivilianManager::SetCivilianCount(unsigned int num) {
	if (m_civs.size() < num) {
		for (size_t i = m_civs.size(); i < num; i++)
		{
			auto newCiv = CreateNewCiv();
			if (m_graph != nullptr) {
				newCiv->InitializeCharacter(m_graph->GetRandomNodePosition());
			}
			else {
				newCiv->InitializeCharacter(Vector2(GetApp()->getWindowWidth() / 2, GetApp()->getWindowHeight() / 2));
			}
			m_civs.push_back(newCiv);
		}
	}
	else {
		for (size_t i = m_civs.size(); i > num; i--)
		{
			
			delete m_civs.back()->gameObject;
			m_civs.back() = nullptr;
			m_civs.pop_back();

			if (m_civs.size() <= 0) {
				break;
			}
		}
	}
}

std::vector<Civilian*>* CivilianManager::GetCivList() {
	return &m_civs;
}


void CivilianManager::IncreaseCivilianCount(int num) {
	int newSize = (int)m_civs.size() + num;
	if (newSize < 0)
		newSize = 0;
	SetCivilianCount(newSize);
}

void CivilianManager::ResetCivilians() {
	for (auto iter = m_civs.begin(); iter != m_civs.end(); iter++)
	{
		(*iter)->isDead = false;
		(*iter)->isTagged = false;

		if (m_graph != nullptr) {
			(*iter)->InitializeCharacter(m_graph->GetRandomNodePosition());
		}
		else {
			(*iter)->InitializeCharacter(Vector2(GetApp()->getWindowWidth()/2, GetApp()->getWindowHeight()/2));
		}
	}
}

void CivilianManager::SetGraph(Graph2D * graph) {
	m_graph = graph;
}
