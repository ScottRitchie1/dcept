#include "Sprite.h"
#include "Application2D.h"
Sprite::Sprite() : IComponent(){
	m_texture = nullptr;
	m_size = Vector2(32, 32);
	m_Origin = Vector2(0.5f, 0.5f);
	SetDepth();
}

Sprite::Sprite(const char* textureFileName, Vector2 size, Vector2 origin) : IComponent() {
	m_texture = new aie::Texture(textureFileName);
	m_size = size;
	m_Origin = origin;
	SetDepth();
}
Sprite::~Sprite() {
	delete m_texture;
	m_texture = nullptr;
}

AnimatedSprite::AnimatedSprite() : Sprite() { // : Sprite() calls the sprites constructor
	m_numFrames = 0;
	m_currentFrame = 0;
	m_currentFrameTime = 0;
	fps = 12;
	loop = false;
}
AnimatedSprite::AnimatedSprite(const char* textureFileName, unsigned int numFrames, float fps, bool loop, Vector2 size, Vector2 origin) : Sprite(textureFileName, size, origin) {// : Sprite(blah blah blah) calls the sprites constructor
	m_numFrames = numFrames;
	if (m_texture != nullptr) {
		m_frameSize.x = (float)(m_texture->getWidth()) / numFrames;
		m_frameSize.y = (float)m_texture->getHeight();
	}
	m_currentFrame = 0;
	m_currentFrameTime = 0;
	this->fps = fps;
	this->loop = loop;

}
AnimatedSprite::~AnimatedSprite() {
	delete m_texture;
	m_texture = nullptr;
}

void Sprite::Draw(aie::Renderer2D* m_2dRenderer) {

	Matrix3 worldTransform = gameObject->WorldTransform();

	m_2dRenderer->setUVRect(0, 0, 1, 1);
	if (m_texture != nullptr) {
		m_2dRenderer->drawSprite(m_texture,
			worldTransform.position.x,
			worldTransform.position.y,
			m_size.x * worldTransform.GetScale().x,
			m_size.y * worldTransform.GetScale().y,
			worldTransform.GetRotation(),
			m_depth,
			m_Origin.x,
			m_Origin.y);
	}
	else {
		m_2dRenderer->drawBox(worldTransform.position.x,
			worldTransform.position.y,
			m_size.x * worldTransform.GetScale().x,
			m_size.y * worldTransform.GetScale().y,
			worldTransform.GetRotation(),
			m_depth);
	}
}

void AnimatedSprite::Draw(aie::Renderer2D* m_2dRenderer) {
	//float drawWidth = m_width; if (w > 0.0f) drawWidth = w;
	//float drawHeight = m_height; if (h > 0.0f) drawHeight = h;
	Matrix3 worldTransform = gameObject->WorldTransform();
	float dt = GetApp()->getDeltaTime();
	
	if (m_texture != nullptr) {
		if (m_numFrames > 1) {

			if (loop == true || m_currentFrame < (m_numFrames - 1)) {
				m_currentFrameTime += dt;

				if (m_currentFrameTime > 1.0f / fps) {
					m_currentFrame++;
					m_currentFrame = m_currentFrame % m_numFrames;
					m_currentFrameTime = 0.0f;
				}
			}


			m_2dRenderer->setUVRect(//this gets the potition of the current frame on the image
				(m_frameSize.x * m_currentFrame) / m_texture->getWidth(),
				0,
				m_frameSize.x / m_texture->getWidth(),
				m_frameSize.y / m_texture->getHeight()
			);

		}
		m_2dRenderer->drawSprite(m_texture,
			worldTransform.position.x,
			worldTransform.position.y,
			m_size.x * worldTransform.GetScale().x,
			m_size.y * worldTransform.GetScale().y,
			worldTransform.GetRotation(),
			m_depth,
			m_Origin.x,
			m_Origin.y);
		m_2dRenderer->setUVRect(0, 0, 1, 1);
	}
	else {
		m_2dRenderer->drawBox(worldTransform.position.x,
			worldTransform.position.y,
			m_size.x * worldTransform.GetScale().x,
			m_size.y * worldTransform.GetScale().y,
			worldTransform.GetRotation(),
			m_depth);
	}
	
}


void Sprite::SetDepth(float depth) {
	m_depth = depth;
}

void Sprite::SetTextureSize(Vector2 size) {
	m_size = size;
}

void Sprite::SetTexture(const char* textureFileName) {
	if (m_texture != nullptr) {
		delete m_texture;
	}
	m_texture = new aie::Texture(textureFileName);
}
Vector2 Sprite::GetTextureSize() {
	return m_size;
}

void Sprite::SetTextureOrigin(Vector2 origin) {
	m_Origin = origin;
}
Vector2 Sprite::GetTextureOrigin() {
	return m_Origin;
}

void AnimatedSprite::BuildSpriteAnimation(const char* textureFileName, unsigned int numFrames, float fps, bool loop, Vector2 size, Vector2 origin) {
	//Sprite(textureFileName, size, origin);
	m_texture = new aie::Texture(textureFileName);
	m_size = size;
	m_Origin = origin;
	SetDepth();

	m_numFrames = numFrames;
	if (m_texture != nullptr) {
		m_frameSize.x = (float)(m_texture->getWidth()) / numFrames;
		m_frameSize.y = (float)m_texture->getHeight();
	}
	m_currentFrame = 0;
	m_currentFrameTime = 0;
	fps = fps;
	loop = loop;
}

bool AnimatedSprite::isValid() {
	if (m_numFrames < 2)
		return false;

	if (m_texture == nullptr)
		return false;

	if (fps <= 0)
		return false;


	return true;
}

void AnimatedSprite::JumpToFirstFrame() {
	m_currentFrame = 0;
	m_currentFrameTime = 0;
}

void AnimatedSprite::JumpToLastFrame() {
	this->m_currentFrame = m_numFrames - 1;
}



AnimatedSprite* CreateSpriteAnimationPointer(const char* textureFileName, unsigned int numFrames, float fps, bool loop, Vector2 size, Vector2 origin) {
	AnimatedSprite* newSpriteAnimation = new AnimatedSprite(textureFileName, numFrames, fps, loop, size, origin);

	return newSpriteAnimation;
}


Vector2 Sprite::GetClosestPointOnSprite(Sprite* sprite, Vector2 to) {
	if (sprite != nullptr) {
		Vector2 point;
		if (sprite->gameObject->transform.position.x < to.x) {
			point.x = sprite->gameObject->transform.position.x + ((sprite->m_size.x/2) * 1-sprite->m_Origin.x);
		}
		else {
			point.x = sprite->gameObject->transform.position.x - ((sprite->m_size.x/2) * sprite->m_Origin.x);
		}

		if (sprite->gameObject->transform.position.y < to.y) {
			point.y = sprite->gameObject->transform.position.y + (sprite->m_size.y * 1-sprite->m_Origin.y);
		}
		else {
			point.y = sprite->gameObject->transform.position.y - (sprite->m_size.y * sprite->m_Origin.y);
		}
		return point;
	}
	return Vector2::Zero();
}

Vector2 Sprite::GetCenterPosition()
{
	return Vector2(gameObject->transform.position.x, gameObject->transform.position.y + (m_size.y * 1-m_Origin.y) / 2);
}
