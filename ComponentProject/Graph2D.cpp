#include "Graph2D.h"
#include <Texture.h>

Graph2D::Graph2D() {

}
Graph2D::~Graph2D(){

}

void Graph2D::GetNearbyNodes(const Vector2 pos, float radius, std::vector<Graph2D::Node*> & out_Nodes) {
	for (auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++)
	{
		float dist = (pos - (*iter)->data).Length();
		if (dist < radius) {
			out_Nodes.push_back((*iter));
		}
	}
}

void Graph2D::GetClosestNode(const Vector2 pos, Graph2D::Node* &node) {
	Graph2D::Node* curNode = nullptr;
	float currentDist = 9999999;
	for (auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++)
	{
		float dist = (pos - (*iter)->data).Length();
		if (dist < currentDist) {
			curNode = (*iter);
			currentDist = dist;
		}
	}

	node = curNode;
}

Vector2 Graph2D::GetClosestNodePoint(const Vector2 pos) {
	Graph2D::Node* node;
	GetClosestNode(pos, node);
	if (node != nullptr) {
		return node->data;
	}
	else {
		return Vector2();
	}
}


Vector2 Graph2D::GetRandomNodePosition() {
	if (m_nodes.size() > 0) {
		return (m_nodes[(rand() % m_nodes.size())]->data);
	}
	return Vector2();
}

void Graph2D::GenerateGraphGrid(int graphRows, int graphCols, float graphSpacing, float edgeLenght, float graphOffsetX, float graphOffsetY) {
	for (int y = 0; y < graphRows; y++)
	{
		for (int x = 0; x < graphCols; x++)
		{
			auto newNode = AddNode(Vector2(
				x*graphSpacing + graphOffsetX,
				y*graphSpacing + graphOffsetY));
		}
	}

	auto nodes = GetNodes();
	for (auto iter = nodes.begin(); iter != nodes.end(); iter++)
	{
		auto node = (*iter);
		std::vector<Graph2D::Node*> surroundingNodes;
		GetNearbyNodes(node->data, edgeLenght, surroundingNodes);
		for (auto connectionsIter = surroundingNodes.begin(); connectionsIter != surroundingNodes.end(); connectionsIter++) {
			if ((*connectionsIter) == node) {
				continue;
			}

			float dist = (node->data - (*connectionsIter)->data).Length();

			AddEdge(node, (*connectionsIter), dist, false);
		}
	}
}

void Graph2D::GenerateGraphFromTexture(const char* textureFileName, int graphRows, float graphSpacing, float edgeLenght, float graphOffsetX, float graphOffsetY) {
	aie::Texture* mapTexture = new aie::Texture(textureFileName);

	if (mapTexture != nullptr) {
		const unsigned char *pixels = mapTexture->getPixels();
		int textureW = mapTexture->getWidth();
		int textureH = mapTexture->getHeight();
		int bpp = mapTexture->getFormat();

		for (int y = 0; y < textureH; y += graphSpacing)
		{
			for (int x = 0; x < textureW; x += graphSpacing)
			{
				int pxIndex = (y * textureW + x) * bpp;

				if (pixels[pxIndex + 0] != 0 ||
					pixels[pxIndex + 1] != 0 ||
					pixels[pxIndex + 2] != 0)
				{
					auto newNode = AddNode(Vector2(
						x + graphOffsetX,
						textureH - y + graphOffsetY));
				}

			}
		}

		auto nodes = GetNodes();
		for (auto iter = nodes.begin(); iter != nodes.end(); iter++)
		{
			auto node = (*iter);
			std::vector<Graph2D::Node*> surroundingNodes;
			GetNearbyNodes(node->data, edgeLenght, surroundingNodes);
			for (auto connectionsIter = surroundingNodes.begin(); connectionsIter != surroundingNodes.end(); connectionsIter++) {
				Vector2 pos = ((node->data) + (*connectionsIter)->data) * 0.5f;
				int pxIndex = ((textureH-(int)pos.y) * textureW + (int)pos.x) * bpp;
				if ((*connectionsIter) == node || 
					(pixels[pxIndex + 0] == 0 && pixels[pxIndex + 1] == 0 && pixels[pxIndex + 2] == 0)) 
				{
					continue;
				}

				float dist = (node->data - (*connectionsIter)->data).Length();

				AddEdge(node, (*connectionsIter), dist, false);
			}
		}
	}
}


