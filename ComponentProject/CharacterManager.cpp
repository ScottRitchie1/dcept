#include "CharacterManager.h"
#include "Hunter_AI.h"
#include "Assassin.h"
#include "Civilian.h"
#include <vector>


CharacterManager::CharacterManager() {
	m_charTracking.clear();
	m_graph = nullptr;
	m_assassin = nullptr;
	m_hunter = nullptr;
}
CharacterManager::~CharacterManager() {
	for (auto iter = m_charTracking.begin(); iter != m_charTracking.end(); iter++)
	{
		delete (*iter)->character->gameObject;
		delete (*iter);
		(*iter) = nullptr;
	}

	delete m_hunter;
	m_hunter = nullptr;
}


void CharacterManager::Update(float deltaTime) {
	for (auto iter = m_charTracking.begin(); iter != m_charTracking.end(); iter++)
	{
		if ((*iter)->character->currentState == IDLE) {
			(*iter)->inactiveTime += deltaTime;
		}
		else {
			(*iter)->inactiveTime = 0;
		}


		(*iter)->character->gameObject->Update(deltaTime);
		if ((*iter)->character->isDead && (*iter)->isDiscovered == false) {
			(*iter)->character->currentState = IDLE;
		}
	}

	if (m_hunter != nullptr) {
		m_hunter->gameObject->Update(deltaTime);
	}
}
void CharacterManager::Draw(aie::Renderer2D* m_2dRenderer) {
	for (auto iter = m_charTracking.begin(); iter != m_charTracking.end(); iter++)
	{
		(*iter)->character->Draw(m_2dRenderer);
	}

	if (m_hunter != nullptr) {
		m_hunter->gameObject->Draw(m_2dRenderer);
	}
}

CharacterTracking* CharacterManager::FindClosestCivilian(Vector2 Pos, bool includeDead) {
	CharacterTracking* closestCiv = nullptr;
	float closestCivDistance;

	for (auto iter = m_charTracking.begin(); iter != m_charTracking.end(); iter++)
	{
		if ((*iter)->character->isDead == false || includeDead == true) {
			float dist = (*iter)->character->gameObject->transform.position.DistanceFrom(Pos);
			if (dist > 0) {
				if (closestCiv == nullptr || dist < closestCivDistance) {
					closestCiv = (*iter);
					closestCivDistance = dist;
				}
			}
		}
	}

	return closestCiv;
}

CharacterTracking* CharacterManager::GetCharacterAtPoint(Vector2 Pos, bool includeDead) {
	CharacterTracking* currentUnderPoint = nullptr;

	for (size_t i = 0; i < m_charTracking.size(); i++)
	{
		if (includeDead == true || m_charTracking[i]->character->isDead == false) {
			Vector2 _size = m_charTracking[i]->character->GetCurrentAnimation()->GetTextureSize();
			Vector2 _pos = m_charTracking[i]->character->gameObject->transform.position;
			Vector2 _origin = m_charTracking[i]->character->GetCurrentAnimation()->GetTextureOrigin();
			float RightSide = _pos.x - (_size.x * _origin.x) + _size.x;
			float LeftSide = _pos.x - (_size.x * _origin.x);
			float TopSide = _pos.y - (_size.y * _origin.y) + _size.y;
			float BottomSide = _pos.y - (_size.y * _origin.y);
			if (Pos.x < RightSide && Pos.x > LeftSide && Pos.y < TopSide && Pos.y > BottomSide) {
				if (currentUnderPoint == nullptr || m_charTracking[i]->character->gameObject->transform.position.y < currentUnderPoint->character->gameObject->transform.position.y) {
					currentUnderPoint = m_charTracking[i];
				}
			}
		}
	}

	return currentUnderPoint;
}


Civilian* CharacterManager::CreateNewCivilian() {
	GameObject* obj = new GameObject(gameObject->GetCurrentState());

	obj->useForces = true;
	obj->drag = 0.3f;

	obj->AddComponent<Civilian>("", INITIAL_CIV_SPEED, INITIAL_CIV_IDLE_CHANCE, Vector2(64, 64), Vector2(0.5f, 0.0f));
	auto charComp = obj->GetComponent<Civilian>();
	charComp->SetGraph(m_graph);
	charComp->SetAnimation(E_State::DEAD, "./textures/Characters/Skeleton.png", 14, 10, false);

	charComp->SetAnimation(E_State::WALK, E_Dir::UP, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_WalkUp.png", 4, 5.f);
	charComp->SetAnimation(E_State::WALK, E_Dir::DOWN, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_WalkDown.png", 4, 5.f);
	charComp->SetAnimation(E_State::WALK, E_Dir::RIGHT, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_WalkRight.png", 4, 5.f);
	charComp->SetAnimation(E_State::WALK, E_Dir::LEFT, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_WalkLeft.png", 4, 5.f);

	charComp->SetAnimation(E_State::IDLE, E_Dir::UP, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_IdleUp.png", 2);
	charComp->SetAnimation(E_State::IDLE, E_Dir::DOWN, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_IdleDown.png", 2);
	charComp->SetAnimation(E_State::IDLE, E_Dir::RIGHT, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_IdleRight.png", 2);
	charComp->SetAnimation(E_State::IDLE, E_Dir::LEFT, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_IdleLeft.png", 2);

	return charComp;
}

Assassin* CharacterManager::CreateNewAssassin() {
	GameObject* obj = new GameObject(gameObject->GetCurrentState());

	obj->useForces = true;
	obj->drag = 0.3f;

	obj->AddComponent<Assassin>("", INITIAL_CIV_SPEED, Vector2(64, 64), Vector2(0.5f, 0.0f));
	auto charComp = obj->GetComponent<Assassin>();
	charComp->SetInputs(aie::INPUT_KEY_W, 
						aie::INPUT_KEY_S, 
						aie::INPUT_KEY_A, 
						aie::INPUT_KEY_D, 
						aie::INPUT_KEY_Q);
	charComp->SetCharacterManager(this);
	charComp->SetAnimation(E_State::DEAD, "./textures/Characters/Skeleton.png", 14, 10, false);

	charComp->SetAnimation(E_State::WALK, E_Dir::UP, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_WalkUp.png", 4, 5.f);
	charComp->SetAnimation(E_State::WALK, E_Dir::DOWN, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_WalkDown.png", 4, 5.f);
	charComp->SetAnimation(E_State::WALK, E_Dir::RIGHT, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_WalkRight.png", 4, 5.f);
	charComp->SetAnimation(E_State::WALK, E_Dir::LEFT, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_WalkLeft.png", 4, 5.f);

	charComp->SetAnimation(E_State::IDLE, E_Dir::UP, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_IdleUp.png", 2);
	charComp->SetAnimation(E_State::IDLE, E_Dir::DOWN, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_IdleDown.png", 2);
	charComp->SetAnimation(E_State::IDLE, E_Dir::RIGHT, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_IdleRight.png", 2);
	charComp->SetAnimation(E_State::IDLE, E_Dir::LEFT, "./textures/Characters/Civ1_OldMan/Civ1_OldMan_IdleLeft.png", 2);

	return charComp;
}

void CharacterManager::CreateNewHunter(bool isAI) {
	GameObject* obj = new GameObject(gameObject->GetCurrentState());


	obj->useForces = true;
	obj->drag = 0.3f;
	obj->AddComponent<HunterLight>("./textures/Effects/LightEffect.png", 400.f, 0.6f, 0.f, Vector2(0, 20.f));
	Hunter* charComp;//, Vector2(64, 64), Vector2(0.5f, 0.0f));
	if (isAI) {
		charComp = obj->AddComponent<Hunter_AI>("", 100);
	}
	else {
		charComp = obj->AddComponent<Hunter>("", 100);
		charComp->SetInputs(aie::EInputCodes::INPUT_MOUSE_BUTTON_LEFT, aie::EInputCodes::INPUT_MOUSE_BUTTON_RIGHT, aie::EInputCodes::INPUT_MOUSE_BUTTON_MIDDLE);
	}
	if (m_graph != nullptr) {
		charComp->InitializeCharacter(m_graph->GetClosestNodePoint(Vector2(GetApp()->getWindowWidth() / 2, GetApp()->getWindowHeight() / 2)));
	}
	charComp->SetCharacterManager(this);
	charComp->SetGraph(m_graph);
	charComp->SetLight(obj->GetComponent<HunterLight>());

	charComp->SetAnimation(E_State::DEAD, "./textures/Character/Skeleton.png", 14, 10, false);

	charComp->SetAnimation(E_State::WALK, E_Dir::UP, "./textures/Characters/Hunter/Hunter_WalkUp.png", 4.f, 3.5f);
	charComp->SetAnimation(E_State::WALK, E_Dir::DOWN, "./textures/Characters/Hunter/Hunter_WalkDown.png", 4.f, 3.5f);
	charComp->SetAnimation(E_State::WALK, E_Dir::RIGHT, "./textures/Characters/Hunter/Hunter_WalkRight.png", 4.f, 3.5f);
	charComp->SetAnimation(E_State::WALK, E_Dir::LEFT, "./textures/Characters/Hunter/Hunter_WalkLeft.png", 4.f, 3.5f);

	charComp->SetAnimation(E_State::IDLE, E_Dir::UP, "./textures/Characters/Hunter/Hunter_IdleUp.png", 2.f);
	charComp->SetAnimation(E_State::IDLE, E_Dir::DOWN, "./textures/Characters/Hunter/Hunter_IdleDown.png", 2.f);
	charComp->SetAnimation(E_State::IDLE, E_Dir::RIGHT, "./textures/Characters/Hunter/Hunter_IdleRight.png", 2.f);
	charComp->SetAnimation(E_State::IDLE, E_Dir::LEFT, "./textures/Characters/Hunter/Hunter_IdleLeft.png", 2.f);
	charComp->ScaleSpeed(150);

	delete m_hunter;
	m_hunter = charComp;
}

CharacterTracking* CharacterManager::CreateNewCharTracker(bool isAssassin, Civilian* civ) {
	auto newTracker = new CharacterTracking();
	newTracker->isDiscovered = false;
	newTracker->inactiveTime = 0;
	newTracker->numNeighbours = 0;
	newTracker->probability = INITIAL_PROBABILITY;
	newTracker->isAssassin = isAssassin;
	if (civ == nullptr) {
		if (isAssassin) {
			newTracker->character = CreateNewAssassin();
		}
		else {
			newTracker->character = CreateNewCivilian();
		}
	}
	else {
		newTracker->character = civ;
	}
	return newTracker;
}

std::vector<CharacterTracking*>* CharacterManager::GetCharList() {
	return &m_charTracking;
}

CharacterTracking* CharacterManager::GetAssassin() {
	return m_assassin;
}

Hunter* CharacterManager::GetHunter() {
	return m_hunter;
}
Hunter_AI* CharacterManager::GetHunter_AI() {
	return dynamic_cast<Hunter_AI*>(m_hunter);
}


void CharacterManager::SetCivilianCount(unsigned int num) {
	if (m_charTracking.size() < num) {
		for (size_t i = m_charTracking.size(); i < num; i++)
		{
			auto newCiv = CreateNewCharTracker();
			if (m_graph != nullptr) {
				newCiv->character->InitializeCharacter(m_graph->GetRandomNodePosition());
			}
			else {
				newCiv->character->InitializeCharacter(Vector2(GetApp()->getWindowWidth() / 2, GetApp()->getWindowHeight() / 2));
			}
			m_charTracking.push_back(newCiv);
		}
	}
	else {
		for (size_t i = m_charTracking.size(); i > num; i--)
		{
			if (m_charTracking[i] == m_assassin) {
				m_assassin = nullptr;
			}
			delete m_charTracking.back()->character->gameObject;
			delete m_charTracking.back();
			m_charTracking.back() = nullptr;
			m_charTracking.pop_back();

			if (m_charTracking.size() <= 0) {
				break;
			}
		}
	}
}


void CharacterManager::IncreaseCivilianCount(int num) {
	int newSize = (int)m_charTracking.size() + num;
	if (newSize < 0)
		newSize = 0;
	SetCivilianCount(newSize);
}

void CharacterManager::AddAssassin() {
	auto newAssassin = CreateNewCharTracker(true);
	m_charTracking.push_back(newAssassin);
	m_assassin = newAssassin;
	if (m_graph != nullptr) {
		auto nodePos = m_graph->GetRandomNodePosition();
		newAssassin->character->InitializeCharacter(nodePos);
	}
	else {
		newAssassin->character->InitializeCharacter(Vector2(GetApp()->getWindowWidth() / 2, GetApp()->getWindowHeight() / 2));
	}
}

void CharacterManager::ResetCivilians() {
	for (auto iter = m_charTracking.begin(); iter != m_charTracking.end(); iter++)
	{
		if ((*iter)->isAssassin == false) {
			(*iter)->character->Reset();
			(*iter)->Reset(INITIAL_PROBABILITY);

			if (m_graph != nullptr) {
				(*iter)->character->InitializeCharacter(m_graph->GetRandomNodePosition());
			}
			else {
				(*iter)->character->InitializeCharacter(Vector2(GetApp()->getWindowWidth() / 2, GetApp()->getWindowHeight() / 2));
			}
		}
	}
}

void CharacterManager::ResetAssassin()
{
	if (m_assassin != nullptr) {
		m_assassin->character->Reset();
		m_assassin->Reset(INITIAL_PROBABILITY);

		if (m_graph != nullptr) {
			m_assassin->character->InitializeCharacter(m_graph->GetRandomNodePosition());
		}
		else {
			m_assassin->character->InitializeCharacter(Vector2(GetApp()->getWindowWidth() / 2, GetApp()->getWindowHeight() / 2));
		}
	}
}

void CharacterManager::ResetHunter()
{
	if (m_hunter != nullptr) {
		//Hunter_AI* m_hunter_AI = dynamic_cast<Hunter_AI*>(m_hunter);
		m_hunter->Reset();
		m_hunter->ClearHunterTargets();

		if (m_graph != nullptr) {
			m_hunter->InitializeCharacter(m_graph->GetRandomNodePosition());
		}
		else {
			m_hunter->InitializeCharacter(Vector2(GetApp()->getWindowWidth() / 2, GetApp()->getWindowHeight() / 2));
		}
	}
}

void CharacterManager::SetGraph(Graph2D * graph) {
	m_graph = graph;
}

void CharacterManager::DistributeProbability(CharacterTracking* charToDistribute, float amount, std::vector<CharacterTracking*>* list) {
	auto _list = list;
	if (_list == nullptr) {
		_list = &m_charTracking;
	}
	
	if (charToDistribute != nullptr) {

		if (amount == 0) {
			amount = -charToDistribute->probability;
		}

		std::vector<CharacterTracking*> distributeTo;

		for (size_t i = 0; i < _list->size(); i++)
		{
			if ((*_list)[i]->probability > 0 && (*charToDistribute).character != (*_list)[i]->character && ((*_list)[i]->character->isDead == false || (*_list)[i]->isDiscovered == false)) {
				distributeTo.push_back((*_list)[i]);
			}
		}

		float seperatedAmount = -amount / distributeTo.size();
		ScaleProbability(charToDistribute, amount);

		for (size_t i = 0; i < distributeTo.size(); i++)
		{
			ScaleProbability(distributeTo[i], seperatedAmount);
		}
	}
}


std::vector<CharacterTracking*>* CharacterManager::GetCharatersWithinRange(Vector2 position, float range) {
	std::vector<CharacterTracking*>* charactersWithinRange = new std::vector<CharacterTracking*>();

	for (size_t i = 0; i < m_charTracking.size(); i++)
	{
		if (m_charTracking[i]->probability > 0 && m_charTracking[i]->character->isDead == false && m_charTracking[i]->character->gameObject->transform.position.DistanceFrom(position) < range) {
			charactersWithinRange->push_back(m_charTracking[i]);
		}
	}

	return charactersWithinRange;
}

std::vector<CharacterTracking*>* CharacterManager::GetCharatersOutsideRange(Vector2 position, float range) {
	std::vector<CharacterTracking*>* charactersWithinRange = new std::vector<CharacterTracking*>();

	for (size_t i = 0; i < m_charTracking.size(); i++)
	{
		if (m_charTracking[i]->probability > 0 && m_charTracking[i]->character->isDead == false && m_charTracking[i]->character->gameObject->transform.position.DistanceFrom(position) > range) {
			charactersWithinRange->push_back(m_charTracking[i]);
		}
	}

	return charactersWithinRange;
}


void CharacterManager::ScaleProbability(CharacterTracking* charToDistribute, float amount) {
	if (charToDistribute != nullptr) {
		charToDistribute->probability += amount;
	}
}

CharacterTracking* CharacterManager::GetHighestProbability() {
	CharacterTracking* highestProbability = nullptr;

	for (size_t i = 0; i < m_charTracking.size(); i++) {
		if (highestProbability == nullptr || m_charTracking[i]->probability > highestProbability->probability) {
			highestProbability = m_charTracking[i];
		}
	}

	return highestProbability;
}

unsigned int CharacterManager::GetActiveCharacterCount() {
	unsigned int count = 0;
	for (size_t i = 0; i < m_charTracking.size(); i++) {
		if ((m_charTracking[i]->character->isDead == false || m_charTracking[i]->isDiscovered == false) && m_charTracking[i]->character->isTagged == false) {
			count++;
		}
	}
	return count;
}

unsigned int CharacterManager::GetAliveCharacterCount() {
	unsigned int count = 0;
	for (size_t i = 0; i < m_charTracking.size(); i++) {
		if (!m_charTracking[i]->character->isDead) {
			count++;
		}
	}
	return count;
}

unsigned int CharacterManager::GetCivilianCount()
{
	return m_charTracking.size()-1;
}

unsigned int CharacterManager::GetDeadCivilianCount() {
	int count = 0;
	for (size_t i = 0; i < m_charTracking.size(); i++) {
		if (m_charTracking[i]->character->isDead && m_charTracking[i]->isAssassin == false) {
			count++;
		}
	}

	return count;
}

unsigned int CharacterManager::GetCharacterCount()
{
	return m_charTracking.size();
}

unsigned int CharacterManager::GetTaggedCivilianCount()
{
	int count = 0;
	for (size_t i = 0; i < m_charTracking.size(); i++)
	{
		if (m_charTracking[i]->character->isTagged == true) {
			count++;
		}
	}

	return count;
}

bool CharacterManager::IsHunterAI()
{
	return (GetHunter_AI() != nullptr);
}

void CharacterTracking::Reset(float defaultProb)
{
	isDiscovered = false;
	probability = defaultProb;
	inactiveTime = 0;
	numNeighbours = 0;
}
